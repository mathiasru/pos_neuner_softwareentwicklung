# Wochenkarte Bericht und Testprotokoll

Das gesamte Programm wurde in Partnerarbeit umgesetzt (Rudig, Tilg).
<br>
<br>

# Inhaltsverzeichnis
- [Wochenkarte Bericht und Testprotokoll](#wochenkarte-bericht-und-testprotokoll)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Konfiguration Webserver](#konfiguration-webserver)
  - [Einstellungen PhpStorm](#einstellungen-phpstorm)
- [Lösungsaufbau](#lösungsaufbau)
- [Implementierung der Wochenkarte](#implementierung-der-wochenkarte)
  - [Formulare](#formulare)
  - [Logik](#logik)
- [Testprotokoll](#testprotokoll)
<br>
<br>

# Konfiguration Webserver

- Virtuelle Maschine mit Linux Ubuntu Server
- Docker Container lt. docker-compose.yml
```yml
  version: '2'
  services:
  apache:
    image: thecodingmachine/php:7.2-v2-apache-node10
    container_name: apache
    restart: unless-stopped
    ports:
      - 80:80
    environment:
            PHP_EXTENSION_PGSQL: 1
            PHP_EXTENSION_XDEBUG: 1
            PHP_EXTENSION_MONGODB: 1
            PHP_EXTENSION_MONGO: 1
            PHP_INI_ERROR_REPORTING: E_ALL
            APACHE_EXTENSION_AUTOINDEX: 1

    volumes:
      - ./www:/var/www/html
```

- sftp installieren
    - sudo apt-get install vsftpd
- Verbindung von PHPStorm zu VM mittels sftp
    - unter Tools Deployment anlegen
- Verbindung zum Host Zugangsdaten
    - Benutzer: mathiasrudig
    - Passwort: Standard ohne !
<br>
<br>

## Einstellungen PhpStorm

- SSH-Verbindung --> Terminal (Tools - Start SSH Session)
- FTP-Verbindung --> Remote Host (Tools - Deployment - Browse Remote Host)
- SSH-Verbindung eintragen (Tools - Deployment - Configuration - Connection)
- Pfad mappen (Tools - Deployment - Configuration - Mappings)
- Upload bei Safe (Settings - Deployment - Options - Always)
<br>
<br>

# Lösungsaufbau

1. Erstellen einer index.php, in der geprüft wird, ob bereits ein Cookie gesetzt wurde
 - falls ja, Loginformular anzeigen
 - falls nein, Fenster anzeigen in dem Cookies akzeptiert werden können und wenn akzeptiert wird ein Cookie setzen

2. Überprüfung auf Benutzer
- entweder weiterleiten auf die Wochenkartenübersicht und setzen einer Session
- oder Fehlermeldung ausgeben, dass die Daten nicht stimmen

3. Wochenkartenübersicht erstellen und Logoutformular einbauen
-  sollte man sich eingeloggt haben und die Session ist noch aufrecht (nicht ausgeloggt), dann sollte man wieder auf die Übersicht geworfen werden
-  nach einem Logout die Session löschen und zum Loginfenster zurückkehren
-  ohne Login wird eine eigene Seite mit Fehler angezeigt
<br>
<br>

# Implementierung der Wochenkarte

## Formulare

Das Projekt beinhaltet folgende Formulare

- Akzeptieren von Cookies
- Login
- Logout

Beim Akzeptieren von Cookies wird ein Post-Request an `cookies.php` (folgender Codeausschnitt) geschickt. Durch das akzepieren wird ein Cookies erstellt, das nach einer Stunde abläuft. Solang das Cookie existiert, wird dieses Formular nicht erscheinen.

```php
<?php
if (isset($_POST['cookie'])) {
    setcookie('active', 1, time() + 3600);
    header("Location: index.php");
    exit();
}
```

Das Formular für den Login versendet einen Request mit den eingegbenen Daten (E-Mail, Passwort) und überprüft darauf hin ob diese mit dem vordefinierten Benutzer übereinstimmen. Entweder wird man auf die Wochenkarte weitergeleitet, oder es wird eine Meldung ausgegeben, dass die Daten nicht korrekt sind.

Duch den Logout wird man wieder zurück auf den Loginscreen geschickt.
<br>
<br>

## Logik

Die `Formularverarbeitung` wird von den jeweiligen Klassen übernommen, an die die Daten gesendet werden. Beim Logout und Login muss sichergestellt werden, dass ein Formular abgeschickt wurde, dann kann erst die benötigte Funktion ausgeführt werden.

Die Validierung (Überprüfung der Logindaten) wird mit der kompletten restlichen Logik, innerhalb der Benutzerklasse mit Funktionen übernommen.

```php
$message = '';
$email = '';
$password = '';

if (isset($_POST['submit'])) { //Formularverarbeitung Login
    $email = ($_POST['login'] ?? '');
    $password = ($_POST['password'] ?? '');
    $user = Benutzer::get($email, $password);
    if ($user == null) { //überprüfen ob die Login Daten stimmen
        $message .= "<div class='alert-danger form-control'><p>Die eingegebenen Daten sind fehlerhaft!</p></div>";
    } else {
        $user->login();
    }
}
```

```php
if (isset($_POST['logout'])) { //Formularverarbeitung Logout
    Benutzer::logout();
}
```

Überprüft wird zusätzlich auch immer, ob man sich auch tatsächlich eingeloggt hat. Sollte man ohne sich eingeloggt zu haben die Wochenkarte aufrufen, so erhält man eine Fehlerseite (siehe Testprotokoll). Möchte man den Loginscreen aufrufen obwohl man noch eingeloggt ist, so wird man direkt auf die Wochenkarte geleitet.
<br>
<br>

# Testprotokoll

Bevor Cookie akzeptiert wurde:</br>
![cookies_01.png](07_Bericht_Wochenkarte_Bilder/cookies_01.png)

Nachdem Cookie akzeptiert wurde:</br>
![cookies_02.png](07_Bericht_Wochenkarte_Bilder/cookies_02.png)

Clientseitige Validierung mittels required:</br>
![validierungClient_01.png](07_Bericht_Wochenkarte_Bilder/validierungClient_01.png)

Nachdem erfolgreichem Login (hinterlegte Zugangsdaten) Session setzen:</br>
![desktopWochenkarte_01.png](07_Bericht_Wochenkarte_Bilder/desktopWochenkarte_01.png)

Responsive Design (für mobile Endgeräte):</br>
![mobileWochenkarte_01.png](07_Bericht_Wochenkarte_Bilder/mobileWochenkarte_01.png)

Direkter Zugriff über URL nach einem Logout (Session zerstört):</br>
![unauthorized_01.png](07_Bericht_Wochenkarte_Bilder/unauthorized_01.png)
