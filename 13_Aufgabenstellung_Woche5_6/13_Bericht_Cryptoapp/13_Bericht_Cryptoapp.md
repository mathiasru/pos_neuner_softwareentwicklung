# Cryptoapp Bericht und Testprotokoll

Das gesamte Programm wurde in Partnerarbeit umgesetzt (Rudig, Tilg).
<br>

# Inhaltsverzeichnis
- [Cryptoapp Bericht und Testprotokoll](#cryptoapp-bericht-und-testprotokoll)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Konfiguration Webserver](#konfiguration-webserver)
  - [Einstellungen PhpStorm](#einstellungen-phpstorm)
- [Lösungsansatz](#lösungsansatz)
  - [Datenbank](#datenbank)
  - [Models](#models)
  - [Grafisch](#grafisch)
  - [REST-Controller](#rest-controller)
- [Implementierung der Cryptoapp](#implementierung-der-cryptoapp)
  - [UML Diagramm](#uml-diagramm)
  - [Datenbankanbindung](#datenbankanbindung)
  - [Model und Controller ergänzen](#model-und-controller-ergänzen)
  - [Vue.js](#vuejs)
    - [main.js](#mainjs)
    - [AppDisplay.js](#appdisplayjs)
    - [PurchaseForm.js](#purchaseformjs)
    - [WalletList.js](#walletlistjs)
- [Testprotokoll](#testprotokoll)
  - [Übersicht](#übersicht)
  - [REST-API](#rest-api)
<br>
<br>
# Konfiguration Webserver

- Virtuelle Maschine mit Linux Ubuntu Server
- Docker Container lt. docker-compose.yml
```yml
  version: '2'
  services:
  apache:
    image: thecodingmachine/php:7.2-v2-apache-node10
    container_name: apache
    restart: unless-stopped
    ports:
      - 80:80
    environment:
            PHP_EXTENSION_PGSQL: 1
            PHP_EXTENSION_XDEBUG: 1
            PHP_EXTENSION_MONGODB: 1
            PHP_EXTENSION_MONGO: 1
            PHP_INI_ERROR_REPORTING: E_ALL
            APACHE_EXTENSION_AUTOINDEX: 1

    volumes:
      - ./www:/var/www/html
```

- sftp installieren
    - sudo apt-get install vsftpd
- Verbindung von PHPStorm zu VM mittels sftp
    - unter Tools Deployment anlegen
- Verbindung zum Host Zugangsdaten
    - Benutzer: mathiasrudig
    - Passwort: Standard ohne !
<br>
<br>

## Einstellungen PhpStorm

- SSH-Verbindung --> Terminal (Tools - Start SSH Session)
- FTP-Verbindung --> Remote Host (Tools - Deployment - Browse Remote Host)
- SSH-Verbindung eintragen (Tools - Deployment - Configuration - Connection)
- Pfad mappen (Tools - Deployment - Configuration - Mappings)
- Upload bei Safe (Settings - Deployment - Options - Always)
<br>
<br>

# Lösungsansatz

## Datenbank

* Datenbank anlegen
* Tabellen und Datensätze importieren
* Datenbankverbindung in Projekt anpassen

## Models

* Das Model Purchase ergänzen (alle Methoden bereitstellen)
* DTO (Data Transfer Object) für REST-Controller bereitstellen (Gruppierte Ausgabe)
* .htaccess Datei implementieren (leserliche URL)

## Grafisch

* Frontend mittels Vue.js-Framework umsetzen
* API-Calls mittels AXIOS aufbauen - Daten empfangen und senden (GET, POST)
* Components aufbauen  (Formular und Anzeige)

## REST-Controller

* Fehlende Endpunkte der REST-Schnittstelle von Purchase ergänzen (alle Methoden bereitstellen) - Requests
* REST-Funktionen testen (Insomnia)

# Implementierung der Cryptoapp

## UML Diagramm

![UML Diagramm](13_Bericht_Cryptoapp_Bilder/UML.png)

## Datenbankanbindung

* PDO-Verbindung einrichten (Singelton Pattern)
* Datenbankverbindung herstellen
* SQL-Statement definieren
* Statement vorbereiten (`prepare()`)
* Wenn Parameter im Statement vorhanden, diese setzen (`bindParam()`)
* Statement ausführen (`execute()`)
* Wenn Datensätze erwartet werden, Daten Fetchen (zB durch `fetchObject()`)
* Datenbankverbindung schließen

## Model und Controller ergänzen 

Das Purchase Model wurde um die `getAllGroupByCurrency()` Methode erweitert.

Der PurchaseRESTController war gegeben, musste jedoch um die handleGETRequest und handlePUTRequest ergänzt werden.

Für die erstellte Methode im Model musste natürlich auch ein Handeler für GET-Request erstellt werden. Mithilfe des PurchaseDTOs (Data Transfer Object) wird ein Objekt geliefert, das die benötigten Daten enthält.

```php
private function handleGETRequest()
{
      //...
    } elseif ($this->verb == 'currency' && sizeof($this->args) == 1) { //getByCurrency
        $purchases = Purchase::getAllGroupByCurrency($this->args[0]);
        $model = new PurchaseDTO();
        $amount = 0;
        $price = 0;
        foreach ($purchases as $purchase) {
            $amount += $purchase->getAmount();
            $price +=  $purchase->getAmount() * $purchase->getPrice();
        }
        $model->setAmount($amount);
        $model->setPrice($price);
        $model->setCurrency($this->args[0]);
        $this->response($model);
    } else {
        $this->response("Bad request", 400);
    }
}
```

## Vue.js

Innerhalb der main.js wird eine Vue App erstellt, die die `index.html` importiert und verwendet. Die unterschiedlichen Funktionen wurden in Komponenten ausgelagert, welche ebenfalls in importiert werden.

### main.js

Die `main.js` übernimmt das Laden der Daten über die REST-API (purchases, prices). Außerdem wird die POST-Request, beim Kauf einer neuen Währung abgewickelt.

### AppDisplay.js

Wie bereits erwähnt werden innerhalb der index.html werden die benutzten Komponenten importiert. Durch den `<appdisplay>` Tag wird die Komponente aufgerufen und ihr werden die geladenen Daten mitgegeben. `AppDisplay.js` ist nur dazu da, um die weiteren zwei Komponenten in einer ansehnlichen Form darzustellen und ihnen weiters die benötigten Daten mitzugeben.

```js
app.component('app-display', {
    props: {
        prices: {
            type: Array,
            required: true
        },
        purchases: {
            type: Array,
            required: true
        }
    },
    template:
    /*html*/
    `<div class="wallet-display">
        <div class="wallet-container">
            <purchase-form :prices="prices"></purchase-form>
            <wallet-list :purchases="purchases" :prices="prices"></wallet-list>
        </div>
    </div>`
})
```

### PurchaseForm.js

Die Hauptaufgabe der Komponente `PurchaseForm.js` ist das Aufnehmen und Weiterverarbeiten von Daten in Form eines Formulares. Zudem wird während des Auswählens der Menge und der Währung im Hintergrund der zu zahlende Preis berechnet und angezeigt (als computet, da bessere Perfomance). Abschließend wird das Formular beim Kauf abgesendet, validiert, ein Objekt erstellt und per POST-Request(Axios) der Kauf getätigt und in der Datenbank gespeichert.

Das Forular lost beim Buttonklick das Exent `submit.prevent` aus, welches die Methode `onSubmit()` zum Verarbeiten des Formulares bereitstellt. Innerhalb der Methode wird validiert, ein Objekt erzeugt, die Inputfelder wieder bereinigt und das erzeugte Objekt an die `createPurchase(purchase)`  weitergeleitet. Dort werden für die Datenbank fehlende Datenfelder hinzugefügt und das fertige Objekt per `this.$parent.$emit('add-to-wallet', request)` an die App (main.js) gesendet, wo dann der POST-Request(Axios) erfolgt. Dieser lädt dann im Amschluss durch dass Schlüsselwort `finally` mittels `loadPurchases()` die aktuellen Käufe und updated die Komponente `WalletList.js` mittels watch befehl.

Aufbau des Formulares

![Formular](13_Bericht_Cryptoapp_Bilder/formular.png)

Methode wird von onSubmit() aufgerufen und enthält die Daten des Forumlares
```javascript
createPurchase(purchase) {
            //Form-Request behandeln
            let currency = purchase.currency
            let price = parseFloat(purchase.price)
            let amount = parseFloat(purchase.amount)
            let timestamp = this.getDateTimeNow();

            //JSON-Objekt erstellen
            let request = {
                date: timestamp,
                amount: parseFloat(amount),
                price: parseFloat(price),
                currency: currency
            }

            //senden des Requests an Parent-Komponente (main.js)
            this.$parent.$emit('add-to-wallet', request)
        }
```

POST - Rquest in main.js
```javascript
addPurchase(request) {
            //Purchase in Datenbank schreiben
            axios.post('http://10.211.55.4/cryptoapp/api/purchase', request).then(function (response) {
                console.log(response)
            }).finally(this.loadPurchases)
                .catch(function (error) {
                    console.log(error)
                })
        }
```

### WalletList.js

Die Komponente `WalletList.js` erhält sowohl alle Käufe und die aktuellen Preise der Währungen. Sie dient dazu, alle Währungspositionen inkl. aktuellem Wert und Gewinn/Verlust anzuzeigen. Zusätzlich sollte auch für die gesamte Wallet ein Betrag mit Gewinn/Verlust dargestellt werden.

Mithilfe der `getWallet()` Methode werden aus allen Käufen die Währungen ermittelt, welche man besitzt. Mit der zu Beginn implementierten Methode `getAllGroupByCurrency()` im Purchase Model, werden über einen `axios.get()` Aufruf alle Positionen der Wallet ermittelt.
Um sich den aktuellen Wert und den Gewinn/Verlust berechnen zu können, werden die aktuellen Preise benötigt.

```js
//...
methods: {
  getWallet() {
      //Alle gekauften Währungen holen
      this.wallet = []; //Einträge zurücksetzen
      let currencies = new Set(); //damit keine doppelten Currency-Einträge enthalten sind
      for (let purchase of this.purchases) {
          currencies.add(purchase.currency)
      }

      //Einträge holen
      for (let currency of currencies) {
          axios.get('http://10.77.0.110/cryptoapp/api/purchase/currency/' + currency)
              .then(response => (this.wallet.push(response.data)))
              .catch(error => console.log(error))
      }
  }
```

# Testprotokoll

## Übersicht

Grafische Übersicht

![Uebersicht](13_Bericht_Cryptoapp_Bilder/uebersicht.png)

Fehler bei unvollständigem Formular

![Formularfehler](13_Bericht_Cryptoapp_Bilder/formularFehler.png)

## REST-API

Abfragen aller Käufe (GET)

![REST GET getAll](13_Bericht_Cryptoapp_Bilder/getAllPurchases.png)

Abfragen der Käufe gruppiert nach Cryptowährung (GET)

![REST GET getByCurrency](13_Bericht_Cryptoapp_Bilder/getByCurrency.png)

Hinzufügen eines neuen Kaufs (POST)

![REST POST error](13_Bericht_Cryptoapp_Bilder/createPurchase.png)