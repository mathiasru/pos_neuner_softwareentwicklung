<?php

require_once('RESTController.php');
require_once('server/models/Purchase.php');
require_once('server/models/PurchaseDTO.php');

class PurchaseRESTController extends RESTController
{
    public function handleRequest()
    {
        switch ($this->method) {
            case 'GET':
                $this->handleGETRequest();
                break;
            case 'POST':
                $this->handlePOSTRequest();
                break;
            case 'PUT':
                $this->handlePUTRequest();
                break;
            case 'DELETE':
                $this->handleDELETERequest();
                break;
            default:
                $this->response('Method Not Allowed', 405);
                break;
        }
    }

    /**
     * get single/all purchase or search purchase
     * all purchase: GET api.php?r=purchase
     * single purchase: GET api.php?r=purchase/25 -> args[0] = 25
     * all purchases group by currency: GET api.php?r=purchase/currency/BTC -> verb = currency, args[0] = BTC
     */
    private function handleGETRequest()
    {
        // TODO
        if ($this->verb == null && empty($this->args)) { //getAll
            $purchases = Purchase::getAll();
            $this->response($purchases);
        } elseif  ($this->verb == null && sizeof($this->args) == 1) { //getById
            $purchase = Purchase::get($this->args[0]);
            $this->response($purchase);
        } elseif ($this->verb == 'currency' && sizeof($this->args) == 1) { //getByCurrency
            $purchases = Purchase::getAllGroupByCurrency($this->args[0]);
            $model = new PurchaseDTO();
            $amount = 0;
            $price = 0;
            foreach ($purchases as $purchase) {
                $amount += $purchase->getAmount();
                $price +=  $purchase->getAmount() * $purchase->getPrice();
            }
            $model->setAmount($amount);
            $model->setPrice($price);
            $model->setCurrency($this->args[0]);
            $this->response($model);
        } else {
            $this->response("Bad request", 400);
        }
    }

    /**
     * create purchase: POST api.php?r=purchase
     */
    private function handlePOSTRequest()
    {
        $model = new Purchase();
        $model->setDate($this->getDataOrNull('date'));
        $model->setAmount($this->getDataOrNull('amount'));
        $model->setPrice($this->getDataOrNull('price'));
        $model->setCurrency($this->getDataOrNull('currency'));

        if ($model->save()) {
            $this->response("OK", 201);
        } else {
            $this->response($model->getErrors(), 400);
        }
    }

    /**
     * update purchase: PUT api.php?r=purchase/25 -> args[0] = 25
     */
    private function handlePUTRequest()
    {
        // TODO
        if ($this->verb == null && sizeof($this->args) == 1) { //URL Überprüfung (ID)
            $model = Purchase::get($this->args[0]);
            if ($model == null) { //überprüfen, ob Eintrag existiert, wenn ID mitgegeben
                $this->response("Data not found", 404);
            } else {
                $model = Purchase::get($this->args[0]);
                $model->setDate($this->getDataOrNull('date'));
                $model->setCurrency($this->getDataOrNull('currency'));
                $model->setAmount($this->getDataOrNull('amount'));
                $model->setPrice($this->getDataOrNull('price'));

                if ($model->save()) {
                    $this->response("OK");
                } else {
                    $this->response($model->getErrors(), 400);
                }
            }
        } else {
            $this->response("Not Found", 404);
        }
    }

    /**
     * delete purchase: DELETE api.php?r=purchase/25 -> args[0] = 25
     */
    private function handleDELETERequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {
            Purchase::delete($this->args[0]);
            $this->response("OK", 200);
        } else {
            $this->response("Not Found", 404);
        }
    }
}
