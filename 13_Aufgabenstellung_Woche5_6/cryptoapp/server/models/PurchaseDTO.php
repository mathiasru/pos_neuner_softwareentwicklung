<?php

class PurchaseDTO implements JsonSerializable
{

    private $currency;
    private $amount;
    private $price;

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * define attributes which are part of the json output
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return [
            "currency" => $this->currency,
            "amount" => doubleval($this->amount),
            "price" => doubleval($this->price),
        ];
    }
}