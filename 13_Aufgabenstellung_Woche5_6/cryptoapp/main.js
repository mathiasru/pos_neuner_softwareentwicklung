const app = Vue.createApp({
    data() {
        return {
            purchases: [],
            prices: []
        }
    },
    mounted() {

        // GetAll Purchases
        this.loadPurchases()

        // GetActualPrices
        axios.get('https://api.bitpanda.com/v1/ticker')
            .then(response => (this.prices = response.data))
            .catch(error => console.log(error))
    },
    methods: {
        loadPurchases() {
            axios.get('http://10.211.55.4/cryptoapp/api/purchase')
                .then(response => (this.purchases = response.data))
                .catch(error => console.log(error))
        },
        addPurchase(request) {
            //Purchase in Datenbank schreiben
            axios.post('http://10.211.55.4/cryptoapp/api/purchase', request).then(function (response) {
                console.log(response)
            }).finally(this.loadPurchases)
                .catch(function (error) {
                    console.log(error)
                })
        }

    }
})
