app.component('wallet-list', {
    props: {
        purchases: {
            type: Array,
            required: true
        },
        prices: {
            type: Array,
            required: true
        }
    },
    watch: {
        //schauen ob sich prop ändert - gesamte Seite wird neu geladen, warum?
        purchases: function () {
            this.getWallet()
        },
    },
    template:
    /*html*/
        `<div class="wallet-list">
            <h3>Wallets: {{Math.round(getWalletValue()*100)/100 + '€ (' + getWalletProfitOrLoss() + '%)'}}</h3>
        <p v-for="entry in wallet">{{entry.amount + ' ' + entry.currency + ' ' + Math.round((calculateCurrentValue(entry))*100)/100 + '€ (' + getEntryProfitOrLoss(entry) + '%)'}}</p> <!-- Vorherigen Wert übergeben?-->
    </div>
    `,
    data() {
        return {
            wallet: []
        }
    },
    methods: {
        getWallet() {
            //Alle gekauften Währungen holen
            this.wallet = []; //Einträge zurücksetzen
            let currencies = new Set(); //damit keine doppelten Currency-Einträge enthalten sind
            for (let purchase of this.purchases) {
                currencies.add(purchase.currency)
            }

            //Einträge holen
            for (let currency of currencies) {
                axios.get('http://10.211.55.4/cryptoapp/api/purchase/currency/' + currency)
                    .then(response => (this.wallet.push(response.data)))
                    .catch(error => console.log(error))
            }
        },
        calculateCurrentValue(entry) {
            for (let price in this.prices) {
                if (price === entry.currency) {
                    let currentPrice = this.prices[price].EUR
                    return currentPrice*entry.amount
                }
            }
        },
        calculateProfitOrLoss(investedValue, currentValue) {
            let profitOrLoss = Math.round((((currentValue)/investedValue)-1)*100*100)/100
            if (profitOrLoss >= 0) {
                return "+" + profitOrLoss
            } else {
                return profitOrLoss
            }
        },
        getWalletValue() {
            let walletValue = 0
            for (let entry of this.wallet) {
                let currentValue = this.calculateCurrentValue(entry)
                walletValue += currentValue //Aufsummieren einer Position zum Gesamtwert
            }
            return walletValue
        },
        getEntryProfitOrLoss(entry) {
            let currentValue = this.calculateCurrentValue(entry)
            return this.calculateProfitOrLoss(entry.price, currentValue)
        },
        getWalletProfitOrLoss() {
            let investedValue = 0
            for (let entry of this.wallet) {
                investedValue += entry.price
            }
            let walletValue = this.getWalletValue()
            return this.calculateProfitOrLoss(investedValue, walletValue)
        }
    }
})
