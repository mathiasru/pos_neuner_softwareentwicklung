app.component('app-display', {
    props: {
        prices: {
            type: Array,
            required: true
        },
        purchases: {
            type: Array,
            required: true
        }
    },
    data() {
        return {}
    },
    template:
    /*html*/
        `<div class="wallet-display">
        <div class="wallet-container">
            <purchase-form :prices="prices"></purchase-form>
            <wallet-list :purchases="purchases" :prices="prices"></wallet-list>
          </div>
    </div>
    `,
    methods: {
    }
})
