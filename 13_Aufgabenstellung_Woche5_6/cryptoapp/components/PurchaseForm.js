app.component('purchase-form', {
    props: {
        prices: {
            type: Array,
            required: true
        }
    },
    template:
    /*html*/
        `<form class="wallet-form" @submit.prevent="onSubmit">
    <h3>Cryptowährung kaufen</h3>
    
    <label for="currency">Cryptowährung:</label>
    <select id="currency" v-model="currency" >
      <option v-for="(price, index) in prices" :value="{currency: index, price: price.EUR}">{{index + ' ' + price.EUR}}€</option>
    </select>
    
    <label for="amount">Menge:</label>
    <input type="number" id="amount" v-model="amount" step="0.0001">
    
    <p>Wert:{{ getCurrentPrice }}€</p>

    <input class="button" type="submit" value="Kaufen">  
  </form>
  `,
    data() {
        return {
            currency: null,
            amount: ''
        }
    },
    methods: {
        onSubmit() {
            if (this.currency == null || this.amount === '' || this.amount <= 0) {
                alert('Formular nicht vollständig oder falscher Betrag. Bitte füllen Sie alle Felder aus.')
                return
            }
            let purchase = {
                currency: this.currency.currency,
                amount: this.amount,
                price: this.currency.price
            }

            this.createPurchase(purchase)

            this.currency = ''
            this.amount = ''
        },
        createPurchase(purchase) {
            //Form-Request behandeln
            let currency = purchase.currency
            let price = parseFloat(purchase.price)
            let amount = parseFloat(purchase.amount)
            let timestamp = this.getDateTimeNow();

            //JSON-Objekt erstellen
            let request = {
                date: timestamp,
                amount: parseFloat(amount),
                price: parseFloat(price),
                currency: currency
            }

            //senden des Requests an Parent-Komponente (main.js)
            this.$parent.$emit('add-to-wallet', request)
        },
        getDateTimeNow() {
            let dateTimeLocal = new Date().toLocaleString()
            dateTimeLocal = dateTimeLocal.replace(",", "")
            let timestamp = dateTimeLocal.split(" ")
            let date = timestamp[0].split(".")
            date = date[2] + '-' + (date[1].length < 2 ? ('0' + date[1]) : date[1]) + '-' + (date[0].length < 2 ? ('0' + date[0]) : date[0])
            return date + ' ' + timestamp[1]
        }
    },
    computed: {
        getCurrentPrice(){
            if (this.currency == null || this.amount === ''|| this.amount < 0) {
                return  0
            } else {
                return Math.round((parseFloat(this.currency.price) * parseFloat(this.amount))*100)/100
            }
        }
    }
})
