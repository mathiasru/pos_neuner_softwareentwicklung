-- Adminer 4.8.0 MySQL 5.5.5-10.6.5-MariaDB-1:10.6.5+maria~focal dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `cryptoapp` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `cryptoapp`;

DROP TABLE IF EXISTS `purchase`;
CREATE TABLE `purchase`
(
    `id`       int(11)     NOT NULL AUTO_INCREMENT,
    `date`     datetime    NOT NULL,
    `amount`   double      NOT NULL,
    `price`    double      NOT NULL,
    `currency` varchar(64) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

INSERT INTO `purchase` (`id`, `date`, `amount`, `price`, `currency`)
VALUES (1, '2021-02-01 09:07:19', 0.01, 25000, 'BTC'),
       (2, '2021-01-25 19:07:19', 0.01, 20000, 'BTC'),
       (16, '2021-02-11 18:01:40', 0.2, 1482.35, 'ETH'),
       (17, '2021-02-11 18:11:13', 0.2, 1476.76, 'ETH'),
       (18, '2021-02-12 07:58:36', 0.03, 39161.95, 'BTC'),
       (19, '2021-02-12 08:00:12', 0.6, 1461.01, 'ETH');

-- 2022-04-12 12:27:39
