const app = Vue.createApp({
    //mindesten ein Obtion Object
    data() {
        return {
            cart: 0,
            product: 'Socks',
            brand: 'Vue Mastery',
            description: 'I love my socks!',
            image: './assets/images/socks_green.jpg',
            url: 'https://www.google.com',
            inventory: 1,
            onSale: true,
            details: ['50% cotton', '30% wool', '20% polyester'],
            variants: [
                { id: 2234, color: 'green', image: './assets/images/socks_green.jpg'},
                { id: 2235, color: 'blue', image: './assets/images/socks_blue.jpg'},
            ]
        }
    },
    methods: {
        addToChart(){
            this.cart += 1
            this.inventory -= 1
        },
        removeFromChart(){
            if (this.cart > 0){
                this.cart -= 1
                this.inventory += 1
            }
        },
        updateImage(variantImage){
            this.image = variantImage
        }
    },
    computed: {
        title(){
            //sehr performant (wird gecached)
            return this.brand + ' ' + this.product
        }
    }
})
