const app = Vue.createApp({
    data() {
        return {
            cart: [],
            premium: true,
            prices: []
        }
    },
    mounted(){
        axios.get('http://10.211.55.4/cryptoapp/api/purchase').then(response => (this.prices = response)).catch(error => console.log(error))
    },
    methods: {
        updateCart(id) {
            this.cart.push(id)
        },
        removeById(id) {
            const index = this.cart.indexOf(id)
            if (index > -1) {
                this.cart.splice(index, 1)
            }
        }
    }
})

