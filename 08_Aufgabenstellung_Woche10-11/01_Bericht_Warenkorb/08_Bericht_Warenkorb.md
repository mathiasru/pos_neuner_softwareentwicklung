# Webshop Bericht und Testprotokoll

Das gesamte Programm wurde in Partnerarbeit umgesetzt (Rudig, Tilg).
<br>
<br>

# Inhaltsverzeichnis
- [Webshop Bericht und Testprotokoll](#webshop-bericht-und-testprotokoll)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Konfiguration Webserver](#konfiguration-webserver)
  - [Einstellungen PhpStorm](#einstellungen-phpstorm)
- [Lösungsaufbau](#lösungsaufbau)
  - [Klassen](#klassen)
    - [Book](#book)
    - [Cart](#cart)
    - [Cartitem](#cartitem)
- [Implementierung des Webshops](#implementierung-des-webshops)
  - [Laden der Daten](#laden-der-daten)
  - [Formulare](#formulare)
  - [Objektorientierung](#objektorientierung)
- [Klassendiagramm](#klassendiagramm)
- [Testprotokoll](#testprotokoll)
<br>
<br>

# Konfiguration Webserver

- Virtuelle Maschine mit Linux Ubuntu Server
- Docker Container lt. docker-compose.yml
```yml
  version: '2'
  services:
  apache:
    image: thecodingmachine/php:7.2-v2-apache-node10
    container_name: apache
    restart: unless-stopped
    ports:
      - 80:80
    environment:
            PHP_EXTENSION_PGSQL: 1
            PHP_EXTENSION_XDEBUG: 1
            PHP_EXTENSION_MONGODB: 1
            PHP_EXTENSION_MONGO: 1
            PHP_INI_ERROR_REPORTING: E_ALL
            APACHE_EXTENSION_AUTOINDEX: 1

    volumes:
      - ./www:/var/www/html
```

- sftp installieren
    - sudo apt-get install vsftpd
- Verbindung von PHPStorm zu VM mittels sftp
    - unter Tools Deployment anlegen
- Verbindung zum Host Zugangsdaten
    - Benutzer: mathiasrudig
    - Passwort: Standard ohne !
<br>
<br>

## Einstellungen PhpStorm

- SSH-Verbindung --> Terminal (Tools - Start SSH Session)
- FTP-Verbindung --> Remote Host (Tools - Deployment - Browse Remote Host)
- SSH-Verbindung eintragen (Tools - Deployment - Configuration - Connection)
- Pfad mappen (Tools - Deployment - Configuration - Mappings)
- Upload bei Safe (Settings - Deployment - Options - Always)
<br>
<br>

# Lösungsaufbau
## Klassen
### Book
* Properties: 
    id, title, price, stock, statische Liste von Büchern
* Behaviours:
    getAll() gibt alle Bücher zurück
    get($id) gibt ein bestimmtes Buch zurück

### Cart
* Properties: 
    Liste von Chartitems
* Behaviours:
    loadCookie() ladet gespeichrten Cookie die eine Liste von Chartitems beinhaltet
    saveCookie() speichert Cookie die eine Liste von ChartItems beinhaltet
    add(\$chartitem) fügt bestimmtes Chartitem hinzu
    remove($id) entfernt ein Chartitem aus dem Warenkorb

### Cartitem
(Wird zum Zwischenspeichern verwendet)
* Properties: book, pieces
- Seiten laut Wireframe aufbauen
- Bereitgestellte JSON-Datei mit file_get_content() einlesen und mittels `json_decode()` Parsen.
- Prüfen ob Daten geladen werden können
- Ausgewählte Bücher(Chartitems) mittels POST und Cookie (serialisiert) abspeichern um Warenkorb zu füllen.

# Implementierung des Webshops

## Laden der Daten

Durch die Methode `json_decode()` können die gegebenen Daten im JSON Format in ein assoziatives Array umgewandelt werden. Daraufhin wird eine Liste aller Bücher erstellt, die über die statische getAll Methode erreicht werden kann.

```php
public static function getAll() {
  if (file_exists(Book::FILENAME)) {
    $jsonInput = file_get_contents(Book::FILENAME);
    $jsonDecode = json_decode($jsonInput, true);
    //Objekt erzeugen
    foreach ($jsonDecode as $books) {
      Book::$bookList[] = new Book($books['id'], $books['title'], $books['price', $books['stock']);
    }
    return Book::$bookList;
  } else {
    Book::$errorMessage['fileNotFound'] = "Das File konnte nicht gefundenwerden!";
    return null;
  }
}
```

## Formulare

Im Zuge der Auflistung aller Artikel, in `index.php` wird für jedes Buch ein eigenes Formular verwendet. Dies liegt daran, dass über die Formularverarbeitung ein neues Cartitem Objekt, anhand der ID des jeweiligen Buches und der ausgewählten Menge erstellt wird.

```php
if (isset($_POST['submit'])) { //Formularverarbeitung
    $book = Book::get($_POST['bookId']);
    if ($cart->validate($book, $_POST['pieces'])){
        $cart->add($book, $_POST['pieces']); //gleichzeitig speichern ins Cookie
    }
}
```

Nach dem gleiche Prinzip funktioniert die Auflistung der Cartitems in unserem Warenkorb (`cart.php`). Nur wird an dem ausgewählten Objekt nicht die add, sondern die remove Methode aufgerufen.

```php
if (isset($_POST['submit'])) { //Formularverarbeitung
    if ($cart->ifBookExists($_POST['bookid'])){
        $cart->remove($_POST['bookid']);
    }
}
```

## Objektorientierung

Der Kernpunkt des Programms ist der Warenkorb, der mithilfe eines Cookies clientseitig gespeichert wird. In unserem Fall wird nicht der gesamte Warenkorb, sondern das Datenfeld `$cartItems` im Cookie gespeichert, das eine Liste aus Chartitems hält. Diese bestehen wiederum jeweils aus einem Buch und einer Anzahl.

Bei Aufruf einer Seite wird ein Warenkorb Objekt erstellt und druch die `setCartItems()` Methode werden die cartItems aus dem Cookie geladen (ruft `loadCookie()` auf).

Nachdem ein Artikel dem Warenkorb hinzugefügt wurde, wird aus einem Buch und der ausgewählten Anzahl ein Cartitem erzeugt und dieses dem Warenkorb hinzugefügt. Solle das ausgewählte Buch bereits im Warenkorb liegen wird die Menge des Artikels addiert. Sollte der Lagerstand nicht großgenug sein, wird eine Fehlermeldung ausgegeben.

Sowohl beim Hinzufügen eines Artikels in den Warenkorb, als auch beim Entfernen wird jeweils das Cookie überschrieben.

# Klassendiagramm

![UML_Diagramm.png](08_Bericht_Webshop_Bilder/UML_Diagramm.png)

# Testprotokoll

Artikelliste:</br>
![Artikelliste.PNG](08_Bericht_Webshop_Bilder/Artikelliste.PNG)

Warenkorb/Einkaufwagen:</br>
![Warenkorb.PNG](08_Bericht_Webshop_Bilder/Warenkorb.PNG)

Überschrittener Lagerbestand:</br>
![FehlerLagerbestand.PNG](08_Bericht_Webshop_Bilder/FehlerLagerbestand.PNG)

Keine Daten gefunden:</br>
![FehlerKeineDaten.PNG](08_Bericht_Webshop_Bilder/FehlerKeineDaten.PNG)

Keine Stückzahl ausgewählt:</br>
![FehlerKeineMenge.PNG](08_Bericht_Webshop_Bilder/FehlerKeineMenge.PNG)
