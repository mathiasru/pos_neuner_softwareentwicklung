<?php
require_once "./models/Book.php";
require_once "./models/Cart.php";
require_once "./models/CartItem.php";

$bookId = '';
$pieces = '';
$errors = [];

$cart = new Cart(); //ertellen eines Warenkorbes und laden aus Cookie mittels Konstruktors
$bookList = Book::getAll(); //Position wichtig

if (isset($_POST['submit'])) { //Formularverarbeitung
    $bookId = isset($_POST['bookId']) ? $_POST['bookId'] : '';
    $pieces = isset($_POST['pieces']) ? $_POST['pieces'] : '';

    $book = Book::get($bookId);
    $cart->add($book, $pieces); //Validieren und gleichzeitiges Speichern ins Cookie
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <title>Warenkorb</title>
    <script type="application/javascript" src="js/index.js"></script>
</head>
<body>
<div class="mt-5 container" style="width: 1000px">
    <div class="row mt-3 mb-3">
        <div class="col-sm-9">
            <h2>Bücher</h2>
        </div>
        <div class="col-sm-2">
            <div class="container" align="right">
                <a href="cart.php"><img src="./img/warenkorb.png" style="width: 60px" alt="Warenkorb"/></a>
            </div>
        </div>
        <div class="col-sm-1">
            <div class="container mt-3 " align="left">
                <h5><?= $cart->getCartSize() ?></h5>
            </div>
        </div>
    </div>
    <?php
    if (!empty(Book::getErrorMessage())) {
        $errors[] = Book::getErrorMessage();
    }
    if (!empty(Cart::getErrorMessage())) {
        $errors[] = Cart::getErrorMessage();
    }
    if (!empty($errors)) {
        echo "<div class='alert-danger form-control'>";
        foreach ($errors as $error) {
            foreach ($error as $value) {
                echo "<p>" . $value . "</p><br>";
            }
        }
        echo "</div>";
    }
    ?>
    <table type="table" class="table table-striped mt-3" style="width: 100%">
        <thead>
        <tr>
            <th style="width: 30%">Buchtitel</th>
            <th style="width: 25%">Preis</th>
            <th style="width: 25%">Menge</th>
            <th style="width: 20%"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($bookList != null) {
            foreach ($bookList as $books) {
                echo "<form action='index.php' method='post' id='hinzufügen'>";
                $id = $books->getID();
                echo "<tr>";
                echo "<td>" . $books->getTitle() . "</td>";
                echo "<td>" . $books->getPrice() . "€</td>";
                echo "<td><select name='pieces' id='pieces' class='custom-select' required style='max-width: 100px'>"; // required validieren
                echo "<option hidden value=''>Anzahl</option>";
                for ($i = 1; $i <= $books->getStock(); $i++) {
                    echo "<option value='$i'>$i</option>";
                }
                echo "<input type='hidden' name='bookId' value='$id'>";
                echo "<td><input type='submit' name='submit' value='hinzufügen' class='btn btn-secondary'></td>";
                echo "</tr>";
                echo "</form>";
            }
        } else {
            echo "<tr></tr>";
        }
        ?>
        </tbody>
    </table>
</div>
</body>
</html>