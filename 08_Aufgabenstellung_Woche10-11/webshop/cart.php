<?php
require_once "./models/Book.php";
require_once "./models/Cart.php";
require_once "./models/CartItem.php";

$bookId = '';

$cart = new Cart(); //ertellen eines Warenkorbes und laden aus Cookie mittels Konstruktors
$bookList = Book::getAll(); //Position wichtig

if (isset($_POST['submit'])) { //Formularverarbeitung
    $bookId = isset($_POST['bookid']) ? $_POST['bookid'] : '';
    $cart->remove(intval($bookId)); //Validieren und gleichzeitiges Speichern ins Cookie
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <title>Warenkorb</title>
    <script type="application/javascript" src="js/index.js"></script>
</head>
<body>
<div class="mt-5 container" style="width: 1000px">

    <div class="row mt-3 mb-3">
        <div class="col-sm-6">
            <h2>Einkaufswagen</h2>
        </div>
        <div class="col-sm-6">
            <div class="container" align="right">
                <a href="index.php" class="btn btn-secondary">Zurück</a>
            </div>
        </div>
    </div>

    <table type="table" class="table table-striped mt-3" style="width: 100%">
        <thead>
        <tr>
            <th style="width: 30%">Buchtitel</th>
            <th style="width: 25%">Preis</th>
            <th style="width: 25%">Menge</th>
            <th style="width: 20%"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $items = $cart->getCartitems();
        if ($items != null) {
            foreach ($items as $item) {
                echo "<form action='cart.php' method='post' id='entfernen'>";
                $book = Book::get($item->getBook()->getId());
                if ($book == null){
                    exit();
                }
                $id = $book->getId();
                $price = $book->getPrice();
                $pieces = $item->getPieces();
                echo "<tr>";
                echo "<td>" . $book->getTitle() . "</td>";
                echo "<td>" . $price . "€</td>";
                echo "<td>" . $pieces . "</td>";
                echo "<input type='hidden' name='bookid' value='$id'>";
                echo "<td><input type='submit' name='submit' value='entfernen' class='btn btn-secondary'></td>";
                echo "</tr>";
                echo "</form>";
            }
        } else {
            echo "<tr></tr>";
        }
        ?>
        </tbody>
    </table>

    <div class="container mt-5" align="right">
        <h5>Gesamtsumme: € <?= $cart->getChartSum() ?></h5>
    </div>

</div>
</body>
</html>