<?php

class Cart
{
    // Datenfelder
    private $cartitems = [];
    private static $errorMessage = [];

    public function __construct()
    {
        $this->loadCookie();
    }

    /**
     * Läd die Daten im Cookie in das Array $cartitems (Datenfeld). Mithilfe der unserialize() Methode wird ein der
     * serialisierte String in Objekte umgewandelt.
     * Dies funktioniert nur, wenn das bereits Cookie bereits gesetzt wurde.
     */
    private function loadCookie()
    {
        if (isset($_COOKIE['cart'])) {
            $this->cartitems = unserialize($_COOKIE['cart']); //überschreiben
        }
    }

    /**
     * Speichert das Array $cartitems (Datenfeld) serialisiert, über die Methode serialize(), als String in ein Cookie,
     * das dann eine Stunde existiert.
     * Falls das Cookie bereits existiert, wird es mit der dem aktuellen Datenfeld überschrieben.
     */
    private function saveCookie()
    {
        setcookie('cart', serialize($this->cartitems), time() + 3600, '/');
    }

    /**
     * Über die Funktion, lässt sich ein Buch mit einer bestimmten Menge in Form eines CartItem-Objektes hinzufügen.
     * Zusätzlich werden die übergebenen Daten zuvor validiert und der Lagerstand geprüft.
     * @param $book übergebenes Objekt vom Typ Book.
     * @param $pieces übergene Menge vom Typ int.
     */
    public function add($book, $pieces)
    {
        if ($this->validate($book, $pieces)) {
            $cartitem = new CartItem($book, $pieces);
            if ($this->checkStock($cartitem)) {
                $this->cartitems[] = $cartitem; //neu hinzufügen
            }
            $this->saveCookie(); //speichern
        }
    }

    /**
     * Überprüft ob ein CartItem vorhanden ist und berechnet die Menge und überschreibt diese ggf.. Bei Überschreitung
     * des Lagerstandes, wird eine Meldung in das Error-Array hinzugefügt und die Menge auf den maximalen Lagerstand gesetzt
     * und ein false zurückgeliefert. Wenn kein bestehender Eintrag vom jeweiligen CartItem existiert, wird ein true zurückgeliefert.
     * @param $cartitem übergebenes Objekt vom Typ CartItem.
     * @return bool gibt bei vorhandenem Eintrag ein false zurück.
     */
    private function checkStock($cartitem)
    {
        foreach ($this->cartitems as $item) {
            $book = Book::get($item->getBook()->getId());
            if ($book != null && $item->getBook()->getId() == $cartitem->getBook()->getId()) {
                if (($item->getPieces() + $cartitem->getPieces() <= $book->getStock())) {
                    $item->setPieces($item->getPieces() + $cartitem->getPieces());
                    return false;
                } else {
                    Cart::$errorMessage['maxStockLimit'] = "Die maximale Anzahl wurde überschritten, es wurde die maximale Menge gewählt!";
                    $item->setPieces($book->getStock()); // maximalen Stock setzen
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Die Funktion gewährleistet das Löschen von ChartItems anhand der übergebenen Id. Zuvor wird geprüft, ob ein
     * Eintrag mit der übergebenen Id im Cart vorhanden ist.
     * @param $id übergebene id vom Typ int.
     */
    public function remove($id)
    {
        if ($this->ifBookExists($id)) {
            foreach ($this->cartitems as $item) {
                if (intval($id) == $item->getBook()->getId()) {
                    $index = array_search($item, $this->cartitems); // sucht Objekt und gibt index zurück
                    unset($this->cartitems[$index]);
                    break;
                }
            }
            $this->saveCookie(); //speichern
        }
    }

    /**
     * Die Funktion ruf alle Validierungsfunktionen auf und gibt einen boolschen-Wert zurück.
     * @param $book übergebenes Objekt vom Typ Book.
     * @param $pieces übergene Menge vom Typ int.
     * @return bool
     */
    public function validate($book, $pieces)
    {
        return $this->validateBooks($book) & $this->validatePieces($pieces);
    }

    /**
     * Die Funktion prüft ob das übergebene Objekt null ist und schreibt ggf. eine Fehlermeldung in das Error-Array.
     * @param $book übergebenes Objekt vom Typ Book.
     * @return bool
     */
    public function validateBooks($book)
    {
        if ($book == null) { // Validierung
            Cart::$errorMessage['noBookSelected'] = "Es wurde kein gültiges Buch hinzugefügt!";
            return false;
        } else {
            return true;
        }
    }

    /**
     * Die Funktion prüft ob die übergebene Menge nummerisch und größer 0 ist und schreibt ggf. eine Fehlermeldung
     * in das Error-Array.
     * @param $pieces übergene Menge vom Typ int.
     * @return bool liefert bei gültiger Anzahl ein true zurück.
     */
    public function validatePieces($pieces)
    {
        if (intval($pieces) <= 0 && !is_numeric($pieces)) { // Validierung
            Cart::$errorMessage['noPiecesSelected'] = "Es wurde keine Stückzahl ausgewählt!";
            return false;
        } else {
            return true;
        }
    }

    /**
     * Die Funktion prüft anhand der übergebenen id ob ein Eintrag vorhanden ist und schreibt ggf. eine Fehlermeldung
     * @param $id übergebene id vom Typ int.
     * @return bool liefert bei vorhandenen Einträgen ein true zurück.
     */
    public function ifBookExists($id)
    {
        foreach ($this->cartitems as $item) {
            if (intval($id) == $item->getBook()->getId()) {
                return true;
            }
        }
        Cart::$errorMessage['bookDoesntExist'] = "Es gibt kein Buch mit der ausgewählten Id!";
        return false;
    }

    /**
     * Die Funktion gibt die Menge der Artikel im Warenkorb zurück.
     * @return int gibt die Menge der Artikel im Warenkorb zurück.
     */
    public function getCartSize()
    {
        $cartSize = 0;
        foreach ($this->cartitems as $item) {
            $cartSize += intval($item->getPieces());
        }
        return $cartSize;
    }

    /**
     * Die Funktion gibt die Gesamtsumme des Warenkorbes als float zurück.
     * @return float|int gibt die Gesamtsumme des Warenkorbes als float zurück.
     */
    public function getChartSum()
    {
        $sum = 0;
        foreach ($this->cartitems as $item) {
            $book = Book::get($item->getBook()->getId());  //Buch aus Datenbank holen, sonst manipulierbar
            $sum += floatval($book->getPrice() * intval($item->getPieces()));
        }
        return $sum;
    }

    /**
     * @return array
     */
    public function getCartitems()
    {
        return $this->cartitems;
    }

    /**
     * @return array
     */
    public static function getErrorMessage()
    {
        return self::$errorMessage;
    }
}