<?php

class CartItem
{
    //Datenfelder
    private $book = '';
    private $pieces = '';

    public function __construct($book, $pieces)
    {
        $this->book = $book;
        $this->pieces = $pieces;
    }

    /**
     * @return string
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @param string $book
     */
    public function setBook($book)
    {
        $this->book = $book;
    }

    /**
     * @return string
     */
    public function getPieces()
    {
        return $this->pieces;
    }

    /**
     * @param string $pieces
     */
    public function setPieces($pieces)
    {
        $this->pieces = $pieces;
    }
}