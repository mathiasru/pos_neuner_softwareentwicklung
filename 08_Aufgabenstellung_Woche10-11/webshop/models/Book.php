<?php

class Book
{
    //Datenfelder
    private $id = '';
    private $title = '';
    private $price = '';
    private $stock = '';

    const FILENAME = "./data/bookdata.json"; // Dateipfad
    private static $bookList = [];
    private static $errorMessage = [];

    public function __construct($id, $title, $price, $stock)
    {
        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
        $this->stock = $stock;
    }

    /**
     * Liest aus der gegebenen JSON-Datei die Daten ein und speichert sie in ein assoziatives Array.
     * Aus jedem Element in diesem Array wird ein Buch Objekt erstellt, welche wiederum in ein statisches Array
     * gespeichert werden. Somit kann über die Klasse auf eine Liste aller Bücher zugegriffen werden.
     * Falls die Datei gar nicht existiert, wird eine Fehlermeldung als Error Message gespeichert.
     * @return array mit allen Bücherobjekten, wenn Daten gefunden wurden.
     * @return null wenn keine Datei vorhanden.
     */
    public static function getAll()
    {
        if (file_exists(Book::FILENAME)) {
            $jsonInput = file_get_contents(Book::FILENAME);
            $jsonDecode = json_decode($jsonInput, true);
            //Objekt erzeugen
            foreach ($jsonDecode as $books) {
                Book::$bookList[] = new Book($books['id'], $books['title'], $books['price'], $books['stock']);
            }
            return Book::$bookList;
        } else {
            Book::$errorMessage['fileNotFound'] = "Das File konnte nicht gefunden werden!";
            return null;
        }
    }

    /**
     * Sucht in der Bücherliste nach dem Buch mit der mitgegebenen ID. Wenn kein Eintrag gefunden wurde oder die
     * Bücherliste leer ist, wird eine Fehlermeldung als Error Message gespeichert.
     * @param $id
     * @return mixed ($book) dessen ID, mit der ID eines Buches aus der Bücherliste übereinstimmt.
     * @return null wenn kein Eintrag in der Bücherliste existiert oder das gesuchte Buch nicht in der Liste gefunden
     * wurde.
     */
    public static function get($id)
    {
        if (!empty(Book::$bookList)) {
            foreach (Book::$bookList as $book) {
                if ($book->getId() == intval($id)) {
                    return $book;
                }
            }
            Book::$errorMessage['bookNotFound'] = "Das Buch konnte nicht gefunden werden!";
            return null;
        } else {
            Book::$errorMessage['noEntry'] = "Keine Einträge in unserem System!";
            return null;
        }
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param string $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return array
     */
    public static function getErrorMessage()
    {
        return self::$errorMessage;
    }

}