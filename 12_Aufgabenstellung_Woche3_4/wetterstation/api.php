<?php

require_once('controllers/RESTController.php');

$route = isset($_GET['r']) ? explode('/', trim($_GET['r'], '/')) : ['station'];
$controller = sizeof($route) > 0 ? $route[0] : 'station';

if ($controller == 'measurement') {
    require_once('controllers/MeasurementRESTController.php');

    try {
        (new MeasurementRESTController())->handleRequest();
    } catch(Exception $e) { //Exception aus RESTController Konstruktor
        RESTController::responseHelper($e->getMessage(), $e->getCode()); //Exception Message wird als JSON ausgegeben
    }
} else if ($controller == 'station') {
    require_once('controllers/StationRESTController.php');
    try {
        (new StationRESTController())->handleRequest();
    } catch(Exception $e) {
        RESTController::responseHelper($e->getMessage(), $e->getCode());
    }
} else {
    RESTController::responseHelper('REST-Controller "' . $controller . '" not found', '404');
}
