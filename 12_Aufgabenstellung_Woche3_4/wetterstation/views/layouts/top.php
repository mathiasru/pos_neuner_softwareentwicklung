<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Wetterstation</title>

    <link rel="shortcut icon" href="css/favicon.ico" type="image/x-icon">
    <link rel="icon" href="css/favicon.ico" type="image/x-icon">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/chart.js"></script> <!--  TODO Einbinden von chart.js -->
    <script src="js/index.js"></script> <!--  TODO Einbinden von index.js -->

</head>
<body>
