<div class="row">
    <div class="col-md-3">
        <div class="form-group required <?= $model['measurement']->hasError('time') ? 'has-error' : ''; ?>">
            <label class="control-label">Zeitpunkt *</label>
            <input type="text" class="form-control" name="time" value="<?= $model['measurement']->getTime() ?>">

            <?php if ($model['measurement']->hasError('time')): ?>
                <div class="help-block"><?= $model['measurement']->getError('time') ?></div>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-2">
        <div class="form-group required <?= $model['measurement']->hasError('temperature') ? 'has-error' : ''; ?>">
            <label class="control-label">Temperatur [°C] *</label>
            <input type="number" step=".1" class="form-control" name="temperature" value="<?= $model['measurement']->getTemperature() ?>">

            <?php if ($model['measurement']->hasError('temperature')): ?>
                <div class="help-block"><?= $model['measurement']->getError('temperature') ?></div>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-2">
        <div class="form-group required <?= $model['measurement']->hasError('rain') ? 'has-error' : ''; ?>">
            <label class="control-label">Regen [ml] *</label>
            <input type="text" class="form-control" name="rain" value="<?= $model['measurement']->getRain() ?>">

            <?php if ($model['measurement']->hasError('rain')): ?>
                <div class="help-block"><?= $model['measurement']->getError('rain') ?></div>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-2">
        <div class="form-group required <?= $model['measurement']->hasError('station_id') ? 'has-error' : ''; ?>">
            <label class="control-label">Station *</label>
            <select class="form-control" name="station_id" style="width: 200px">

                <?php
                echo '<option selected hidden value="' . $model['measurement']->getStationId() . '">' . $model['measurement']->getStation()->getName() . '</option>';
                foreach($model['stations'] as $station):
                    echo '<option value="' . $station->getId() . '">' . $station->getName() . '</option>';
                endforeach;
                ?>
            </select>

            <?php if ($model['measurement']->hasError('station_id')): ?>
                <div class="help-block"><?= $model['measurement']->getError('station_id') ?></div>
            <?php endif; ?>
        </div>
    </div>
</div>

