var myChart = null; //global setzen um später Chart prüfen und löschen zu können
var measuremets = null;

$(document).ready(function () { //body on load function, wenn Seite geladen ist
    loadMeasurementsByStation();
    fillChart(measuremets);

    $("#btnSearch").click(function () {
        loadMeasurementsByStation();
        fillChart(measuremets);
    });
});

function parseMeasurementsTable(data) {
    var tmp = "";
    $.each(data, function (index, measurements) {
        tmp += "<tr>";
        tmp += "<td>" + measurements.time + "</td>";
        tmp += "<td>" + measurements.temperature + "</td>";
        tmp += "<td>" + measurements.rain + "</td>";
        tmp += "<td>";
        tmp += '<a class="btn btn-info" href="index.php?r=measurement/view&id=' + measurements.id + '"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;'
        tmp += '<a class="btn btn-primary" href="index.php?r=measurement/update&id=' + measurements.id + '"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;'
        tmp += '<a class="btn btn-danger" href="index.php?r=measurement/delete&id=' + measurements.id + '"><span class="glyphicon glyphicon-remove"></span></a>';
        tmp += "</td>";
        tmp += "</tr>";
    });
    return tmp;
}

function loadMeasurementsByStation() {
    var stationId = $("#stationId").val(); //Wert aus Eingabefeld auslesen
    $.ajax({
        type: "GET",
        url: "api/station/" + stationId + "/measurement",
        success: function (data) {
            measuremets = data;
            $("#measurements").html(parseMeasurementsTable(data));
        },
        async: false
    });

    /* Asyncron und daher kann der Wert der Variable nicht gesetzt werden
    $.get("api/station/" + stationId + "/measurement", function (data) {
        $("#measurements").html(parseMeasurementsTable(data)); //Daten in Form von String auslesen und an die Tabelle schicken
    });*/
}

function fillChart(data) {
    let time = [];
    let temperature = [];
    let rain = [];

    var numberOfMeasurements = $("#numberOfMeasurements").val(); //Wert aus Eingabefeld auslesen
    //var stationId = $("#stationId").val(); //Wert aus Eingabefeld auslesen
    //$.get("api/station/" + stationId + "/measurement", function (data) { //daten per REST holen

        if (myChart != null) {
            myChart.destroy(); //Chart löschen um neuen zu erstellen (sonst überlappen sich beide)
        }

        $.each(data, function (index, measurements) {
            time.push(measurements.time);
            temperature.push(parseFloat(measurements.temperature));
            rain.push(parseFloat(measurements.rain));
        });

        //nur bestimmte Anzahl der letzen Einträge filtern
        time = calculateLastEntries(time, numberOfMeasurements);
        temperature = calculateLastEntries(temperature, numberOfMeasurements);
        rain = calculateLastEntries(rain, numberOfMeasurements);

        showChart(time, temperature, rain); //Daten an Chart senden und anzeigen
    //});

}

function calculateLastEntries(array, entries) {
    const data = [];
    let position = 0;
    for (i = array.length - entries; i < array.length; i++) {
        data[position] = array[i];
        position++;
    }
    return data;
}

function showChart(time, temperature, rain) {
    const ctx = document.getElementById('chart').getContext('2d');
    myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: time,
            datasets: [{
                label: 'Temperatur °C',
                data: temperature,
                backgroundColor: ['rgba(107,214,0,0.8)'],
                borderColor: ['rgba(255, 99, 132, 1)'],
                yAxisID: 'temperature',
                borderWidth: 1
            },
                {
                    label: 'Regen mm',
                    data: rain,
                    backgroundColor: 'rgba(0,129, 218, 0.8)',
                    borderColor: 'rgb(75, 192, 192)',
                    yAxisID: 'rain',
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                temperature: {
                    display: true,
                    position: 'left',
                },
                rain: {
                    display: true,
                    position: 'right',
                }
            }
        }
    });
}