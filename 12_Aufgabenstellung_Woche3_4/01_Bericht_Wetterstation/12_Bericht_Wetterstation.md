# Wetterstation Bericht und Testprotokoll

Das gesamte Programm wurde in Partnerarbeit umgesetzt (Rudig, Tilg).
<br>

# Inhaltsverzeichnis
- [Wetterstation Bericht und Testprotokoll](#wetterstation-bericht-und-testprotokoll)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Konfiguration Webserver](#konfiguration-webserver)
  - [Einstellungen PhpStorm](#einstellungen-phpstorm)
- [Lösungsansatz](#lösungsansatz)
  - [Datenbank](#datenbank)
  - [Grafisch](#grafisch)
  - [REST-API](#rest-api)
  - [JavaScript](#javascript)
- [Implementierung des Wetterstation](#implementierung-des-wetterstation)
  - [UML Diagramm](#uml-diagramm)
  - [Datenbank importieren](#datenbank-importieren)
  - [MVC](#mvc)
    - [Models](#models)
      - [Datenbankanbindung](#datenbankanbindung)
    - [Controller implementieren](#controller-implementieren)
      - [Controller](#controller)
      - [REST-Controller](#rest-controller)
    - [Views](#views)
  - [JavaScript, JQuery und ChartJS](#javascript-jquery-und-chartjs)
- [Testprotokoll](#testprotokoll)
  - [REST-API](#rest-api-1)
  - [Javascript](#javascript-1)
<br>
<br>
# Konfiguration Webserver

- Virtuelle Maschine mit Linux Ubuntu Server
- Docker Container lt. docker-compose.yml
```yml
  version: '2'
  services:
  apache:
    image: thecodingmachine/php:7.2-v2-apache-node10
    container_name: apache
    restart: unless-stopped
    ports:
      - 80:80
    environment:
            PHP_EXTENSION_PGSQL: 1
            PHP_EXTENSION_XDEBUG: 1
            PHP_EXTENSION_MONGODB: 1
            PHP_EXTENSION_MONGO: 1
            PHP_INI_ERROR_REPORTING: E_ALL
            APACHE_EXTENSION_AUTOINDEX: 1

    volumes:
      - ./www:/var/www/html
```

- sftp installieren
    - sudo apt-get install vsftpd
- Verbindung von PHPStorm zu VM mittels sftp
    - unter Tools Deployment anlegen
- Verbindung zum Host Zugangsdaten
    - Benutzer: mathiasrudig
    - Passwort: Standard ohne !
<br>
<br>

## Einstellungen PhpStorm

- SSH-Verbindung --> Terminal (Tools - Start SSH Session)
- FTP-Verbindung --> Remote Host (Tools - Deployment - Browse Remote Host)
- SSH-Verbindung eintragen (Tools - Deployment - Configuration - Connection)
- Pfad mappen (Tools - Deployment - Configuration - Mappings)
- Upload bei Safe (Settings - Deployment - Options - Always)
<br>
<br>

# Lösungsansatz

## Datenbank

* Anlegen
* Tabellen und Datensätze importieren
* Zeitstempel mittels ID abändern (Interwall)
* Datenbankverbindung in Projekt anpassen

## Grafisch

* Views von Stationen und Messwerte erweitern respektive von Grund implementieren
* Fehlende Inputs aktualisieren (chart,js, index.js)

## REST-API

* REST-Schnittstelle von Stationen und Messwerte implementieren

## JavaScript

* Mittels jQuery und JavaScript Restanfragen per Buttonklick implementieren
* Chartjs einbinden

# Implementierung des Wetterstation

## UML Diagramm

![UML Diagramm](12_Bericht_Wetterstation_Bilder/UML.png)

## Datenbank importieren
* SQL - File importieren
* Zeitstempel updaten

```SQL
UPDATE measurement
SET    measurement.time = DATE_SUB(measurement.time,INTERVAL (measurement.id) SECOND);
WHERE  measurement.time = '2022-03-08 09:41:58';
```

## MVC

### Models

Das Station Model war bereits vollständig gegeben. Beim Station Measurement mussten die Datenbankoperationen ergänzt werden.

#### Datenbankanbindung

* PDO-Verbindung einrichten (Singelton Pattern)
* Datenbankverbindung herstellen
* SQL-Statement definieren
* Statement vorbereiten (`prepare()`)
* Wenn Parameter im Statement vorhanden, diese setzen (`bindParam()`)
* Statement ausführen (`execute()`)
* Wenn Datensätze erwartet werden, Daten Fetchen (zB durch `fetchObject()`)
* Datenbankverbindung schließen

### Controller implementieren

Sowohl der MeasurementController also auch der MeasurementRESTController waren in der Angabe bereits vollständig ausimplementiert. Ergänzt werden mussen die Methoden im StationController und StationRESTController.

#### Controller

* `handelRequest()` Methode ausimplementieren - je nach Routen-Parameter entsprechende Methode aufrufen
* Je nach CRUD-Vorgang Logik implementieren
  - `actionIndex()` und `actionView()` - Datensätze zurückliefern
  - `actionCreate()` - wird zweimal aufgerufen, einmal zum Laden des Views und einmal, wenn POST-Request vorhanden, für die Formularverarbeitung und das speichern des Datensatzes
  - `actionUpdate()` - wird zweimal aufgerufen, einmal zum Laden des Views und einmal, wenn POST-Request vorhanden, für die Formularverarbeitung und das Aktualisieren des Datensatzes
  - `actionDelete()` - wird zweimal aufgerufen, einmal zum Laden des Views und einmal, wenn POST-Request vorhanden, für das Löschen des Datensatzes

#### REST-Controller

* `handelRequest()` Methode ausimplementieren - je nach HTTP-Mothode entsprechende Controllermethode aufrufen
*  Je nach HTTP-Mothode CRUD Logik implementieren
    - `handleGETRequest()` - prüfen ob mittels URL alle, nur einer, oder alle Datensätze einer Station abgefragt werden und die jeweiligen Daten zurückliefern
    - `handlePOSTRequest()` - Datensatz speichern
    - `handlePUTRequest()` - Datensatz aktualisieren
    - `handleDELETERequest()` - Datensatz löschen

### Views

Für die RUD Operationen der Messwerte mussten noch die Views ergänzt werden.
* _form.php - entspricht einem Formular (auslagern aufgrund der Wiederverwendbarkeit)
* view.php - anzeigen des Datensatzes
* update.php - aktualisieren des Datensatzes
* delete.php - löschen des Datensatzes

## JavaScript, JQuery und ChartJS
Um die Daten auf der Seite anzeigen zu lassen, wurde diesmal mit jQuery und JavaScript gearbeitet um die Daten im Hintergrund per **REST-Call** abzufragen und nur Abschnitte der Seite neu laden zu müssen. Zusätzlich wurde unter Verwendung der Biblipthek ChartJS eine Grafik implementiert, welche diese Werte in Form eines Liniendiagrammes darstellt.
* Javascript Dateien anlegen (`index.js`, `chart.js` -> Bibliothek aus dem internet)
* Scripte mittels script-Tag im layout.top einbinden
* JQuery für die Events `$("#btnSearch").click` und den REST-Call verwenden
* Daten über Tabelle an die View senden
* ChartJS laut Doku einbinden und Daten mittels Array übergeben
* ein Erstellter Chart muss gelöscht werden (`chart.destroy()`), wenn ein neuer auf selber Position eingebunden werden soll (überlappen der Charts)

Um die Daten nur einmal Laden zu müssen, muss eine synchrone Abfrage implementiert werden. Innerhalb dieser Abfrage können die Daten einer globalen Variable zugewiesen werden und verwendet werden.

```javascript
function loadMeasurementsByStation() {
    var stationId = $("#stationId").val(); //Wert aus Eingabefeld auslesen
    jQuery.ajax({
        type: "GET",
        url: "api/station/" + stationId + "/measurement",
        success: function (data) {
            measuremets = data;
            $("#measurements").html(parseMeasurementsTable(data));
        },
        async: false
    });
}
```

Befüllen des Charts 
```javascript
function fillChart(data) {
    let time = [];
    let temperature = [];
    let rain = [];

    var numberOfMeasurements = $("#numberOfMeasurements").val(); //Wert aus Eingabefeld auslesen

        if (myChart != null) {
            myChart.destroy(); //Chart löschen um neuen zu erstellen (sonst überlappen sich beide)
        }

        $.each(data, function (index, measurements) {
            time.push(measurements.time);
            temperature.push(parseFloat(measurements.temperature));
            rain.push(parseFloat(measurements.rain));
        });

        //nur bestimmte Anzahl der letzen Einträge filtern
        time = calculateLastEntries(time, numberOfMeasurements);
        temperature = calculateLastEntries(temperature, numberOfMeasurements);
        rain = calculateLastEntries(rain, numberOfMeasurements);

        showChart(time, temperature, rain); //Daten an Chart senden und anzeigen

}
```


# Testprotokoll

## REST-API

Abfragen eines Messwertes inkl. Station (GET)

![REST GET Measurement_with_Station](12_Bericht_Wetterstation_Bilder/REST_GET_Measurement_with_Station.png)

Abfragen aller Station (GET)

![REST GET Station](12_Bericht_Wetterstation_Bilder/REST_GET_Station.png)

Schreiben eines neuen Messwertes ohne Werte (POST)

![REST POST error](12_Bericht_Wetterstation_Bilder/REST_POST_error.png)

Löschen eines nicht vorhandenen Messwertes (DELETE)

![REST DELETE notfound](12_Bericht_Wetterstation_Bilder/REST_DELETE_notfound.png)

## Javascript
Anzeige nach Laden der Seite

![View](12_Bericht_Wetterstation_Bilder/View.png)

Erfolgreiches Laden der Daten per JavaScript

![View Script](12_Bericht_Wetterstation_Bilder/View_Script.png)