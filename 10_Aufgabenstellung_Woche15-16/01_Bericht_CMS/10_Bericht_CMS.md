# CMS-System Bericht und Testprotokoll

Das gesamte Programm wurde in Partnerarbeit umgesetzt (Rudig, Tilg).
<br>

# Inhaltsverzeichnis
- [CMS-System Bericht und Testprotokoll](#cms-system-bericht-und-testprotokoll)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Konfiguration Webserver](#konfiguration-webserver)
  - [Einstellungen PhpStorm](#einstellungen-phpstorm)
- [Lösungsansatz](#lösungsansatz)
  - [Datenbank modelieren](#datenbank-modelieren)
  - [Klassen implementieren](#klassen-implementieren)
    - [User](#user)
    - [Contribution](#contribution)
    - [Datenbankanbindung](#datenbankanbindung)
  - [Darstellung implementieren](#darstellung-implementieren)
- [Implementierung des CMS-Systems](#implementierung-des-cms-systems)
  - [Laden der Daten](#laden-der-daten)
  - [Objektorientierung](#objektorientierung)
  - [Formulare](#formulare)
- [Testprotokoll](#testprotokoll)
<br>
<br>

# Konfiguration Webserver

- Virtuelle Maschine mit Linux Ubuntu Server
- Docker Container lt. docker-compose.yml
```yml
  version: '2'
  services:
  apache:
    image: thecodingmachine/php:7.2-v2-apache-node10
    container_name: apache
    restart: unless-stopped
    ports:
      - 80:80
    environment:
            PHP_EXTENSION_PGSQL: 1
            PHP_EXTENSION_XDEBUG: 1
            PHP_EXTENSION_MONGODB: 1
            PHP_EXTENSION_MONGO: 1
            PHP_INI_ERROR_REPORTING: E_ALL
            APACHE_EXTENSION_AUTOINDEX: 1

    volumes:
      - ./www:/var/www/html
```

- sftp installieren
    - sudo apt-get install vsftpd
- Verbindung von PHPStorm zu VM mittels sftp
    - unter Tools Deployment anlegen
- Verbindung zum Host Zugangsdaten
    - Benutzer: mathiasrudig
    - Passwort: Standard ohne !
<br>
<br>

## Einstellungen PhpStorm

- SSH-Verbindung --> Terminal (Tools - Start SSH Session)
- FTP-Verbindung --> Remote Host (Tools - Deployment - Browse Remote Host)
- SSH-Verbindung eintragen (Tools - Deployment - Configuration - Connection)
- Pfad mappen (Tools - Deployment - Configuration - Mappings)
- Upload bei Safe (Settings - Deployment - Options - Always)
<br>
<br>

# Lösungsansatz

## Datenbank modelieren

![ER_Diagramm.png](10_Bericht_CMS_Bilder/ER_Diagramm.png)

## Klassen implementieren

### User
* Properties: 
    id, username, password
* Behaviours:
    get(\$id) gibt einen bestimmten User zurück
    getUserByUsernameAndPassword(\$username, \$password) gibt einen User anhand des übergebenen Parametern \$usernamen und \$password zurück.
    isLoggedIn() prüft ob der User eingelogged ist oder nicht und gibt einen boolschen Wert zurück. (\$_SESSION)

### Contribution
* Properties: 
    id, title, content, release_date, users_id
* Behaviours:
    get(\$id) gibt einen bestimmten Eintrag zurück
    getAll() gibt alle Einträge zurück
    create() erzeugt einen neuen Eintrag
    update(\$id) updated einen Eintrag anhand der übergebene \$id
    delete(\$id) löscht einen Eintrag anhand der übergebene \$id

### Datenbankanbindung
* PDO-Verbindung einrichten (Singelton Pattern)

## Darstellung implementieren
 Darstellung anhand des bereitgestellten Codes/ Mockup implementieren.
 Wichtige Punkte:
 * Authentifizierung und Authorisierung
 * Erstellen von Einträgen nur durch Login
 * Bearbeiten und Löschen nur vom User erstellte Einträge

# Implementierung des CMS-Systems

## Laden der Daten

Mithilfe der Klasse `DatabaseAccess` kann eine Datenbankverbindung aufgebaut werden. Durch die Implementierung nach Singleton Pattern, kann immer nur eine Datenbankverbindung zur gleichen Zeit existieren. Bevor eine neue Verbindung aufgebaut wird, muss also die alte geschlossen werden.

Zugriff auf die Datenbank und somit das Laden der Daten geschieht im Model (Active Record Pattern). Über das Interface `DatabaseObject` werden alle CRUD-Methoden vorgegeben. 

## Objektorientierung

Die Applikation arbeitet mit zwei Objekten, den Usern und den Beiträgen (Contributions). Im Model selbst wird wie erwähnt die Datenbankkommuniktaion abgewickelt. Ein Beitrag besitzt immer eine user_id um festzustellen, welchem User dieser gehört. Die komplette Logik der Objekte wird in den Klassen abgewickelt, um diese von der Darstellung zu trennen. Auch die serverseitige Validierung der Daten, die die jeweilige Klasse betreffen werden darn abgewickelt.

## Formulare

Neben den Überprüfungen zur Autorisierung und Authentifizierung (Login) wird nur die Formularverarbeitung in den einzelnen Views abgewickelt. Nach der clientseitigen Validierung im Formular und der serverseitigen Überprüfung über die Klassen, wird entweder eine Fehlermeldungen ausgegeben oder an die benötigte Location weitergeleitet.


<!-- # Klassendiagramm

![Klassendiagramm.png](10_Bericht_CMS_Bilder/Klassendiagramm.png) -->

# Testprotokoll

Unautorisierter Zugang:</br>
![401.png](10_Bericht_CMS_Bilder/401.png)


Beitrag nicht gefunden:</br>
![404.png](10_Bericht_CMS_Bilder/404.png)


Falscher GET Parameter oder Wert:</br>
![400.png](10_Bericht_CMS_Bilder/400.png)


Kein Parameter:</br>
![400.png](10_Bericht_CMS_Bilder/400_2.png)


Serverseitige Validierung:</br>
![Validierung.png](10_Bericht_CMS_Bilder/Validierung.png)