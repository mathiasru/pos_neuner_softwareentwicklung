<?php

require_once "DatabaseAccess.php";

class User
{

    private $id = 0;
    private $username = '';
    private $password = '';

    private $errors = [];

    public static function get($id)
    {
        $db = DatabaseAccess::connect();
        $sql = "SELECT * FROM users WHERE id = :id";

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');

        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_CLASS);
        DatabaseAccess::disconnect();
        return $user !== false ? $user : null;
    }

    public static function getAll()
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM users ORDER BY id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->execute(); // in Datenbank schreiben
        // ORM - erzeugen eines Users
        $users = $stmt->fetchAll(PDO::FETCH_CLASS, 'User'); // ORM
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        return $users;
    }

    public static function getUserByUsernameAndPassword($username, $password)
    {
        $db = DatabaseAccess::connect();
        $sql = "SELECT * FROM users WHERE username = :username AND password = :password";

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':password', $password);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');

        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_CLASS);
        DatabaseAccess::disconnect();
        return $user !== false ? $user : null;
    }

    public static function isLoggedIn()
    {
        if (isset($_SESSION['userid'])){
            return true;
        } else {
            return false;
        }
    }


    public function validateUsername($username)
    {
        if (strlen($username) < 5) {
            $this->errors['username'] = "Der Benutzername muss mindestens 5 Zeichen lang sein";
            return false;
        } else if (strlen($username) > 20) {
            $this->errors['username'] = "Der Benutzername darf nicht mehr als 20 lang sein";
            return false;
        } else if ($this->ifUsernameExists($username)) {
            $this->errors['username'] = "Dieser Username ist bereits vergeben";
            return false;
        } else {
            return true;
        }
    }

    private function ifUsernameExists($username)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM users WHERE username = :username";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':username', $username);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User'); // Klasse für ORM definieren
        $stmt->execute(); // in Datenbank schreiben
        $user = $stmt->fetch(PDO::FETCH_CLASS); // ORM - erzeugen eines Users
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        // Wenn kein User gefunden wurde, wird NULL zurückgegeben
        return $user !== false ? true : false;
    }

    public function validatePassword($password)
    {
        if (strlen($password) < 8) {
            $this->errors['password'] = "Passwortlänge beachten (min. 8 Zeichen)";
            return false;
        } else {
            return true;
        }
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        if ($this->validateUsername($username)) {
            $this->username = $username;
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        if ($this->validatePassword($password)) {
            $this->password = $password;
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }
}