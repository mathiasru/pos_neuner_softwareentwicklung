<?php

require_once "DatabaseAccess.php";
require_once "DatabaseObject.php";

class Contribution implements DatabaseObject
{

    private $id = 0;
    private $titel = '';
    private $content = '';
    private $release_date = '';
    private $user_id = '';

    private $errors = [];

    /**
     * @inheritDoc
     */
    public function create()
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "INSERT INTO contributions (titel, release_date, content, user_id) values (:titel, :release_date, :content, :user_id)";
        $stmt = $db->prepare($sql);
        //Schutz vor Dependency Injection
        $stmt->bindParam(':titel', $this->titel);
        $stmt->bindParam(':release_date', $this->release_date);
        $stmt->bindParam(':content', $this->content);
        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->execute(); // In Datenbank schreiben
        $lastId = $db->lastInsertId();  // ID vom zuletz gespeicherten Datensatz
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        return $lastId;
    }

    /**
     * @inheritDoc
     */
    public function update()
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "UPDATE contributions SET titel = :titel, release_date = :release_date, content = :content, user_id = :user_id WHERE id = :id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':id', $this->id);
        $stmt->bindParam(':titel', $this->titel);
        $stmt->bindParam(':release_date', $this->release_date);
        $stmt->bindParam(':content', $this->content);
        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->execute(); // in Datenbank schreiben
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
    }

    /**
     * @inheritDoc
     */
    public static function get($id)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM contributions WHERE  id = :id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':id', $id);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Contribution'); // Klasse für ORM definieren
        $stmt->execute(); // in Datenbank schreiben
        $contribution = $stmt->fetch(PDO::FETCH_CLASS); // ORM - erzeugen eines Users
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        // Wenn kein User gefunden wurde, wird NULL zurückgegeben
        return $contribution !== false ? $contribution : null;
    }

    /**
     * @inheritDoc
     */
    public static function getAll()
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM contributions ORDER BY id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->execute(); // in Datenbank schreiben
        // ORM - erzeugen eines Users
        $contributions = $stmt->fetchAll(PDO::FETCH_CLASS, 'Contribution'); // ORM
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        return $contributions;
    }

    /**
     * @inheritDoc
     */
    public static function getAllByReleasedateOrUserId($UserId)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM contributions WHERE release_date <= NOW() OR user_id = :UserId ORDER BY id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':UserId', $UserId);
        $stmt->execute(); // in Datenbank schreiben
        // ORM - erzeugen eines Users
        $contributions = $stmt->fetchAll(PDO::FETCH_CLASS, 'Contribution'); // ORM
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        return $contributions;
    }

    /**
     * @inheritDoc
     */
    public static function getAllByReleasedate()
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM contributions WHERE release_date <= NOW() ORDER BY id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->execute(); // in Datenbank schreiben
        // ORM - erzeugen eines Users
        $contributions = $stmt->fetchAll(PDO::FETCH_CLASS, 'Contribution'); // ORM
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        return $contributions;
    }

    /**
     * @inheritDoc
     */
    public static function delete($id)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "DELETE FROM contributions WHERE id = :id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':id', $id);
        $stmt->execute(); // in Datenbank schreiben
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
    }


    public function validateData()
    {
        return $this->validateTitel($this->titel) & $this->validateContent($this->content) &
            $this->validateReleaseDate($this->release_date) & $this->validateUserId($this->user_id);
    }

    public function validateTitel($titel)
    {
        if (strlen($titel) == 0) { //strlen prüft die Länge eines Strings
            $this->errors['titel'] = "Titel darf nicht leer sein"; //schreiben in das globalle Array
            return false;
        } elseif (strlen($titel) > 100) {
            $this->errors['titel'] = "Titel darf nicht mehr als 100 Zeichen lang sein";
            return false;
        } else {
            return true;
        }
    }

    public function validateContent($content)
    {
        if (strlen($content) == 0) { //strlen prüft die Länge eines Strings
            $this->errors['content'] = "Inhalt darf nicht leer sein"; //schreiben in das globalle Array
            return false;
        } elseif (strlen($content) > 1000) {
            $this->errors['content'] = "Inhalt darf nicht mehr als 1000 Zeichen lang sein";
            return false;
        } else {
            return true;
        }
    }

    public function validateReleaseDate($releaseDate)
    {
        $today = new DateTime();
        $today->setTime(0, 0, 0);

        try {
            if ($releaseDate == "") {
                $this->errors['releaseDate'] = "Freigabedatum darf nicht leer sein";
                return false;
            } else if ($today > new DateTime($releaseDate)) {
                $this->errors['releaseDate'] = "Freigabedatum darf nicht in der Vergangenheit liegen";
                return false;
            } else {
                return true;
            }
        } catch (Exception $exception) {
            $this->errors['releaseDate'] = "Freigabedatum ungültig";
            return false;
        }
    }

    public function validateUserId($userId)
    {
        if (!is_numeric($userId)) { //strlen prüft ob es sich um eine Zahl handelt
            $this->errors['UserId'] = "Ungültige User-Id"; //schreiben in das globale Array
            return false;
        } elseif (!$this->ifUserExists($userId)) {
            $this->errors['UserId'] = "User-ID nicht im System vorhanden.";
            return false;
        } else {
            return true;
        }
    }

    private function ifUserExists($userId)
    {
        //Alternativ: $user = User::get($userId); //Abhängigkeit!!

        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM users WHERE id = :user_id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':user_id', $userId);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User'); // Klasse für ORM definieren
        $stmt->execute(); // in Datenbank schreiben
        $user = $stmt->fetch(PDO::FETCH_CLASS); // ORM - erzeugen eines Users
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        // Wenn kein User gefunden wurde, wird NULL zurückgegeben
        return $user !== false ? true : false;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitel()
    {
        return $this->titel;
    }

    /**
     * @param string $titel
     */
    public function setTitel($titel)
    {
        $this->titel = $titel;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getReleaseDate()
    {
        return $this->release_date;
    }

    /**
     * @param string $release_date
     */
    public function setReleaseDate($release_date)
    {
        $this->release_date = $release_date;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param string $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }


}