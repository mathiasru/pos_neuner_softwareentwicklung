<?php
session_start();
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>CMS</title>

    <link rel="shortcut icon" href="css/favicon.ico" type="image/x-icon">
    <link rel="icon" href="css/favicon.ico" type="image/x-icon">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<body>

<?php
require_once "models/Contribution.php";
require_once "models/User.php";

$username = '';
$password = '';
$falseData = false;

if (isset($_POST['submit']) && !User::isLoggedIn()) {

    $username = isset($_POST['username']) ? $_POST['username'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] : '';

    $user = User::getUserByUsernameAndPassword($username, $password);
    if ($user != null) {
        $_SESSION['userid'] = $user->getId();
        header("Location: index.php");
        exit();
    } else {
        $falseData = true;
    }
} elseif (isset($_POST['submit']) && User::isLoggedIn()) {
    session_destroy();
    header("Location: index.php");
    exit();
}

?>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/CMS/index.php">CMS Ihres Vertrauens</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <?php if (User::isLoggedIn()) { ?>
                    <li><a href="/CMS/views/article/index.php">Beiträge</a></li>
                <?php } ?>
            </ul>

            <form class="navbar-form navbar-right" action="index.php" method="post">
                <?php if (User::isLoggedIn()) { ?>
                    <input type="submit"
                           name="submit"
                           class="btn btn-warning"
                           value="Abmelden"
                    />
                <?php } else { ?>
                    <input type="text"
                           name="username"
                           id="username"
                           placeholder="Benutzername"
                           class="form-control"
                           value=""
                           required
                    />
                    <input type="password"
                           name="password"
                           id="password"
                           placeholder="Passwort"
                           class="form-control"
                           value=""
                           required
                    />
                    <input type="submit"
                           name="submit"
                           class="btn btn-success"
                           value="Anmelden"
                    />
                    <?php
                    if ($falseData) {
                        echo "<button class='btn btn-danger disabled'>!</button>";
                    }
                    ?>
                <?php } ?>
            </form>

        </div><!--/.navbar-collapse -->
    </div>
</nav>
<br><br>

<div class="jumbotron">
    <div class="container">
        <h1>Das CMS Ihres Vertrauens!</h1>
        <p>Dieses CMS ist ein essenzielles Tool zum Erstellen von Beiträgen!</p>
        <p><a class="btn btn-primary btn-lg" href="/ebanking" role="button">Bank Ihres Vertrauens&raquo;</a></p>
    </div>
</div>

<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <?php
        $contributions = Contribution::getAllByReleasedate();
        foreach ($contributions as $contribution) {
            $id = $contribution->getID();
            echo "<div class='col-md-4'>";
            echo "<h2>" . $contribution->getTitel() . "</h2>";
            echo "<p>" . $contribution->getContent() . "</p>";
            echo "<p><a class='btn btn-default' href='views/article/viewNotLoggedIn.php?id=$id' role='button'>View details &raquo;</a></p>";
            echo "</div>";
        }
        ?>
    </div>

    <hr>

    <footer>
        <p>&copy; 2022 Zurcük, Inc.</p>
    </footer>
</div> <!-- /container -->

</body>
</html>