<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

<?php
include "../helper/navbar.php";

if (!User::isLoggedIn()) {
    echo "<div class='mt-5 h1 text-center'>401: Unautorisierter Zugang";
    exit();
}

$contribution = '';

//Prüfen, ob Beitrag vom jeweiligen User stammt
if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $contribution = Contribution::get($_GET['id']);
    if ($contribution == null) {
        echo "<div class='mt-5 h1 text-center'>404: Beitrag nicht Gefunden";
        exit();
    }
} elseif (empty($_GET) == 1) { //update.php?
    echo "<div class='mt-5 h1 text-center'>400: Kein Parameter</div>";
    exit();
} else { //anderer Parameter
    echo "<div class='mt-5 h1 text-center'>400: Falscher Parameter oder Wert</div>";
    exit();
}

$userid = $_SESSION['userid'];
if ($userid != $contribution->getUserId()) {
    echo "<div class='mt-5 h1 text-center'>401: Unautorisierter Zugang";
    exit();
}

if (isset($_POST['submitDelete'])) {
    Contribution::delete($_GET['id']);
    header('Location: index.php');
    exit();
}

?>

<div class="container">
    <h2>Beitrag löschen</h2>

    <form class="form-horizontal" action="delete.php?id=<?= $_GET['id']; ?>"  method="post">
        <input type="hidden" name="id" value="<?= $_GET['id']; ?>"/>
        <p class="alert alert-error">Wollen Sie den Beitrag wirklich löschen?</p>
        <div class="form-actions">
            <input type="submit" name="submitDelete" class="btn btn-danger" value="Löschen" />
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
</body>
</html>
