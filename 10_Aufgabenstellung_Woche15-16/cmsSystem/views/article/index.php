<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

<?php
include "../helper/navbar.php";

if (!User::isLoggedIn()) {
    echo "<div class='mt-5 h1 text-center'>401: Unautorisierter Zugang";
    exit();
}
$contributions = '';
$userid = $_SESSION['userid'];

?>

<div class="container">
    <div class="row">
        <h2>Beiträge</h2>
    </div>
    <div class="row">
        <p>
            <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
        </p>

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th width="16%">Titel</th>
                <th width="50%">Inhalt</th>
                <th width="10%">Besitzer</th>
                <th width="10%">Freigabedatum</th>
                <th width="14%"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $contributions = Contribution::getAllByReleasedateOrUserId($userid);
            foreach ($contributions as $contribution) {
                $contributionID = $contribution->getId();
                echo "<tr>";
                echo "<td>" . $contribution->getTitel() . "</td>";
                echo "<td>" . $contribution->getContent() . "</td>";
                echo "<td>" . User::get($contribution->getUserId())->getUsername() . "</td>";
                echo "<td>" . $contribution->getReleaseDate() . "</td>";
                ?>

                <td>
                    <a class='btn btn-info' href='view.php?id=<?= $contributionID ?>'>
                        <span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;

                    <!-- Prüfen ob Beitrag vom jeweiligen User stammt und bearbeiten bzw. löschen darf -->
                    <?php if ($contribution->getUserId() == $userid) { ?>
                    <a class="btn btn-primary" href="update.php?id=<?= $contributionID ?>">
                        <span class="glyphicon glyphicon-pencil"></span></a>&nbsp;

                    <a class="btn btn-danger" href="delete.php?id=<?= $contributionID ?>">
                        <span class="glyphicon glyphicon-remove"></span></a>
                    <?php } ?>
                </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>

    </div>
</div> <!-- /container -->
</body>
</html>