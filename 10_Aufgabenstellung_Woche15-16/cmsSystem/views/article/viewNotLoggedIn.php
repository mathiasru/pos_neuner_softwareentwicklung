<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

<?php
include "../helper/navbar.php";

$contribution = '';

//Überprüfen, ob ein User eingeloggt ist
if (isset($_SESSION['userid'])) {
    $userid = $_SESSION['userid'];
    $user = '';
    $user = $user = User::get($userid);
    $useridIsset = true; //sonst Fehlermeldung falls nicht gesetzt
} else {
    $useridIsset = false;
}

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $contribution = Contribution::get($_GET['id']);
    if ($contribution == null) {
        echo "<div class='mt-5 h1 text-center'>404: Beitrag nicht Gefunden";
        exit();
    }
} elseif (empty($_GET) == 1) { //update.php?
    echo "<div class='mt-5 h1 text-center'>400: Kein Parameter</div>";
    exit();
} else { //anderer Parameter
    echo "<div class='mt-5 h1 text-center'>400: Falscher Parameter oder Wert</div>";
    exit();
}

$sqldate = date_create($contribution->getReleaseDate());
$contributionDate = date_format($sqldate, "d.m.Y");

?>

<div class="container">
    <h2>Beitrag anzeigen</h2>

    <p>
        <?php if ($useridIsset) { //ist ein User eigeloggt
            if ($contribution->getUserId() == $userid) { ?> <!-- darf der User die Buttons sehen -->
            <a class="btn btn-primary" href="update.php?id=<?= $_GET['id']; ?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?= $_GET['id']; ?>">Löschen</a>
            <?php }
        } ?>
        <a class="btn btn-default" href="/CMS/index.php">Zurück</a> <!-- wie gelangt man zurück auf die Seite von der man kommt -->
    </p>

    <table class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>Titel</th>
            <td> <?= $contribution->getTitel(); ?> </td>
        </tr>
        <tr>
            <th>Freigabedatum</th>
            <td> <?= $contributionDate ?> </td>
        </tr>
        <tr>
            <th>Besitzer</th>
            <td> <?= User::get($contribution->getUserId())->getUsername(); ?> </td>
        </tr>
        <tr>
            <th>Inhalt</th>
            <td> <?= $contribution->getContent(); ?> </td>
        </tr>
        </tbody>
    </table>
</div> <!-- /container -->
</body>
</html>