<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

<?php
include "../helper/navbar.php";

if (!User::isLoggedIn()) {
    echo "<div class='mt-5 h1 text-center'>401: Unautorisierter Zugang";
    exit();
}

$users = ''; //für SelectBox
$contribution = '';

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $contribution = Contribution::get($_GET['id']);
    if ($contribution == null) {
        echo "<div class='mt-5 h1 text-center'>404: Beitrag nicht Gefunden</div>";
        exit();
    }
} elseif (empty($_GET) == 1) { //update.php?
    echo "<div class='mt-5 h1 text-center'>400: Kein Parameter</div>";
    exit();
} else { //anderer Parameter
    echo "<div class='mt-5 h1 text-center'>400: Falscher Parameter oder Wert</div>";
    exit();
}

$userid = $_SESSION['userid'];
if ($userid != $contribution->getUserId()) {
    echo "<div class='mt-5 h1 text-center'>401: Unautorisierter Zugang";
    exit();
}

if (isset($_POST['submitUpdate'])) {
    $contribution->setTitel(isset($_POST['title']) ? $_POST['title'] : '');
    $contribution->setContent(isset($_POST['content']) ? $_POST['content'] : '');
    $contribution->setReleaseDate(isset($_POST['releasedate']) ? $_POST['releasedate'] : '');
    $contribution->setUserId(isset($_POST['owner']) ? $_POST['owner'] : '');

    if ($contribution->validateData()) {
        $contribution->update();
        header("Location: /CMS/views/article/index.php");
        exit();
    } else {
        echo "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
        foreach ($contribution->getErrors() as $key => $value) {
            echo "<li>" . $value . "</li>";
        }
        echo "</ul></div>";
    }
}
?>

<div class="container">
    <div class="row">
        <h2>Beitrag bearbeiten</h2>
    </div>

    <form class="form-horizontal" action="update.php?id=<?= $_GET['id']; ?>" method="post">

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required">
                    <label class="control-label">Titel *</label>
                    <input
                            type="text"
                            class="form-control <?= isset($contribution->getErrors()['titel']) ? 'is-invalid' : '' ?>"
                            name="title"
                            maxlength="100"
                            value="<?= $contribution->getTitel(); ?>"
                            required
                    >
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required">
                    <label class="control-label">Freigabedatum *</label>
                    <input
                            type="date"
                            class="form-control <?= isset($contribution->getErrors()['releaseDate']) ? 'is-invalid' : '' ?>"
                            name="releasedate"
                            value="<?= $contribution->getReleaseDate(); ?>"
                            required
                    >
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="form-group required">
                    <label class="control-label">Besitzer *</label>
                    <select
                            class="form-control <?= isset($contribution->getErrors()['UserId']) ? 'is-invalid' : '' ?>"
                            name="owner">
                        <option value="<?= $contribution->getUserId() ?>"><?= User::get($contribution->getUserId())->getUsername() ?></option>
                        <?php
                        $users = User::getAll();
                        foreach ($users as $user) {
                            if ($user->getId() != $contribution->getUserId()) {
                                echo "<option value=" . $user->getId() . ">" . $user->getUsername() . "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group required">
                    <label class="control-label">Inhalt *</label>
                    <textarea
                            class="form-control <?= isset($contribution->getErrors()['content']) ? 'is-invalid' : '' ?>"
                            name="content"
                            maxlength="1000"
                            rows="10"
                            required><?= $contribution->getContent(); ?></textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <input type="submit" name="submitUpdate" class="btn btn-primary" value="Aktualisieren"/>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
</body>
</html>