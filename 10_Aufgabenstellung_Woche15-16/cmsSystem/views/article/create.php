<!DOCTYPE html>
<html lang="de">
<script type="application/javascript" src="../../js/article.js"></script>
<?php
include "../helper/head.php";
?>

<body>

<?php
include "../helper/navbar.php";

if (!User::isLoggedIn()) {
    echo "<div class='mt-5 h1 text-center'>401: Unautorisierter Zugang";
    exit();
}

$userid = $_SESSION['userid'];
$user = '';
$user = User::get($userid);

$contribution = new Contribution();

if (isset($_POST['submitCreate'])) {
    $contribution->setTitel(isset($_POST['title']) ? $_POST['title'] : '');
    $contribution->setContent(isset($_POST['content']) ? $_POST['content'] : '');
    $contribution->setReleaseDate(isset($_POST['releasedate']) ? $_POST['releasedate'] : '');
    $contribution->setUserId($userid); //wird aus der Session gesetzt (Angemeldeter User)

    //Validierung
    if ($contribution->validateData()){
        $contribution->create();
        header("Location: /CMS/views/article/index.php");
        exit();
    } else {
        echo "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
        foreach ($contribution->getErrors() as $key => $value) {
            echo "<li>" . $value . "</li>";
        }
        echo "</ul></div>";
    }
}
?>

<div class="container">
    <div class="row">
        <h2>Beitrag erstellen</h2>
    </div>

    <form class="form-horizontal" action="/CMS/views/article/create.php" method="post">

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required">
                    <label class="control-label">Titel *</label>
                    <input
                            type="text"
                            class="form-control <?= isset($contribution->getErrors()['titel']) ? 'is-invalid' : '' ?>"
                            name="title"
                            maxlength="100"
                            value="<?= $contribution->getTitel(); ?>"
                            required
                    >
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required">
                    <label class="control-label">Freigabedatum *</label>
                    <input
                            type="date"
                            class="form-control <?= isset($contribution->getErrors()['releaseDate']) ? 'is-invalid' : '' ?>"
                            name="releasedate"
                            value="<?= $contribution->getReleaseDate(); ?>"
                            required
                    >
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="form-group required">
                    <label class="control-label">Besitzer *</label>
                    <input
                            type="text"
                            class="form-control <?= isset($contribution->getErrors()['UserId']) ? 'is-invalid' : '' ?>"
                            name="owner"
                            value="<?= $user->getUsername(); ?>"
                            readonly
                    >
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group required">
                    <label class="control-label">Inhalt *</label>
                    <textarea
                            class="form-control <?= isset($contribution->getErrors()['content']) ? 'is-invalid' : '' ?>"
                            name="content"
                            maxlength="1000"
                            rows="10"
                            required
                    ><?= $contribution->getContent(); ?></textarea>
                </div>
            </div>
        </div>

        <div class="form-group">
            <input type="submit" name="submitCreate" class="btn btn-success" value="Erstellen"/>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->
</body>
</html>