<?php
require_once "../../models/Contribution.php";
require_once "../../models/User.php";

$username = '';
$password = '';
$falseData = false;

if (isset($_POST['submit']) && !User::isLoggedIn()) {

    $username = isset($_POST['username']) ? $_POST['username'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] : '';

    $user = User::getUserByUsernameAndPassword($username, $password);
    if ($user != null) {
        $_SESSION['userid'] = $user->getId();
        header("Location: /CMS/index.php");
        exit();
    } else {
        $falseData = true;
    }
} elseif (isset($_POST['submit']) && User::isLoggedIn()) {
    session_destroy();
    header("Location: /CMS/index.php");
    exit();
}

/*function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

$path = pathinfo($_SERVER['REQUEST_URI'], PATHINFO_FILENAME);*/

?>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/CMS/index.php">CMS ihres Vertrauens</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <?php if (User::isLoggedIn()) { ?>
                    <li><a href="/CMS/views/article/index.php">Beiträge</a></li>
                <?php } ?>
            </ul>

            <form class="navbar-form navbar-right" action="index.php" method="post">
                <?php if (User::isLoggedIn()) { ?>
                    <input type="submit"
                           name="submit"
                           class="btn btn-warning"
                           value="Abmelden"
                    />
                <?php } else { ?>
                    <input type="text"
                           name="username"
                           id="username"
                           placeholder="Benutzername"
                           class="form-control"
                           value=""
                           required
                    />
                    <input type="password"
                           name="password"
                           id="password"
                           placeholder="Passwort"
                           class="form-control"
                           value=""
                           required
                    />
                    <input type="submit"
                           name="submit"
                           class="btn btn-success"
                           value="Anmelden"
                    />
                    <?php
                    if ($falseData) {
                        echo "<button class='btn btn-danger disabled'>!</button>";
                    }
                    ?>
                <?php } ?>
            </form>

        </div><!--/.navbar-collapse -->
    </div>
</nav>
<br><br>