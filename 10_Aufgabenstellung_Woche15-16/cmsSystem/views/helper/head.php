<?php
session_start();
?>

<head>
    <meta charset="utf-8">
    <title>CMS</title>

    <link rel="shortcut icon" href="../../css/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../css/favicon.ico" type="image/x-icon">

    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/index.css" rel="stylesheet">
    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
</head>