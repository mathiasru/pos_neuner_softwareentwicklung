-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema cmsDB
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `cmsDB` ;

-- -----------------------------------------------------
-- Schema cmsDB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cmsDB` DEFAULT CHARACTER SET utf8 ;
USE `cmsDB` ;

-- -----------------------------------------------------
-- Table `cmsDB`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cmsDB`.`users` ;

CREATE TABLE IF NOT EXISTS `cmsDB`.`users` (
                                                   `id` INT NOT NULL AUTO_INCREMENT,
                                                   `username` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cmsDB`.`contributions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cmsDB`.`contributions` ;

CREATE TABLE IF NOT EXISTS `cmsDB`.`contributions` (

    `id` INT NOT NULL AUTO_INCREMENT,
    `titel` VARCHAR(255) NOT NULL,
    `content` TEXT NOT NULL,
    `release_date` DATE NOT NULL,
    `user_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_contributions_users_idx` (`user_id` ASC) VISIBLE,
    CONSTRAINT `fk_contributions_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `cmsDB`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT)
    ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;