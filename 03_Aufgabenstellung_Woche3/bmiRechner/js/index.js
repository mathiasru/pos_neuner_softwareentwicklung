/**
 * Die Funktion prüft auf ein gültiges Datum, welches nicht in der Zukunft liegen darf und gibt dem übergebenen element eine classe mit.
 * @param elem
 */
function validateDate(elem) {
    let today = new Date();
    today.setHours(0,0,0,0);

    let date = new Date (elem.value);
    date.setHours(0,0,0,0,) //es soll nur das Datum verglichen werden

    //Bootstrap bietet die Möglichkeit im nachhinein Klassen hinzuzufügen/zu entfernen
    if (date <= new Date()) {
        elem.classList.add("is-valid")
        elem.classList.remove("is-invalid")
    } else {
        elem.classList.add("is-invalid")
        elem.classList.remove("is-valid")
    }
}