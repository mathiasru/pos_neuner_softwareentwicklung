<?php

// ----------- Serverseitige Validierung ---------------

$errors = []; //Assoziatives Array als globale Variable
/**
 * Die Funktion führt eine Prüfung aller übergebener Parameter mittels Methodenaufrufen aus und gibt einen boolschen
 * Wert zurück.
 * @param $name
 * @param $messdatum
 * @param $groesse
 * @param $gewicht
 * @return bool gibt im Fehlerfall ein false zurück.
 */
function validate($name, $messdatum, $groesse, $gewicht){
    return validateName($name) & validatemessdatum($messdatum) & validategroesse($groesse) & validateGewicht($gewicht);
}

/**
 * Die Funktion überprüft den übergebenen String auf Gültigkeit.
 * @param $name
 * @return bool gibt im Fehlerfall ein false zurück.
 */
function validateName($name) {
    global $errors;

    if (strlen($name) == 0) {
        $errors['name'] = "Name darf nicht leer sein.";
        return false;
    } else if (strlen($name) > 25) {
        $errors['name'] = "Name darf nicht länger als 25 Zeichen sein.";
        return false;
    } else {
        return true;
    }
}

/**
 * Die Funktion prüft auf ein gültiges Datum, welches nicht leer oder in der Zukunft liegen darf.
 * @param $messdatum
 * @return bool gibt im Fehlerfall ein false zurück.
 */
function validateMessdatum($messdatum) {
    global $errors;

    try {
        if ($messdatum == '') {
            $errors['messdatum'] = "Das Datum darf nicht leer sein.";
            return false;
        } else if (new DateTime() < new DateTime($messdatum)) {
            $errors['messdatum'] = "Das Datum darf nicht in der Zukunft liegen.";
            return false;
        } else {
            return true;
        }
    } catch (Exception $e) {
        $errors['messdatum'] = "Ungültiges Datum.";
        return false;
    }
}

/**
 * Die Funktion überprüft den übergebene Zahl auf Gültigkeit.
 * @param $groesse
 * @return bool gibt im Fehlerfall ein false zurück.
 */
function validateGroesse($groesse)
{
    global $errors;

    if ($groesse <= 0) {
        $errors['groesse'] = "Groesse kann nicht <= 0 sein.";
        return false;
    } else if ($groesse > 300) {
        $errors['groesse'] = "So groß ist niemand!";
        return false;
    } else {
        return true;
    }
}

/**
 * Die Funktion überprüft den übergebene Zahl auf Gültigkeit.
 * @param $gewicht
 * @return bool gibt im Fehlerfall ein false zurück.
 */
function validateGewicht($gewicht)
{
    global $errors;

    if ($gewicht <= 0) {
        $errors['gewicht'] = "Gewicht kann nicht <= 0 sein.";
        return false;
    } else if ($gewicht > 300) {
        $errors['gewicht'] = "So schwer ist niemand!";
        return false;
    } else {
        return true;
    }
}

// ------------- BMI-Rechner ---------------
/**
 * Die Funktion berechnet den BMI auf eine Kommastelle und gibt diesen zurück.
 * @param $groesse
 * @param $gewicht
 * @return float gibt den berechneten, auf eine Kommastelle greundeten BMI zurück.
 */
function berechneBMI($groesse, $gewicht) {
    $gerundeterBMI = round(($gewicht/(($groesse/100)**2)), 1 ,PHP_ROUND_HALF_UP);
    return $gerundeterBMI;
}

/**
 * Die Funktion ermittelt einen Infotext, welcher auf dem Wert des übergebenen basiert.
 * @param $gerundeterBMI
 * @return string gibt die ermittelte infoBMI in Form eines Strings zurück
 */
function infoBMI($gerundeterBMI) {
    if ($gerundeterBMI < 18.5){
        return "Untergewicht";
    } else if($gerundeterBMI <= 24.9){
        return "Normalgewicht";
    } else if($gerundeterBMI <= 29.9){
        return "Übergewicht";
    } else {
        return "Adipositas";
    }
}

/**
 * Die Funktion ermittelt aus dem übergebenen String die CSS-Klasse, und gibt diese als string zurück.
 * @param $berechneterBMI
 * @return string ermittelte CSS-Klasse vom typ string.
 */
function alarmFarbe($berechneterBMI) {
    switch ($berechneterBMI){
        case 'Untergewicht':
            return 'alert alert-warning';
        case 'Normalgewicht':
            return 'alert alert-success';
        case 'Übergewicht':
            return 'alert alert-warning';
        case 'Adipositas':
            return 'alert alert-danger';
        default:
            return 'alert alert-danger';
    }
}