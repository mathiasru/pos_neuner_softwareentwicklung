<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"><!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous"> <!-- einbinden der Bootstrap CSS Datei-->
    <title>BMI-Rechner</title>
    <script type="text/javascript" src="js/index.js"></script> <!-- Einbinden der JavaScript-Datei -->
</head>
<body>
<div class="container">
    <h1 class="mt-5 mb-3">Body-Mass-Index-Rechner</h1>

    <?php

    require "lib/func.inc.php";
    //Variablen deklarieren, da sonst beim initialisieren der Seite ein Fehler auftritt
    $name = "";
    $messdatum = "";
    $groesse = "";
    $gewicht = "";
    //mittels isset prüfen ob die Felder Gestzt wurden
    if (isset($_POST['submit'])) {
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $messdatum = isset($_POST['messdatum']) ? $_POST['messdatum'] : '';
        $groesse = isset($_POST['groesse']) ? $_POST['groesse'] : '';
        $gewicht = isset($_POST['gewicht']) ? $_POST['gewicht'] : '';

        if (validate($name, $messdatum, $groesse, $gewicht)) {
            $berechneterBMI = berechneBMI($groesse, $gewicht); //gerundet auf eine Nachkommastelle
            $bmiInfo = infoBMI($berechneterBMI);
            echo "<p class='".alarmFarbe($bmiInfo)."'>BMI: ".$berechneterBMI." - ". $bmiInfo."</p>";
        } else {
            echo "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
            foreach ($errors as $key => $value) { //$errors wird aus dem Script(func.inc.php) verwendet (Warnung kann ignoriert werden)
                echo "<li>" . $value . "</li>";
            }
            echo "</ul></div>";
        }

    }
    ?>

    <form action="index.php" method="post" id="form-grade"> <!-- action definiert wohin das Formular per method(POST) geschickt wird -->

        <div class="row"> <!-- Übergeordnete Reihe (wegen Textfeld)-->

            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-8 form-group">
                        <label for="name">Name*</label>
                        <input type="text"
                               name="name"
                               class="form-control <?= isset($errors['name']) ? 'is-invalid' : '' ?>"
                               maxlength="25"
                               required
                               value="<?= htmlspecialchars($name) ?>"
                        /><!-- htmlspecialchars wird zum Schutz vor Code Einschleusungen verwendet -->

                    </div>

                    <div class="col-sm-4 form-group">
                        <label for="messdatum">Messdatum*</label>
                        <input type="date"
                               name="messdatum"
                               class="form-control <?= isset($errors['messdatum']) ? 'is-invalid' : '' ?>"
                               required
                               onchange="validateDate(this)"
                               value="<?= htmlspecialchars($messdatum) ?>"
                        /><!-- ruft eine JS-Funktion auf und übergibt das aktuelle Element -->
                    </div>

                </div>

                <div class="row mt-3">
                    <div class="col-sm-6 form-group">
                        <label for="groesse">Größe (cm)*</label>
                        <input type="number"
                               name="groesse"
                               class="form-control <?= isset($errors['groesse']) ? 'is-invalid' : '' ?>"
                               required
                               min="1"
                               max="300"
                               value="<?= htmlspecialchars($groesse) ?>"
                        />
                    </div>

                    <div class="col-sm-6 form-group">
                        <label for="gewicht">Gewicht (kg)*</label>
                        <input type="number"
                               name="gewicht"
                               class="form-control <?= isset($errors['gewicht']) ? 'is-invalid' : '' ?>"
                               required
                               min="1"
                               max="300"
                               value="<?= htmlspecialchars($gewicht) ?>"
                        />
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-sm-3 form-group">
                        <input type="submit"
                               name="submit"
                               class="btn btn-primary w-100"
                               value="Speichern"/>
                    </div>

                    <div class="col-sm-3 form-group">
                        <a href="index.php" class="btn btn-secondary w-100">Löschen</a>
                    </div>
                </div>

            </div>

            <div class="col-sm-3">
                <h3>Infos zum BMI:</h3>
                <p>Unter 18.5 Untergewicht <br>
                    18.5 - 24.9 Normalgewicht <br>
                    25.0 - 29.9 Übergewicht <br>
                    30.0 und darüber Adipositas
                </p>
            </div> <!-- Testfeld rechts -->

        </div>

    </form>
</div>
</body>
</html>