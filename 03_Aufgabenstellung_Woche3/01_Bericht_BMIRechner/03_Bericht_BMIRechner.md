# BMIRechner Bericht und Testprotokoll

- [BMIRechner Bericht und Testprotokoll](#bmirechner-bericht-und-testprotokoll)
- [Konfiguration Webserver](#konfiguration-webserver)
- [Implementieren eines BMI-Rechners](#implementieren-eines-bmi-rechners)
  - [Einstellungen PhpStorm](#einstellungen-phpstorm)
  - [Formular erstellen](#formular-erstellen)
  - [Eingabefelder Definieren](#eingabefelder-definieren)
  - [Clientseitige Validierung](#clientseitige-validierung)
  - [Serverseitige Validierung](#serverseitige-validierung)
- [Berechnen vom BMI](#berechnen-vom-bmi)
- [Testprotokoll](#testprotokoll)
# Konfiguration Webserver

- Virtuelle Maschine mit Linux Ubuntu Server
- Docker Container lt. docker-compose.yml
```yml
  version: '2'
  services:
  apache:
    image: thecodingmachine/php:7.2-v2-apache-node10
    container_name: apache
    restart: unless-stopped
    ports:
      - 80:80
    environment:
            PHP_EXTENSION_PGSQL: 1
            PHP_EXTENSION_XDEBUG: 1
            PHP_EXTENSION_MONGODB: 1
            PHP_EXTENSION_MONGO: 1
            PHP_INI_ERROR_REPORTING: E_ALL
            APACHE_EXTENSION_AUTOINDEX: 1

    volumes:
      - ./www:/var/www/html
```

- sftp installieren
    - sudo apt-get install vsftpd
- Verbindung von PHPStorm zu VM mittels sftp
    - unter Tools Deployment anlegen
- Verbindung zum Host Zugangsdaten
    - Benutzer: mathiasrudig
    - Passwort: Standard ohne !

# Implementieren eines BMI-Rechners

Das gesamte Programm wurde im Zuge einer Partnerarbeit (Rudig, Tilg) durchgeführt, unter Verwendung des PlugIns Code-With-Me, welches direkt in PhpStorm integriert ist.
## Einstellungen PhpStorm

- SSH-Verbindung --> Terminal (Tools - Start SSH Session)
- FTP-Verbindung --> Remote Host (Tools - Deployment - Browse Remote Host)
- SSH-Verbindung eintragen (Tools - Deployment - Configuration - Connection)
- Pfad mappen (Tools - Deployment - Configuration - Mappings)
- Upload bei Safe (Settings - Deployment - Options - Always)

## Formular erstellen

- Aufbau und Ordnerstruktur des Projektes einrichten (css, lib, js).
- Zieldatei und Methode definieren `action="index.php" method="post"` .
- Bootstrap-Framework als css-Datei lokal einbinden.

## Eingabefelder Definieren

- Aufbau und  des Formulares mittels css-Klassen in verschachtelten Containern, um die Inputfelder laut dem Mockup anzuordnen.
- Inputfelder mit Labels definieren `for="name"` und `name="name"` erzeugen eine Verknüpfung.
- Es werden nur Elemente übertragen, welche einen `name` zugeordnet haben !!
- Speicher und Lösch-Button anlegen (Löschen wird nur als Link inkl. der richtigen css-Klasse definiert).
- Je nach Eingabefeld sollte der Richtige input-type für die nachstehene clientseitige Validierung.

## Clientseitige Validierung

- verhindern von fehlerhaften Benutzereingaben durch verwenden von input-attributes `min max maxlength required`.
- Einbinden einer JavaScript-Datei `<script type="text/javascript" src="js/index.js"></script>` , um eine fehlerhafte Datumseingabe bei **onchange** sofort mittels css-Klassen anzuzeigen.
- implementieren der Methode innerhalb der JavaScript-Datei, der das aktuelle Objekt bei **onchange** übergeben wird `onchange=validateDate(this)`.

## Serverseitige Validierung

- Implementieren des php-Codes im `<bod>` innerhalb der Tags `<?php ?>` .
- Einbinden einer `func.inc.php` Datei mit required, damit das der Code nur ausgeführt wird, sofern die eingebundene Datei vorhanden ist.
- Um zusätzlich vor Codeeinschleußung zu schützen sollten die input-Felder mit `value="<?= htmlspecialchars($messdatum) ?>"` geschützt werden.
- Variablen anlegen und anschließend nach betätigtem `submit` die eingegeben Werte zuordnen oder leer lassen.
- Für den Fehlerfall wird ein globales assoziatives-Array definiert, welches über eine foreach-Schleife ausgegeben wird.
- Damit sich die input-Felder im Fehlerfall färben, kann eine css-Klasse über folgenden Code umgesetzt werden `<?= isset($errors['messdatum']) ? 'is-invalid' : '' ?>` .
- Anschließend können die Daten mittels Funktionen validiert werden und der BMI inkl. Info berechnet werden.

# Berechnen vom BMI

```php
// ------------- BMI-Rechner ---------------
/**
 * Die Funktion berechnet den BMI auf eine Kommastelle und gibt diesen zurück.
 * @param $groesse
 * @param $gewicht
 * @return float gibt den berechneten, auf eine Kommastelle greundeten BMI zurück.
 */
function berechneBMI($groesse, $gewicht) {
    $gerundeterBMI = round(($gewicht/(($groesse/100)**2)), 1 ,PHP_ROUND_HALF_UP);
    return $gerundeterBMI;
}
 * @param $groesse
 * @param $gewicht
 * @return float gibt den berechneten, auf eine Kommastelle greundeten BMI zurück.
 */
function berechneBMI($groesse, $gewicht) {
    $gerundeterBMI = round(($gewicht/(($groesse/100)**2)), 1 ,PHP_ROUND_HALF_UP);
    return $gerundeterBMI;
}

/**
 * Die Funktion ermittelt einen Infotext, welcher auf dem Wert des übergebenen basiert.
 * @param $gerundeterBMI
 * @return string gibt die ermittelte infoBMI in Form eines Strings zurück
 */
function infoBMI($gerundeterBMI) {
    if ($gerundeterBMI < 18.5){
        return "Untergewicht";
    } else if($gerundeterBMI <= 24.9){
        return "Normalgewicht";
    } else if($gerundeterBMI <= 29.9){
        return "Übergewicht";
    } else {
        return "Adipositas";
    }
}

/**
 * Die Funktion ermittelt aus dem übergebenen String die CSS-Klasse, und gibt diese als string zurück.
 * @param $berechneterBMI
 * @return string ermittelte CSS-Klasse vom typ string.
 */
function alarmFarbe($berechneterBMI) {
    switch ($berechneterBMI){
        case 'Untergewicht':
            return 'alert-warning';
        case 'Normalgewicht':
            return 'alert-success';
        case 'Übergewicht':
            return 'alert-warning';
        case 'Adipositas':
            return 'alert-danger';
        default:
            return 'alert-danger';
    }
}
```

# Testprotokoll

Testen der Validierung:</br>
<img src="03_Bericht_BMIRechner_Bilder/Test_Valid.png" alt="Test_Valid.png" width=""/>

<img src="03_Bericht_BMIRechner_Bilder/Test_Valid_POST.png" alt="Test_Valid_POST.png" width=""/>

Durch manipulieren der input-Felder (unter F12) kann die clienseitige-Validierung ausgehebelt werden:</br>
<img src="03_Bericht_BMIRechner_Bilder/Test_Error.png" alt="Test_Error.png" width=""/>

Testen des responsive Designs:</br>
<img src="03_Bericht_BMIRechner_Bilder/Test_Responsive.png" alt="Test_Responsive.png" width="300"/>
