<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Required meta tags -->
    <link href="css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <!-- einbinden der Bootstrap CSS Datei-->
    <title>Notenerfassung</title>
    <script type="application/javascript" src="js/index.js"></script>
    <!-- Einbinden der JavaScript-Datei -->
</head>
<body>
<div class="container">
    <h1 class="mt-5 mb-5">Notenerfassung</h1>
    <!-- mt-5 = margin top -->
    <!-- mb-5 = margin bottom -->
    <!-- Hier startet das Php-Script zur Serverseitigen Validierung -->
    <?php
    require "lib/func.inc.php"; //die nachfolgende datei muss vorhanden sein, ansonsten wird das Script nicht ausgeführt
    // (im Gegensatz zu include)
    //print_r($_POST); //Ausgabe eines POST-Arrays

    //Variablen deklarieren, da sonst beim initialisieren der Seite ein Fehler auftritt
    $name = '';
    $email = '';
    $subject = '';
    $grade = '';
    $examDate = '';

    //mittels isset prüfen ob die Felder Gestzt wurden
    if (isset($_POST['submit'])) { //name des submit buttons
        $name = isset($_POST['name']) ? $_POST['name'] : ''; //ternary-operator
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $subject = isset($_POST['subject']) ? $_POST['subject'] : '';
        $grade = isset($_POST['grade']) ? $_POST['grade'] : '';
        $examDate = isset($_POST['examDate']) ? $_POST['examDate'] : '';

        if (validate($name, $email, $examDate, $subject, $grade)) { //Reihenfolge beachten!!!
            echo "<p class='alert alert-success'>Die eingegebenen Daten sind in Ordnung!</p>";
        } else {
            echo "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
            foreach ($errors as $key => $value) { //$errors wird aus dem Script(func.inc.php) verwendet (Warnung kann ignoriert werden)
                echo "<li>" . $value . "</li>";
            }
            echo "</ul></div>";
        }

    }
    ?>
    <form id="form_grade" action="index.php" method="post">
        <!-- action definiert wohin das Formular per method(POST) geschickt wird -->
        <div class="row">
            <!-- row = neue Reihe innerhalb des containers -->
            <div class="col-sm-6 form-group">
                <!-- col-sm-6 = Größe für responsive Design -->
                <label for="name">Name*</label>
                <input type="text" name="name" class="form-control <?= isset($errors['name']) ? 'is-invalid' : '' ?>" maxlength="20" required value="<?= htmlspecialchars($name)?>">
                <!-- type legt den Typ der Eingabe fest für die Clientseitige Validierung -->
                <!-- maxlength prüft die Länge der Eingabe für die Clientseitige Validierung -->
                <!-- required beschreibt ein verpflichtendes Feld (bei nicht eingabe kann das Formular nicht abgeschickt werden) -->
            </div>
            <div class="col-sm-6 form-group">
                <label for="email">E-Mail</label>
                <input type="email" name="email" class="form-control <?= isset($errors['email']) ? 'is-invalid' : '' ?>" value="<?= htmlspecialchars($email)?>">
                <!-- mit value werden vorher eingetragene Werte wieder eingesetzt werden -->
                <!-- htmlspecialchars wird zum Schutz vor Code Einschleusungen verwendet -->
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 form-group">
                <label for="subject">Fach*</label>
                <select name="subject" class="custom-select <?= isset($errors['subject']) ? 'is-invalid' : '' ?>" required>
                    <!-- custom-select = Auswahl -->
                    <option value="" hidden>Fach auswählen</option>
                    <!-- hidden wird nur angezeigt, kann aber nicht selektiert werden -->
                    <option value="m" <?php if ($subject == 'm') echo "selected = 'selected'"; ?>>Mathematik</option>
                    <option value="d" <?php if ($subject == 'd') echo "selected = 'selected'"; ?>>Deutsch</option>
                    <option value="e" <?php if ($subject == 'e') echo "selected = 'selected'"; ?>>Englisch</option>
                </select>
            </div>
            <div class="col-sm-4 form-group">
                <label for="grade">Note*</label>
                <input type="number" name="grade" class="form-control <?= isset($errors['grade']) ? 'is-invalid' : '' ?>" min="1" max="5" required value="<?= htmlspecialchars($grade)?>">
                <!-- min und max grenzen den Wertebereich für die Clientseitige Validierung ein -->
            </div>
            <div class="col-sm-4 form-group">
                <label for="examDate">Prüfungsdatum*</label>
                <input type="date" name="examDate" class="form-control <?= isset($errors['examDate']) ? 'is-invalid' : '' ?>" required onchange="validateExamDate(this)" value="<?= htmlspecialchars($examDate)?>">
                <!-- onchange löst eine funktion aus und mit this wird das eingetragene Datum übergeben -->
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3 mb-3">
                <input type="submit" name="submit" class="btn btn-primary btn-block" value="Validieren">
                <!-- btn-primary = roter Button -->
                <!-- btn-block = Format des Buttons -->
                <!-- value = Dargestellter Werteinhalt des Buttons -->
            </div>
            <div class="col-sm-3 mb-3">
                <a href="index.php" class="btn btn-secondary btn-block">Löschen</a>
                <!-- Link als Button zur index.php -->
                <!-- btn-secondary = blauer Button -->
            </div>
        </div>
</div>
</form>
</div>
</body>
</html>