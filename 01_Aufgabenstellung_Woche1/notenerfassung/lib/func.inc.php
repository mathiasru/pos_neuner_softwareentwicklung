<?php

/**
 * Hier werden alle Serverseitigen Validierungen als Funktionen aufgeführt.
 */
$errors = []; //globale Variable

/**
 * Die Funktion führt eine Prüfung aller übergebener Parameter mittels Methodenaufrufen aus und gibt einen boolschen
 * Wert zurück.
 * @param $name
 * @param $email
 * @param $subject
 * @param $grade
 * @param $examDate
 * @return bool
 */
function validate($name, $email, $examDate, $subject, $grade){ //Reihenfolge beachten!!!
    return validateName($name) & validateEmail($email) & validateSubject($subject) & validateGrade($grade) & validateExamDate($examDate);
    //Verwendung einfacher & damit alle Funktionen für die Ausgabe der Fehlermeldungen ausgeführt werden
}

/**
 * Die Funktion überprüft den übergebenen String auf Gültigkeit.
 * @param $name
 * @return bool
 */
function validateName($name){
    global $errors; // um auf globale Variablen zugreifen zu können
    if (strlen($name) == 0){ //strlen prüft die Länge eines Strings
        $errors['name'] = "Name darf nicht leer sein"; //schreiben in das globalle Array
        return false;
    } elseif (strlen($name) > 20){
        return false;
    } else {
        return true;
    }
}

/**
 * Die Funktion prüft auf ein gültiges Datum, welches nicht leer oder in der Zukunft liegen darf.
 * @param $examDate
 * @return bool
 */
function validateExamDate($examDate)
{
    global $errors; // um auf globale Variablen zugreifen zu können
    try {
        if ($examDate == "") {
            $errors['examDate'] = "Prüfungsdatum darf nicht leer sein";
            return false;
        } else if (new DateTime($examDate) > new DateTime()) {
            $errors['examDate'] = "Prüfungsdatum darf nicht in der Zukunft liegen";
            return false;
        } else {
            return true;
        }
    } catch (Exception $exception) {
        $errors['examDate'] = "Prüfungsdatum ungültig";
        return false;
    }
}

/**
 * Die Funktion überprüft auf ein gültiges Fach (m,d,e).
 * @param $subject
 * @return bool
 */
function validateSubject($subject)
{
    global $errors;
    if ($subject != 'm' && $subject != 'd' && $subject != 'e') {
        $errors['subject'] = "Fach ungültig";
        return false;
    } else {
        return true;
    }
}

/**
 * Die Funktion überprüft auf eine gültige Zahl zwischen 1-5 und prüft ob es überhaupt eine Zahl ist.
 * @param $grade
 * @return bool
 */
function validateGrade($grade)
{
    global $errors;
    // Zahl, 1 bis 5
    if (!is_numeric($grade) || $grade < 1 || $grade > 5) { //is_numeric prüft auf eine Zahl
        $errors['grade'] = "Note ungültig";
        return false;
    } else {
        return true;
    }
}

/**
 * Die Funktion überprüft auf eine gültige E-Mail Adresse.
 * @param $email
 * @return bool
 */
function validateEmail($email)
{
    global $errors;
    if ($email != "" && !filter_var($email, FILTER_VALIDATE_EMAIL)) { //prüfen auf leer und gültig
        $errors['email'] = "E-Mail ungültig";
        return false;
    } else {
        return true;
    }
}


