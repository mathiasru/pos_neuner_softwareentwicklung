# Einführung PHP

- [Einführung PHP](#einführung-php)
  - [PHP-Grundlagen](#php-grundlagen)
    - [Beispielcode](#beispielcode)
- [Notenerfassung Aufgabe 01](#notenerfassung-aufgabe-01)
  - [Konfiguration Webserver](#konfiguration-webserver)
    - [Clienseitige Validierung implementieren](#clienseitige-validierung-implementieren)
    - [Serverseitige Validierung implementieren](#serverseitige-validierung-implementieren)
- [Testen der Funktionen](#testen-der-funktionen)

PHP ist eine Sprache die vom Browser interpretiert (Zeile für Zeile ausgeführt)

## PHP-Grundlagen

- In einer PHP-Datei kann HTML-Code geschrieben werden, der dann vom Browser interpretiert wird.
- Tags können beliebig oft verwendet werden (<?php?>)
- In PHP gibt es keine Datentypen und Variablen werden mit einem $ deklariert. (Variablen sind nicht Typisiert und können sich zur Laufzeit ändern)
- Ausgaben können mit `echo` oder `print` gemacht werden.
- Strings können "" oder '' verwendet werden, Unterschied bei "" werden Variablenwerte berücksichtigt.
- Strings werden unter PHP mit einem Punkt concatiniert werden. Ein Plus + wird als Rechenoperation interpretiert.
- Eine Seite wird per GET-Methode geladen und ein evtl. Formular per POST-Methode verschickt.
- GET wird in der Url übertragen und die POST-Methode überträgt im Hintergrund
- Client und Serverseitig Validieren, da die Clientseitige validierung ausgehebelt werden kann.

### Beispielcode

```php
<?php
$x = 5;
echo "Hallo Welt. x = $x";
echo 'Hallo Welt. x = $x';
echo 'Hallo Welt. x =' . $x; //effizientere Variante
$x = $x + 4;
for ($i = 0; $i <= 10; $i++) {
    echo $i . "</br>";
}
?>
```

# Notenerfassung Aufgabe 01

## Konfiguration Webserver

- Virtuelle Maschine mit Linux Ubuntu Server
- Docker Container lt. docker-compose.yml
  ```yml
  version: '2'
  services:
  apache:
    image: thecodingmachine/php:7.2-v2-apache-node10
    container_name: apache
    restart: unless-stopped
    ports:
      - 80:80
    environment:
            PHP_EXTENSION_PGSQL: 1
            PHP_EXTENSION_XDEBUG: 1
            PHP_EXTENSION_MONGODB: 1
            PHP_EXTENSION_MONGO: 1
            PHP_INI_ERROR_REPORTING: E_ALL
            APACHE_EXTENSION_AUTOINDEX: 1

    volumes:
      - ./www:/var/www/html
      ```

- sftp installieren
    - sudo apt-get install vsftpd
- Verbindung von PHPStorm zu VM mittels sftp
    - unter Tools Deployment anlegen
- Verbindung zum Host Zugangsdaten
    - Benutzer: mathiasrudig
    - Passwort: Standard ohne !

## Einstellungen PhpStorm

- SSH-Verbindung --> Terminal (Tools - Start SSH Session)
- FTP-Verbindung --> Remote Host (Tools - Deployment - Browse Remote Host)
- SSH-Verbindung eintragen (Tools - Deployment - Configuration - Connection)
- Pfad mappen (Tools - Deployment - Configuration - Mappings)
- Upload bei Safe (Settings - Deployment - Options - Always)

## Vorbereitung Projekt

- neues Projekt erstellen (Bootstrap) mit folgender Projektstruktur
    - css (Stylesheet)
    - js (JavaScript)
    - lib (Libary)
- Bootstrap Vorlage von der Hompage für die index.php Seite verwenden
- Bootrap-CSS einbinden

## Schritte der Formularverarbeitung

1. Formular definieren
2. Eingabefelder definieren
3. Clientseitige Validierung implementieren
4. Serverseitige Validierung implementieren
5. Serverseitig Daten verarbeiten
6. Ausgabe der Ergebnisse


## Implementierung

### Aufbau und Design der index.php Seite implementieren

```html
<div class="container">
    <h1 class="mt-5 mb-5">Notenerfassung</h1>
    <!-- mt-5 = margin top -->
    <!-- mb-5 = margin bottom -->
    <form id="form_grade" action="index.php" method="post">
				<!-- action definiert wohin das Formular per method(POST) geschickt wird -->
        <div class="row">
            <!-- row = neue Reihe innerhalb des containers -->
            <div class="col-sm-6 form-group">
                <!-- col-sm-6 = Größe für responsive Design -->
                <label for="name">Name*</label>
                <input type="text" name="name" class="form-control" maxlength="20" required>
                <!-- type legt den Typ der Eingabe fest für die Clientseitige Validierung -->
                <!-- maxlength prüft die Länge der Eingabe für die Clientseitige Validierung -->
                <!-- required beschreibt ein verpflichtendes Feld (bei nicht eingabe kann das Formular nicht abgeschickt werden) -->
            </div>
            <div class="col-sm-6 form-group">
                <label for="email">E-Mail</label>
                <input type="email" name="email" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 form-group">
                <label for="subject">Fach*</label>
                <select name="subject" class="custom-select" required>
                    <!-- custom-select = Auswahl -->
                    <option>Mathematik</option>
                    <option>Deutsch</option>
                    <option>Englisch</option>
                </select>
            </div>
            <div class="col-sm-4 form-group">
                <label for="grade">Note*</label>
                <input type="number" name="grade" class="form-control" min="1" max="5" required>
                <!-- min und max grenzen den Wertebereich für die Clientseitige Validierung ein -->
            </div>
            <div class="col-sm-4 form-group">
                <label for="examDate">Prüfungsdatum*</label>
                <input type="date" name="examDate" class="form-control" required onchange="validateExamDate(this)">
                <!-- onchange löst eine funktion aus und mit this wird das eingetragene Datum übergeben -->
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3 mb-3">
                <input type="submit" name="submit" class="btn btn-primary btn-block" value="Validieren">
                <!-- btn-primary = roter Button -->
                <!-- btn-block = Format des Buttons -->
                <!-- value = Dargestellter Werteinhalt des Buttons -->
            </div>
            <div class="col-sm-3 mb-3">
                <a href="index.php" class="btn btn-secondary btn-block">Löschen</a>
                <!-- Link als Button zur index.php -->
                <!-- btn-secondary = blauer Button -->
            </div>
        </div>
</div>
</form>
</div>
```

- Unter Netzwerkanalyse im Browser den abgesetzten POST-Befehl über den Validierungs-Button prüfen

---

### Clienseitige Validierung implementieren

- index.js Datei mit der entsprechenden Funktion im head oder ganz unten in der Seite einbinden.
`<script type="application/javascript" src="js/index.js"></script>`
- diverse Validierungen direkt in der index.php seite durch verwenden von (required;min;max;maxlength)
- durch verwenden unterschiedlicher typen ist ein entsprechende Validierung gewährleistet (text;number;email;datte)
- Clientseitige Datums Validierung mittels JavaScript

    ```jsx
    function validateExamDate(element) {
        let today = new Date(); //erzeugen eines neunen Datumsobjektes (aktuelles Datum)
        let examDate = new Date(element.value); //erzeugen eines Datumsobjektes mit dem übergebenen Wert
        examDate.setHours(0, 0, 0, 0); //um nur das Datum zu prüfen (evtl. Fehlerquelle)

        if (examDate <= today) { //vergleichen der beiden Datumswerten
            element.classList.add("is-valid"); //durch integrierte Bootstrap css (is-valid )wird das Eingabefeld grün aktiviert
            element.classList.remove("is-invalid"); //durch integrierte Bootstrap css (is-valid )wird das rote Eingabefeld deaktiviert
        } else {
            element.classList.add("is-invalid");
            element.classList.remove("is-valid");
        }

        console.log(examDate);
    }
    ```

---

### Serverseitige Validierung implementieren

- Implementierung innerhalb der index.php Seite und durch Auslagerung ind func.inc.php
- Zunächst haben wir einen GET Aufruf um die Seit anzuzeigen und anschließend mit POST um die Daten an den Server zu schicken
- Formularverarbeitung (PHP) im Script integrieren (Scriptsprachen lesen Zeile für Zeile).
- In unserem Fall: Im BODY-Bereich, gekennzeichnet durch **<?php?>**
- Einbindung des Scripts im PHP-Bereich der *index.php* mit Hilfe von *include* bzw. besser **require da hier das Script nicht ausgeführt wird (wenn Datei nicht gefunden)**
- Auszug aus der index.php

```php
<?php
    require "lib/func.inc.php"; //die nachfolgende datei muss vorhanden sein, ansonsten wird das Script nicht ausgeführt (im Gegensatz zu include)
    //print_r($_POST); //Ausgabe eines POST-Arrays

    //Variablen deklarieren, da sonst beim initialisieren der Seite ein Fehler auftritt
    $name = '';
    $email = '';
    $subject = '';
    $grade = '';
    $examDate = '';

    //mittels isset prüfen ob die Felder Gestzt wurden
    if (isset($_POST['submit'])) { //name des submit buttons
        $name = isset($_POST['name']) ? $_POST['name'] : ''; //ternary-operator
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $subject = isset($_POST['subject']) ? $_POST['subject'] : '';
        $grade = isset($_POST['grade']) ? $_POST['grade'] : '';
        $examDate = isset($_POST['examDate']) ? $_POST['examDate'] : '';

        if (validate($name, $email, $examDate, $subject, $grade)) { //Reihenfolge beachten!!!
            echo "<p class='alert alert-success'>Die eingegebenen Daten sind in Ordnung!</p>";
        } else {
            echo "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
            foreach ($errors as $key => $value) { //$errors wird aus dem Script(func.inc.php) verwendet (Warnung kann ignoriert werden)
                echo "<li>" . $value . "</li>";
            }
            echo "</ul></div>";
        }

    }
    ?>
```

- Wichtig!!! Immer auf die Reihenfolge bei Parameter Übergabe/ -nahme beachten! Durch die Typfreien Variablen kommt es zu keinen Fehlern in der IDE.

# Testen der Funktionen
* Prüfen auf Funktion
![Abbildung1](01_Bericht_Notenerfassung_Bilder/Bildschirmfoto_2021-09-22_um_8.29.08_PM.png)
</br></br>
* Fehler durch Manipulation erzwingen
![Abbildung2](01_Bericht_Notenerfassung_Bilder/Bildschirmfoto_2021-09-22_um_8.36.21_PM.png)
</br></br>
![Abbildung3](01_Bericht_Notenerfassung_Bilder/Bildschirmfoto_2021-09-22_um_8.37.10_PM.png)