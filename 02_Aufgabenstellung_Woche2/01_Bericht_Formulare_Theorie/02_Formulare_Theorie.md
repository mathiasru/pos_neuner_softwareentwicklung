# Formulare - Theorie (Woche 2)

- [Formulare - Theorie (Woche 2)](#formulare---theorie-woche-2)
- [HTTP Meldungen und Anfragen](#http-meldungen-und-anfragen)
  - [HTML - Status Messages](#html---status-messages)
  - [HTTP - Request Methods](#http---request-methods)
    - [GET Anfrage](#get-anfrage)
    - [POST Anfrage](#post-anfrage)
- [HTML Formulare](#html-formulare)
  - [HTML Form Attributes](#html-form-attributes)
  - [HTML Form Elements](#html-form-elements)
    - [input](#input)
    - [label](#label)
    - [select und options](#select-und-options)
    - [button](#button)
- [Benutzereingabe und Datenvalidierung](#benutzereingabe-und-datenvalidierung)
  - [Schritte der Benutzereingabe/ Validierung](#schritte-der-benutzereingabe-validierung)
  - [Schritte der Formularverarbeitung](#schritte-der-formularverarbeitung)
- [Bootstrap](#bootstrap)
  - [Einbinden in HTML/ PHP Seite](#einbinden-in-html-php-seite)
  - [Beispielaufbau](#beispielaufbau)
- [Erste Schritte in PHP](#erste-schritte-in-php)
  - [PHP-Grundlagen](#php-grundlagen)
  - [Variablen](#variablen)
  - [Arrays](#arrays)
    - [Eindimensionale Arrays](#eindimensionale-arrays)
    - [Assoziatieves Array](#assoziatieves-array)
    - [Mehrdimensionale Arrays](#mehrdimensionale-arrays)
    - [Short Syntax Array](#short-syntax-array)
  - [Funktionen](#funktionen)
    - [Einbinden einer PHP-Datei](#einbinden-einer-php-datei)
    - [Übergabe einer Referenz](#übergabe-einer-referenz)
    - [Standardparameter](#standardparameter)

# HTTP Meldungen und Anfragen

## HTML - Status Messages

- 1xx → Information
- 2xx → Successful
    - 200 OK
- 3xx → Redirection (Umleitung)
- 4xx → Client Error
    - 404 Not Found
    - 400 Bad Request
- 5xx → Server Error
    - 502 Bad Gateway
    - 503 Service Unavailable
    

## HTTP - Request Methods

### GET Anfrage

- GET wird verwendet, um Daten von einer angegebenen Ressource anzufordern.
- GET ist eine der gängigsten HTTP-Methoden ist aber auf 2048 Zeichen begrenzt.
- Verwenden Sie NIEMALS GET, um sensible Daten zu senden! (die übermittelten Formulardaten sind in der URL sichtbar!)
- GET- Anfragen sendet die Daten in der URL mit und bleiben im Browserverlauf.
- Können als Lesezeichen erstellt werden
- Beispielcode:

```php
<form action="welcome_get.php" method="get">
	Name: <input type="text" name="name"><br>
	E-mail: <input type="text" name="email"><br>
	<input type="submit">
</form>

//Ausgabe in welcome.php
Welcome <?php echo $_GET["name"]; ?><br>
Your email address is: <?php echo $_GET["email"];?>
```

### POST Anfrage

- POST wird verwendet, um Daten an einen Server zu senden, um eine Ressource zu erstellen/zu aktualisieren.
- POST ist eine der gängigsten HTTP-Methoden.
- Ein zu übertragender Datensatz ist nicht Teil der URL, sondern wird, durch eine Leerzeile getrennt, an den Header angehängt.
- Beim Aktualisieren einer Seite mit F5, übermittelt eine POST-Anfrage die Daten erneut.
- Beispielcode:

```php
<form action="welcome.php" method="post">
	Name: <input type="text" name="name"><br>
	E-mail: <input type="text" name="email"><br>
	<input type="submit">
</form>

//Ausgabe in welcome.php
Welcome <?php echo $_POST["name"]; ?><br>
Your email address is: <?php echo $_POST["email"];?>
```

---

# HTML Formulare

Ein HTML-Formular wird verwendet, um Benutzereingaben zu sammeln. Die Benutzereingaben werden meistens zur Verarbeitung an einen Server gesendet.

## HTML Form Attributes

- Das `action`Attribut definiert die Aktion, die beim Absenden des Formulars ausgeführt werden soll. (index.php)
Normalerweise werden die Formulardaten an eine Datei auf dem Server gesendet, wenn der Benutzer auf die Schaltfläche zum Senden klickt.
- Das `target` Attribut gibt an, wo die nach dem Absenden des Formulars empfangene Antwort angezeigt werden soll. (_blank in eigenem Tab/ _self im selben Tab)
- Das `method` Attribut gibt die HTTP-Methode an, die beim Senden der Formulardaten verwendet werden soll. (POST/ GET)
- Das `autocomplete` Attribut gibt an, ob für ein Formular die automatische Vervollständigung aktiviert oder deaktiviert sein soll. (on/ off)
- Das `novalidate` Attribut ist ein boolesches Attribut. Wenn vorhanden, gibt es an, dass die Formulardaten (Eingabe) beim Senden nicht validiert werden sollen.

```html
<!-- Beispiel -->
<form action="index.php" target="_blank" method="post" autocomplete="on" novalidate>
<!-- Formular -->
</form>
```

## HTML Form Elements

Die wichtigsten Elements sind:

### input

Das `<input>` Element kann je nach type Attribut auf verschiedene Arten angezeigt werden. (text, number, date, email, submit, button, reset, checkbox, password, ...)

```html
<input type="text" id="fname" name="fname">
```

- `reset`  → würde value Felder nicht reseten, deshalb wird dieser kleine Trick angewendet:
    
    ```html
    <a href="index.php" class="btn btn-secondary btn-block">Löschen</a>
    <!-- Link als Button dargstellt (Vorteil von Bootstrap), führt einfach wieder zurück zum Anfangsformular -->
    ```
    
- Eingabebeschränkungen für Clientseitige Validierung
    
|   Attribute   |   Beschreibung
|   ---         |   ---    
|   checked     |   spezifiziert ein selektiertes Feld beim Laden
|   disabled    |   spezifiziert ein deaktiviertes Feldes
|   max         |   spezifiziert den maximalen Wert eines Feldes
|   maxlength   |   spezifiziert die Anzahl an Zeichen
|   min         |   spezifiziert den minimalen Wert eines Feldes
|   pattern     |   spezifiziert ein Feld welches mittels RegEx geprüfft wird
|   readonly    |   spezifiziert ein nicht änderbares Feld
|   required    |   spezifiziert ein verpflichtendes Feld
|   size        |   spezifiziert Breite in Zeichen für ein Feld
|   value       |   spezifiziert einen default-Wert

### label

Das <label>Element definiert ein Label für Formularelemente.

Das `for` Attribut des `<label>` Tags sollte dem `id` Attribut des `<input>` Elements entsprechen, um sie miteinander zu verbinden. Das `name` Tag ist essenziell für die Verarbeitung und Übertragung an den Server.

```html
<label for="fname">First name:</label>
<input type="text" id="fname" name="fname">
```

### select und options

Das `<select>` Element definiert eine Dropdown-Liste.

```html
<label for="cars">Choose a car:</label>
<select id="cars" name="cars">
  <option value="volvo">Volvo</option>
  <option value="saab">Saab</option>
</select>
```

### button

Das `<button>` Element definiert eine anklickbare Schaltfläche.

```html
<button type="button" onclick="alert('Hello World!')">Click Me!</button>
```

---

# Benutzereingabe und Datenvalidierung

## Schritte der Benutzereingabe/ Validierung

1. Validierung durch HTML-Attribute
2. Validierung durch JavaScript
3. Formularübermittlung
4. Validierung durch PHP

## Schritte der Formularverarbeitung

1. Formular definieren
2. Eingabefelder definieren
3. Clientseitige Validierung implementieren
4. Serverseitige Validierung implementieren
5. Serverseitig Daten verarbeiten
6. Ausgabe der Ergebnisse

---

# Bootstrap

[https://getbootstrap.com/docs/3.4/css/](https://getbootstrap.com/docs/3.4/css/)

## Einbinden in HTML/ PHP Seite

```html
<link href="css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
<!-- einbinden der Bootstrap CSS Datei -->
```

## Beispielaufbau

```html
<div class="container">
    <h1 class="mt-5 mb-5">Notenerfassung</h1>
    <!-- mt-5 = margin top -->
    <!-- mb-5 = margin bottom -->
        <div class="row">
            <!-- row = neue Reihe innerhalb des containers -->
            <div class="col-sm-6 form-group">
                <!-- col-sm-6 = Größe für responsive Design -->
                <label for="name">Name*</label>
                <input type="text" name="name" class="form-control" maxlength="20" required value="<?= htmlspecialchars($name)?>">
                <!-- type legt den Typ der Eingabe fest für die Clientseitige Validierung -->
                <!-- maxlength prüft die Länge der Eingabe für die Clientseitige Validierung -->
                <!-- required beschreibt ein verpflichtendes Feld (bei nicht eingabe kann das Formular nicht abgeschickt werden) -->
								<!-- htmlspecialchars() um sich vor Codeeinschleusungen zu schützen -->
            </div>
        </div>
</div>
```

---

# Erste Schritte in PHP

## PHP-Grundlagen

- In einer PHP-Datei kann HTML-Code geschrieben werden, der dann vom Browser interpretiert wird.
- Der Code wird zur Laufzeit interpretiert (Interpreter - Zeile für Zeile ausführen)
- Tags können beliebig oft verwendet werden (<?php?>)
- In PHP gibt es keine Datentypen und Variablen werden mit einem $ deklariert. (Variablen sind nicht Typisiert und können sich zur Laufzeit ändern)
- Ausgaben können mit `echo` oder `print` gemacht werden. (echo ist schneller)
- Strings können "" oder '' verwendet werden, Unterschied bei "" werden Variablenwerte berücksichtigt, jedoch dauert die Verarbeitung länger.
- Strings werden unter PHP mit einem Punkt concatiniert werden. Ein Plus + wird als Rechenoperation interpretiert.
- Dokumentation PHP: [http://php.net/manual/de/](http://php.net/manual/de/)

## Variablen

Variablen sind in PHP typenunabhänging

```php
$name = "Mathias";
echo $name. "<br/>";
echo strlen($name). "<br/>"; //gibt die Länge des Strings zurück
if (!empty($name)){ //überprüft ob die Variable lerr ist "" oder 0
    echo "richtig";
}
if (isset($name)){ //überprüft ob die Variable existiert und nicht null ist
    echo "richtig";
}
```

## Arrays

Arrays sind auch typunabhängig

Mit dem + Operator können Arrays concateniert werden.

### Eindimensionale Arrays

```php
$liste = array("Mathias", "Tobias", 123);
echo $liste[0] . "<br/>";
$liste[] = "am Ende"; //anfügen am Ende
array_unshift($liste, "am Anfang"); //einfügen am Anfang
echo array_shift($liste). "<br/>"; //löscht das erste Element und gibt es zurück
echo array_pop($liste). "<br/>"; //löscht das letzte Element und gibt es zurück
unset($liste[0]); //löscht den Eintrag an einem bestimmten Index
$liste[0] = null; //setzt den Eintrag an einem bestimmten Index auf null

//Ausgabe eines Arrays mittels for-Schleife
for ($index = 0; $index < count($liste); $index++){ //count()-Methode gibt die Anzahl an Einträgen zurück
    echo "<li>" . $liste[$index] . "</li>";
}
//Ausgabe eines Arrays mittels forEach-Schleife
foreach ($liste as $elements){
    echo "<li>" . $elements . "</li>";
}
//Ausgabe eines Arrays mittels forEach-Schleife inkl. Index
foreach ($liste as $key => $elements ){
    echo "<li>" . $key. " ".$elements . "</li>";
}
var_dump($liste); //gibt alle Inhalte der Variable inkl. Typen aus (eignet sich für Debugging)
```

### Assoziatieves Array

Assoziatives Array (Art HashMap key-value)

```php
$listeMap = array("Mathias" => 28, "Tobias" => 21);
$listeMap[] = "am Ende"; //anfügen am Ende eines normal Array-Eintrages möglich da sie sich nicht unterscheiden
$listeMap["Mathias"] = 29; //überschreiben eines Eintrages
foreach ($listeMap as $key => $elements ){
    echo "<li>" . $key. ": ".$elements . "</li>";
```

### Mehrdimensionale Arrays

```php
//Mehrdimensionales Array (ausgabe eines verschachtelten Arrays)
$produkte = array("Obst" => array("Eintrag1", "Eintrag2", "Eintrag3"));
echo "<ul>";
foreach ($produkte as $kategorie => $eintraege) {
    echo "<li>" . $kategorie . "<ul>";
    foreach ($eintraege as $eintrag) {
        echo "<li>" . $eintrag . "</li>";
    }
    echo "</ul></li>";
}
echo "</ul>";
```

### Short Syntax Array

Short Array Syntax, ist seit PhP 5.4 implementiert und können vermischt werden.

```php
//
$shortArray = ["Eintraege" => array("Eintrag1", "Eintrag2", "Eintrag3")];
```

## Funktionen

Können direkt auf der index.php Seite innerhalb der Tags platziert werden, oder ausgelagert werden! 

### Einbinden einer PHP-Datei

```php
require "lib/func.inc.php"; //die nachfolgende datei muss vorhanden sein, ansonsten wird das Script nicht ausgeführt 
//(im Gegensatz zu include)
//print_r($_POST); //Ausgabe eines POST-Arrays
```

### Übergabe einer Referenz

Sollte nicht unbedingt verwendet werden!

Wird aber zum Beispiel von Funktionen wie str_replace verwendet.

```php
function referenz(&$var1){ 
    $var1 = "nicht mehr Mathias";
}

$var1 = "Mathias";
referenz($var1);
echo $var1;
```

### Standardparameter

Mit dieser Funktionen können Methoden aufgerufen werden, welche einen Standardparameter bereitstellen.

```php
function standardparameter($name, $alter = 33){
    return $name . " " . $alter;
}
$var1 = "Mathias";
echo standardparameter($var1) . "</br>";
```