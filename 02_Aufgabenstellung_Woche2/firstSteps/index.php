<?php
//Variablen sind typunabhänging
$name = "Mathias";
echo $name . "<br/>";
echo strlen($name) . "<br/>"; //gibt die Länge des Strings zurück
if (!empty($name)) { //überprüft ob die Variable lerr ist "" oder 0
    echo "richtig";
}
if (isset($name)) { //überprüft ob die Variable existiert und nicht null ist
    echo "richtig";
}


//Arrays sind auch typunabhängig
$liste = array("Mathias", "Tobias", 123);
echo $liste[0] . "<br/>";
$liste[] = "am Ende"; //anfügen am Ende
array_unshift($liste, "am Anfang"); //einfügen am Anfang
echo array_shift($liste) . "<br/>"; //löscht das erste Element und gibt es zurück
echo array_pop($liste) . "<br/>"; //löscht das letzte Element und gibt es zurück
//unset($liste[0]); //löscht den Eintrag an einem bestimmten Index
//$liste[0] = null; //setzt den Eintrag an einem bestimmten Index auf null

//Ausgabe eines Arrays mittels for-Schleife
for ($index = 0; $index < count($liste); $index++) { //count()-Methode gibt die Anzahl an Einträgen zurück
    echo "<li>" . $liste[$index] . "</li>";
}
//Ausgabe eines Arrays mittels forEach-Schleife
foreach ($liste as $elements) {
    echo "<li>" . $elements . "</li>";
}
//Ausgabe eines Arrays mittels forEach-Schleife inkl. Index
foreach ($liste as $key => $elements) {
    echo "<li>" . $key . " " . $elements . "</li>";
}
var_dump($liste); //gibt alle Inhalte der Variable inkl. Typen aus (eignet sich für Debugging)

//Assoziatives Array (Art HashMap key-value)
$listeMap = array("Mathias" => 28, "Tobias" => 21);
$listeMap[] = "am Ende"; //anfügen am Ende eines normal Array-Eintrages möglich da sie sich nicht unterscheiden
$listeMap["Mathias"] = 29; //überschreiben eines Eintrages
foreach ($listeMap as $key => $elements) {
    echo "<li>" . $key . ": " . $elements . "</li>";
}

//Mehrdimensionales Array (ausgabe eines verschachtelten Arrays)
$produkte = array("Obst" => array("Eintrag1", "Eintrag2", "Eintrag3"));
echo "<ul>";
foreach ($produkte as $kategorie => $eintraege) {
    echo "<li>" . $kategorie . "<ul>";
    foreach ($eintraege as $eintrag) {
        echo "<li>" . $eintrag . "</li>";
    }
    echo "</ul></li>";
}
echo "</ul>";

//Short Array Syntax, ist seit PhP 5.4 implementiert und können vermischt werden
$shortArray = ["Eintraege" => ["Eintrag1", "Eintrag2", "Eintrag3"]];

//Funktionen
function referenz(&$var1){ //übergabe einer Referenz (sollte nicht unbedingt verwendet werden!!!)
    $var1 = "nicht mehr Mathias";
}

function standardparameter($name, $alter = 33){
    return $name . " " . $alter;
}
$var1 = "Mathias";
echo standardparameter($var1) . "</br>";

referenz($var1);
echo $var1;
$filteredData = [];
$produkte2 = array(array("id"=>"Eintrag1","firstname" => "testitest"));
foreach ($produkte2 as $users => $inhalt) {
    $gefunden = strpos($inhalt['firstname'], 'testitest' );
        if ( $gefunden !== false){
        print_r($inhalt);
    }
}