<?php

require_once('controllers/Controller.php');

// http://10.211.55.4/passwordmanager2.0/index.php?r=credentials/index
// http://10.211.55.4/passwordmanager2.0/index.php?r=credentials/update&id=25

//hier wird entschieden wie die URL verarbeitet wird per GET-Parameterabfrage
//sofern im GET nichts vorhanden ist, wird default der credentials controller verwendet
//ansonsten wird ein Array erzeugt (gesplittet an /) und die Leerzeichen entfernt

$route = isset($_GET['r']) ? explode('/', trim($_GET['r'], '/')) : ['credentials'];
//zuweisen des controllers anhand des ersten Eintrages im Array
$controller = sizeof($route) > 0 ? $route[0] : 'credentials';

//hier findet das Routing statt
if ($controller == 'credentials') {
    require_once('controllers/CredentialsController.php');
    (new CredentialsController())->handleRequest($route);
} else {
    //im Fehlerfall auf eine Errorseite weiterleiten
    Controller::showError("Page not found", "Page for operation " . $controller . " was not found!", 404);
}

?>
