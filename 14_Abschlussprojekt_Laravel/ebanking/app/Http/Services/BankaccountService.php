<?php

namespace App\Http\Services;

use App\Models\Bankaccount;

class BankaccountService
{
    const COUNTRY_CODE = 'AT';
    const CHECKSUM = '25';
    const BANKNUMBER = '20502000';
    const IBAN_BANK = 'AT25205020001234567890';
    const BIC_BANK = 'SPIMAT25XXX';

    public function getBankAccountByUserId($userId) {
        return Bankaccount::find($userId);
    }

    public function createBankaccount($userId) {
        Bankaccount::create([
            'iban' => $this->generateIban(),
            'bic' => self::BIC_BANK,
            'balance' => 0,
            'user_id' => $userId
        ]);
    }

    private function generateIban() {
        //überprüfen ob IBAN schon existiert
        $iban = self::COUNTRY_CODE . self::CHECKSUM . self::BANKNUMBER;
        $data = '1234567890';
        $iban .= substr(str_shuffle($data), 0, 10);
        return $iban;
    }



}
