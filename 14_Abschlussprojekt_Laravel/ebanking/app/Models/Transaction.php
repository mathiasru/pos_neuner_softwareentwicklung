<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'transactions';
    protected $primaryKey = 'id';

    protected $fillable = [
        'amount',
        'purpose',
        'transaction_time',
        'sender_iban',
        'receiver_iban',
        'receiver_bic'
    ];

    //Beziehung zu Transaktionen - 1:n / n-Seite --> zusammengesetzten Fremdschlüssel
    public function bankaccountSender() {
        return $this->belongsTo(Bankaccount::class, 'sender_iban', 'iban');
    }

    public function bankaccountReceiver() {
        return $this->belongsTo(Bankaccount::class, 'receiver_iban', 'iban');
    }

}
