<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bankaccount extends Model
{
    use HasFactory;

    protected $table = 'bankaccounts';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'iban',
        'bic',
        'balance',
        'user_id'
    ];

    //Beziehung zum User - 1:n / n-Seite
    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function transition() //Realtionship 1:M
    {
        return $this->hasMany(Transaction::class, 'iban', 'id');
    }
}
