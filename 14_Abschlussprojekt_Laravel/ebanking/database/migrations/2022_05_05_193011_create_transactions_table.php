<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->double('amount', 255);
            $table->string('purpose', 255);
            $table->dateTime('transaction_time');
            $table->string('sender_iban', 255)->nullable();
            $table->foreign('sender_iban')->references('iban')->on('bankaccounts')
                ->restrictOnDelete()->restrictOnUpdate();
            $table->string('receiver_iban', 255)->nullable();
            $table->foreign('receiver_iban')->references('iban')->on('bankaccounts')
                ->restrictOnDelete()->restrictOnUpdate();
            $table->string('receiver_bic', 255)->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
