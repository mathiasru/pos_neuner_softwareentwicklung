<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Transaktion durchführen</title>
</head>
<body class="bg-light">

<div class="mt-5 container rounded">
    <div class="row">
        <h1 class="m-2 mb-5 mt-3 text-center">Neue Transaktion</h1>
    </div>

    <div class="m-5 mb-2 mt-2">

        <div class="row mb-3">
            <div class="col-sm-6">
                Absenderkonto: <span
                    class="h6">{{$bankaccount->iban}}, {{$user->firstname}}, {{$user->lastname}}</span>
            </div>
            <div class="col-sm-6">
                Absender BIC: <span class="h6">{{$bankaccount->bic}}</span>
            </div>
        </div>

        <form action="{{route('transaction.store')}}" method="post">
            @csrf
            <div class="row mt-5">
                <div class="col-sm-6 form-group">
                    <div class="input-group">
                        <span class="input-group-text">Empfängerkonto IBAN</span>
                        <input type="text"
                               name="iban"
                               id="iban"
                               class="form-control"
                               value=""
                               required
                               minlength="22"
                               maxlength="30"
                        />
                    </div>
                </div>

                <div class="col-sm-6 form-group">
                    <div class="input-group">
                        <span class="input-group-text">Empfänger BIC</span>
                        <input type="text"
                               name="bic"
                               id="bic"
                               class="form-control"
                               value=""
                               required
                               minlength="8"
                               maxlength="11"
                        />
                    </div>
                </div>
            </div>


            <div class="row mt-3">
                <div class="col-sm-6 form-group">
                    <div class="input-group">
                        <span class="input-group-text">Verwendungszweck</span>
                        <input type="text"
                               name="purpose"
                               id="purpose"
                               class="form-control "
                               value=""
                               required
                               minlength="0"
                               maxlength="255"
                        />
                    </div>
                </div>

                <div class="col-sm-6 form-group">
                    <div class="input-group">
                        <span class="input-group-text">Summe</span>
                        <input type="number"
                               name="amount"
                               id="amount"
                               class="form-control "
                               value=""
                               required
                               min="0"
                        />
                    </div>
                </div>
            </div>

            <div class="row mt-4 mb-4">
                <div class="col-6"></div>
                <div class="col-3">
                    <input type="submit"
                           value="Überweisen"
                           name="submit"
                           class="btn btn-outline-primary"
                           id="submit"
                    />
                </div>
                <div class="col-3" align="right">
                    <a href="#back to customer" class="btn btn-outline-secondary">Zurück</a>
                </div>
            </div>


        </form>
    </div>

</div>
</body>
</html>
