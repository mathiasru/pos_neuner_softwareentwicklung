<x-guest-layout>
    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" />

    <div class="mt-5 container border border-secondary rounded w-75">

        <h1 class="m-3 text-center">Willkommen bei der Bank Ihres Vertrauens</h1>
        <h2 class="m-3 text-center">Login</h2>

        <p class="m-3 text-center">Melden sie sich mit Ihren Daten an.</p>

        <div class="m-3">
            {{--HIER ERRORS EINFÜGEN--}}
            <x-auth-validation-errors class="mb-4" :errors="$errors" />
        </div>

    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="m-2 row">
            <div class="col-sm-12 form-group">
                <!--<label for="login">Benutzer</label>-->
                <input type="text"
                       name="username"
                       id="username"
                       placeholder="Benutzername"
                       class="form-control"
                       value="{{old('username')}}"
                       required
                />
            </div>
        </div>
        <div class="m-2 mt-3 row">
            <div class="col-sm-12 form-group">
                <!--<label for="password">Passwort</label>-->
                <input type="password"
                       name="password"
                       id="password"
                       placeholder="Passwort"
                       class="form-control"
                       value=""
                       required
                />
            </div>
        </div>
        <div class="m-3 row">
            <div class="col-12 form-group" align="center">
                <input type="submit" value="Login" name="submit" class="btn btn-primary" />
                <a href="{{ route('register') }}" class="btn btn-secondary">Noch kein Konto?</a>
            </div>
        </div>
    </form>
</x-guest-layout>
