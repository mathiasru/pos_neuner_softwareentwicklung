<x-guest-layout>
    <div class="mt-5 container border border-secondary rounded">
        <h1 class="m-2 text-center">Registrieren</h1>
        <p class="m-3 text-center">Bitte geben Sie Ihre Kundendaten ein.</p>
        <div class="m-3">
            {{--HIER ERRORS EINFÜGEN--}}
            <x-auth-validation-errors class="mb-4" :errors="$errors" />
        </div>

        <form action="{{ route('register') }}" method="post">
            @csrf
            <div class="m-2 row">
                <div class="col-sm-6 form-group">
                    <label for="firstname">Vorname:*</label>
                    <input type="text"
                           name="firstname"
                           id="firstname"
                           class="form-control"
                           value="{{old('firstname')}}"
                           required
                           minlength="0"
                           maxlength="20"
                    />
                </div>
                <div class="col-sm-6 form-group">
                    <label for="lastname">Nachname:*</label>
                    <input type="text"
                           name="lastname"
                           id="lastname"
                           class="form-control"
                           value="{{old('lastname')}}"
                           required
                           minlength="0"
                           maxlength="20"
                    />
                </div>
            </div>

            <div class="m-2 row">
                <div class="col-sm-6 form-group">
                    <label for="email">E-Mail:*</label>
                    <input type="email"
                           name="email"
                           id="email"
                           class="form-control"
                           value="{{old('email')}}"
                           required
                    />
                </div>
                <div class="col-sm-6 form-group">
                    <label for="birthdate">Geburtsdatum:*</label>
                    <input type="date"
                           name="birthdate"
                           id="birthdate"
                           onchange=""
                           class="form-control"
                           value="{{old('birthdate')}}"
                           required
                    />
                </div>
            </div>

            <div class="m-2 row">
                <div class="col-sm-6 form-group">
                    <label for="username">Benutzername:*</label>
                    <input type="text"
                           name="username"
                           id="username"
                           class="form-control"
                           value="{{old('username')}}"
                           required
                           minlength="5"
                           maxlength="20"
                    />
                </div>

                <div class="col-sm-6 form-group">
                    <label for="password">Passwort:*</label>
                    <input type="password"
                           name="password"
                           id="password"
                           class="form-control"
                           value=""
                           required
                           minlength="8"
                    />
                </div>
            </div>

            <div class="m-2 row">
                <div class="m-3 col-12 form-group" align="center">
                    <input type="submit" value="Registrieren" name="submit" class="btn btn-primary">
                    <a href="{{route('login')}}" class="btn btn-secondary">Zurück</a>
                </div>
            </div>
        </form>
    </div>
</x-guest-layout>
