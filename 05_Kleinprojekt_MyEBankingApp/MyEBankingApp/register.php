<?php
ob_start();
require_once "model/User.php";
require_once "model/Bankaccount.php";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
    <script type="application/javascript" src="js/index.js"></script>
    <title>Registrieren</title>
</head>
<body>
<div class="mt-5 container border border-secondary rounded">
    <h1 class="m-2 text-center">Registrieren</h1>
    <p class="m-3 text-center">Bitte geben Sie Ihre Kundendaten ein.</p>

    <div class="m-3">
        <?php
        $user = new User();
        if (isset($_POST['submit'])) {

            $user->setFirstname(isset($_POST['firstname']) ? $_POST['firstname'] : '');
            $user->setLastname(isset($_POST['lastname']) ? $_POST['lastname'] : '');
            $user->setEmail(isset($_POST['email']) ? $_POST['email'] : '');
            $user->setBirthdate(isset($_POST['birthdate']) ? $_POST['birthdate'] : '');
            $user->setUsername(isset($_POST['username']) ? $_POST['username'] : '');
            $user->setPassword(isset($_POST['password']) ? $_POST['password'] : '');


            if ($user->validateUserdata()) { //Zusätzliche Überprüfung bevor in Datenbank geschrieben wird

                try {
                    $userId = $user->create();
                    $bankaccount = new Bankaccount();
                    $bankaccount->setIban();
                    $bankaccount->setUserId($userId);
                    $bankaccountId = $bankaccount->create();
                    header("Location: index.php"); //zurück auf index
                    exit();
                } catch (PDOException $exception) {
                    echo "<div class='alert alert-danger'><p>Es ist ein Fehler beim Erstellen aufgetreten!</p><ul>";
                    //hier Exception - Eintrag aus Datenbank löschen
                    User::delete($userId);
                }

            } else {
                echo "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
                foreach ($user->getErrors() as $key => $value) { //$errors wird aus dem Script(valid.inc.php) verwendet (Warnung kann ignoriert werden)
                    echo "<li>" . $value . "</li>";
                }
                echo "</ul></div>";
            }
        }
        ?>
    </div>

    <form action="register.php" method="post" id="userLogin">

        <div class="m-2 row">
            <div class="col-sm-6 form-group">
                <label for="firstname">Vorname:*</label>
                <input type="text"
                       name="firstname"
                       id="firstname"
                       class="form-control <?= isset($user->getErrors()['name']) ? 'is-invalid' : '' ?>"
                       value="<?= $user->getFirstName() ?>"
                       required
                       minlength="0"
                       maxlength="20"
                />
            </div>
            <div class="col-sm-6 form-group">
                <label for="lastname">Nachname:*</label>
                <input type="text"
                       name="lastname"
                       id="lastname"
                       class="form-control <?= isset($user->getErrors()['name']) ? 'is-invalid' : '' ?>"
                       value="<?= $user->getLastName() ?>"
                       required
                       minlength="0"
                       maxlength="20"
                />
            </div>
        </div>

        <div class="m-2 row">
            <div class="col-sm-6 form-group">
                <label for="email">E-Mail:*</label>
                <input type="email"
                       name="email"
                       id="email"
                       class="form-control <?= isset($user->getErrors()['email']) ? 'is-invalid' : ''  ?>"
                       value="<?= $user->getEmail() ?>"
                       required
                />
            </div>
            <div class="col-sm-6 form-group">
                <label for="birthdate">Geburtsdatum:*</label>
                <input type="date"
                       name="birthdate"
                       id="birthdate"
                       onchange="validateBirthdate(this)"
                       class="form-control <?= isset($user->getErrors()['birthdate']) ? 'is-invalid' : '' ?>"
                       value="<?= $user->getBirthdate() ?>"
                       required
                />
            </div>
        </div>

        <div class="m-2 row">
            <div class="col-sm-6 form-group">
                <label for="username">Benutzername:*</label>
                <input type="text"
                       name="username"
                       id="username"
                       class="form-control <?= isset($user->getErrors()['username']) ? 'is-invalid' : '' ?>"
                       value="<?= $user->getUsername() ?>"
                       required
                       minlength="5"
                       maxlength="20"
                />
            </div>
            <div class="col-sm-6 form-group">
                <label for="password">Passwort:*</label>
                <input type="password"
                       name="password"
                       id="password"
                       class="form-control <?= isset($user->getErrors()['password']) ? 'is-invalid' : '' ?>"
                       value="<?= $user->getPassword() ?>"
                       required
                       minlength="8"
                />
            </div>
        </div>

        <div class="m-2 row">
            <div class="m-3 col-12 form-group" align="center">
                <p class="fe-button">
                    <input type="submit"
                           value="Registrieren"
                           name="submit"
                           class="btn btn-primary"
                           id="submit"
                    />
                    <a href="index.php" class="btn btn-secondary">Zurück</a>
            </div>
        </div>
    </form>
</div>
</body>
</html>