<?php
ob_start();
session_start();
require_once "model/User.php";
require_once "model/Bankaccount.php";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
    <title>Login</title>
</head>
<body class="bg-light">
<div class="mt-5 container border border-secondary rounded w-75">

    <h1 class="m-3 text-center">Wilkommen bei der Bank Ihres Vertrauens</h1>
    <h2 class="m-3 text-center">Login</h2>

    <p class="m-3 text-center">Melden sie sich mit Ihren Daten an.</p>

    <div class="m-3">
        <?php
        $username = '';
        $password = '';

        if (User::isLoggedIn()) {
            $user = User::get($_SESSION['userid']);
            $user->accessControl();
        }

        if (isset($_POST['submit'])) {
            $username = isset($_POST['username']) ? $_POST['username'] : '';
            $password = isset($_POST['password']) ? $_POST['password'] : '';

            $user = User::getUserByUsernameAndPassword($username, $password);
            if ($user != null) {
                echo "<p class='alert-success'>Richtige Eingabe</p>";
                // Rolle überprüfen und umleiten
                $user->accessControl();
            } else {
                echo "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
            }
        }
        ?>
    </div>

    <form action="index.php" method="post" id="userLogin">

        <div class="m-2 row">
            <div class="col-sm-12 form-group">
            <!--<label for="login">Benutzer</label>-->
            <input type="text"
                   name="username"
                   id="username"
                   placeholder="Benutzername"
                   class="form-control"
                   value="<?= htmlspecialchars($username) ?>"
                   required
            />
            </div>
        </div>
        <div class="m-2 mt-3 row">
            <div class="col-sm-12 form-group">
            <!--<label for="password">Passwort</label>-->
            <input type="password"
                   name="password"
                   id="password"
                   placeholder="Passwort"
                   class="form-control"
                   value="<?= htmlspecialchars($password) ?>"
                   required
            />
        </div>
        </div>
        <div class="m-3 row">
            <div class="col-12 form-group" align="center">
                <input type="submit"
                       value="Login"
                       name="submit"
                       class="btn btn-primary"
                       id="submit"
                />
                <a href="register.php" class="btn btn-secondary">Noch kein Konto?</a>
            </div>
        </div>
    </form>
</div>
</body>
</html>