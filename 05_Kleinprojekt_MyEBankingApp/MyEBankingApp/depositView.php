<?php
ob_start();
session_start();
require_once "model/User.php";
require_once "model/Bankaccount.php";
require_once "model/Transaction.php";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
    <title>Einzahlung durchführen</title>
</head>
<body class="bg-light">
<?php
if (!User::isLoggedIn()) {
    echo "<div class='mt-5 h1 text-center'>401: Unautorisierter Zugang";
    exit();
}

if (User::isLoggedIn()) {
    $user = User::get($_SESSION['userid']);
    if ($user->getRole() != 'Angestellter') {
        $user->accessControl();
    }
}

$userid = $_SESSION['userid'];
$user = new User();
$user = $user->get($userid);

$senderIban = Bankaccount::IBAN_BANK;
?>
<div class="mt-5 container rounded">
    <div class="row">
        <h1 class="m-2 mb-5 mt-3 text-center">Einzahlung durchführen</h1>
    </div>
    <div class="m-5 mb-2 mt-2">

        <?php
        $transaction = new Transaction();

        if (isset($_POST['submit'])) { //Formularverarbeitung Transaktion

            $transaction->setTransactionTime(date_format(date_create("now"), "Y-m-d H:i:s")); //Standard SQL Datetime
            $transaction->setSenderIban(Bankaccount::IBAN_BANK);
            $transaction->setReceiverIban(isset($_POST['iban']) ? $_POST['iban'] : '');
            $transaction->setReceiverBic(isset($_POST['bic']) ? $_POST['bic'] : '');
            $transaction->setPurpose(isset($_POST['purpose']) ? $_POST['purpose'] : '');
            $transaction->setAmount(isset($_POST['amount']) ? $_POST['amount'] : '');

            if ($transaction->validateTransactiondata()) { //ggf. noch Prüfung ob BIC zu IBAN passt

                $transactionDeposit = new Transaction();
                $transactionDeposit->setTransactionTime(date_format(date_create("now"), "Y-m-d H:i:s"));
                $transactionDeposit->setSenderIban(null);
                $transactionDeposit->setReceiverIban(Bankaccount::IBAN_BANK);
                $transactionDeposit->setReceiverBic($transaction->getReceiverBic());
                $transactionDeposit->setPurpose($transaction->getPurpose());
                $transactionDeposit->setAmount($transaction->getAmount());
                //überprüfen ob Amount nicht im Minus sein darf

                try {
                    $transactionDepositId = $transactionDeposit->create();
                    $transactionId = $transaction->create();
                    //Balance der betroffenen Bankaccounts aktualisieren
                    $transaction = Transaction::get($transactionId);

                    $senderBankaccount = Bankaccount::getByIban(Bankaccount::IBAN_BANK);
                    $senderBankaccount->calculateBalance($senderBankaccount->getIban());

                    $empfaengerBankaccount = Bankaccount::getByIban($transaction->getReceiverIban());
                    $empfaengerBankaccount->calculateBalance($empfaengerBankaccount->getIban());

                    header("Location: employeeView.php");
                    exit();
                } catch (PDOException $exception) {
                    echo "<div class='alert alert-danger'><p>Es ist ein Fehler beim Erstellen aufgetreten!</p><ul></div>";
                    Transaction::delete($transactionDepositId);
                }

            } else {
                echo "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
                foreach ($transaction->getErrors() as $key => $value) {
                    echo "<li>" . $value . "</li>";
                }
                echo "</ul></div>";
            }
        }
        ?>

        <div class="row mb-3">
            <div class="col-sm-6">
                Absenderkonto: <span
                        class="h6"><?= Bankaccount::IBAN_BANK ?>, <?= $user->getFirstName() . " " . $user->getLastName() ?></span>
            </div>
            <div class="col-sm-6">
                Absender BIC: <span class="h6"><?= Bankaccount::BIC_BANK ?></span>
            </div>
        </div>

        <form action="depositView.php" method="post">

            <div class="row mt-5">
                <div class="form-group col-sm-6">
                    <div class="input-group">
                        <span class="input-group-text">Empfängerkonto IBAN</span>
                        <select class="form-control <?= isset($transaction->getErrors()['iban']) ? 'is-invalid' : '' ?>"
                                name="iban">
                            <option>IBAN</option>
                            <?php
                            $bankaccounts = Bankaccount::getAll();
                            foreach ($bankaccounts as $bankaccount) {
                                if ($bankaccount->getIban() != Bankaccount::IBAN_BANK) {
                                    echo "<option value=" . $bankaccount->getIban() . ">" . $bankaccount->getIban() . "</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-6 form-group">
                    <div class="input-group">
                        <span class="input-group-text">Summe</span>
                        <input type="number"
                               name="amount"
                               id="amount"
                               class="form-control <?= isset($transaction->getErrors()['amount']) ? 'is-invalid' : '' ?>"
                               value="<?= $transaction->getAmount() ?>"
                               required
                               min="0"
                        />
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-sm-6 form-group">
                        <!--<label for="bic">Empfänger BIC:*</label>-->
                        <input type="hidden"
                               name="bic"
                               id="bic"
                               class="form-control <?= isset($transaction->getErrors()['bic']) ? 'is-invalid' : '' ?>"
                               value="<?= Bankaccount::BIC_BANK ?>"
                               required
                               minlength="8"
                               maxlength="11"
                        />
                    </div>

                    <div class="col-sm-6 form-group">
                        <!--<label for="purpose">Verwendungszweck:*</label>-->
                        <input type="hidden"
                               name="purpose"
                               id="purpose"
                               class="form-control <?= isset($transaction->getErrors()['purpose']) ? 'is-invalid' : '' ?>"
                               value="<?= "Einzahlung" ?>"
                               required
                               minlength="0"
                               maxlength="255"
                        />
                    </div>
                </div>

                <div class="row mt-4 mb-4">
                    <div class="col-6"></div>
                    <div class="col-3">
                        <input type="submit"
                               value="Überweisen"
                               name="submit"
                               class="btn btn-outline-primary"
                               id="submit"
                        />
                    </div>
                    <div class="col-3" align="right">
                        <a href="employeeView.php" class="btn btn-outline-secondary">Zurück</a>
                    </div>
                </div>


        </form>
    </div>
</div>
</body>
</html>