<?php

$filtererrors = [];

function validateDate($dateFrom, $dateTo)
{
    global $filtererrors;
    if ($dateFrom != '' && $dateTo != '') { //Validierung
        try {
            if (new DateTime($dateFrom) >= new DateTime() || new DateTime($dateFrom) < new DateTime() ||
                new DateTime($dateTo) >= new DateTime() || new DateTime($dateTo) < new DateTime()) {

                if (new DateTime($dateFrom) > new DateTime($dateTo)) {
                    $filtererrors['date'] = "Von-Datum muss kleiner als Bis-Datum sein";
                    return false;
                } else {
                    return true;
                }
            }
        } catch (Exception $e) { //durch Exception prüfen, ob Datum gültig ist - daher ist die obere if-Bedingung "unlogisch" (Methode checkdate() zu komplex)
            $filtererrors['date'] = "Es muss sich um Daten handeln, die nicht leer sein dürfen";
            return false;
        }
    } else {
        $filtererrors['date'] = "Daten dürfen nicht leer sein";
        return false;
    }
}

function validateAmount($amountFrom, $amountTo)
{
    global $filtererrors;
    if ($amountFrom != '' && $amountTo != '' && $amountFrom >= 0 && is_numeric($amountFrom) && is_numeric($amountTo)) { //Validierung
        return true;
    } else {
        $filtererrors['amount'] = "Es muss sich um Beträge handeln die nicht leer sein dürfen";
        return false;
    }
}

function validateText($text)
{
    global $filtererrors;
    if (strlen($text) != 0) { //Validierung
        return true;
    } else {
        $filtererrors['text'] = "Textsuche darf nicht leer sein";
        return false;
    }
}
