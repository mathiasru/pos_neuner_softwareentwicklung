/**
 * Die Funktion überprüft das übergebene Datum auf Gültigkeit und.
 * @param element
 */
function validateBirthdate(element) {
    let today = new Date(); //erzeugen eines neunen Datumsobjektes (aktuelles Datum)
    let examDate = new Date(element.value); //erzeugen eines Datumsobjektes mit dem übergebenen Wert
    examDate.setHours(0, 0, 0, 0); //um nur das Datum zu prüfen (evtl. Fehlerquelle)

    if (examDate <= today) { //vergleichen der beiden Datumswerten
        element.classList.add("is-valid"); //durch integrierte Bootstrap css (is-valid )wird das Eingabefeld grün aktiviert
        element.classList.remove("is-invalid"); //durch integrierte Bootstrap css (is-valid )wird das rote Eingabefeld deaktiviert
    } else {
        element.classList.add("is-invalid");
        element.classList.remove("is-valid");
    }

    console.log(examDate); //Ausgabe auf der Konsole
}