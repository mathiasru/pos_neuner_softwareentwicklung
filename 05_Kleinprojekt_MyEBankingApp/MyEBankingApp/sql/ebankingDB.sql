DROP SCHEMA IF EXISTS `ebankingDB`;
CREATE SCHEMA IF NOT EXISTS `ebankingDB` charset utf8;

USE ebankingDB;

DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users(
	id INT NOT NULL auto_increment,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    birthdate DATETIME NOT NULL,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(255) NOT NULL,
    CONSTRAINT pk_id PRIMARY KEY (id),
    CONSTRAINT u_email UNIQUE INDEX (email),
    CONSTRAINT u_username UNIQUE INDEX (username)
);

DROP TABLE IF EXISTS bankaccounts;
CREATE TABLE IF NOT EXISTS bankaccounts(
    id INT NOT NULL AUTO_INCREMENT,
    iban VARCHAR(30) NOT NULL,
    bic VARCHAR(11) NOT NULL,
    balance INT NOT NULL,
    user_id INT,
    CONSTRAINT pk_iban PRIMARY KEY (iban),
    CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(id)
);

DROP TABLE IF EXISTS transactions;
CREATE TABLE IF NOT EXISTS transactions(
	id INT NOT NULL auto_increment,
    amount double NOT NULL,
    purpose VARCHAR(255) NOT NULL,
    transaction_time DATETIME NOT NULL,
    sender_iban VARCHAR(255),
    receiver_iban VARCHAR(255),
    receiver_bic VARCHAR(255),
    CONSTRAINT pk_id PRIMARY KEY (id),
    CONSTRAINT fk_sender_iban FOREIGN KEY (sender_iban) REFERENCES bankaccounts(iban),
    CONSTRAINT fk_receiver_iban FOREIGN KEY (receiver_iban) REFERENCES bankaccounts(iban)
);>