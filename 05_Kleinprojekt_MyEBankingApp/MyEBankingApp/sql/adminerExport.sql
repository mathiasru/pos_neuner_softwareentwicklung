-- Adminer 4.8.0 MySQL 5.5.5-10.6.5-MariaDB-1:10.6.5+maria~focal dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `bankaccounts`;
CREATE TABLE `bankaccounts`
(
    `id`      int(11)     NOT NULL AUTO_INCREMENT,
    `iban`    varchar(30) NOT NULL,
    `bic`     varchar(11) NOT NULL,
    `balance` int(11)     NOT NULL,
    `user_id` int(11) DEFAULT NULL,
    PRIMARY KEY (`iban`),
    UNIQUE KEY `id` (`id`),
    KEY `fk_user_id` (`user_id`),
    CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

INSERT INTO `bankaccounts` (`id`, `iban`, `bic`, `balance`, `user_id`)
VALUES (5, 'AT25205020000876921435', 'SPIMAT25XXX', 0, 8),
       (1, 'AT25205020001234567890', 'SPIMAT25XXX', 10000, NULL),
       (2, 'AT25205020002541768309', 'SPIMAT25XXX', 1135, 4),
       (3, 'AT25205020003621790458', 'SPIMAT25XXX', -30, 2),
       (4, 'AT25205020008921354067', 'SPIMAT25XXX', -725, 1);

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT,
    `amount`           double       NOT NULL,
    `purpose`          varchar(255) NOT NULL,
    `transaction_time` datetime     NOT NULL,
    `sender_iban`      varchar(255) DEFAULT NULL,
    `receiver_iban`    varchar(255) DEFAULT NULL,
    `receiver_bic`     varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_sender_iban` (`sender_iban`),
    KEY `fk_receiver_iban` (`receiver_iban`),
    CONSTRAINT `fk_receiver_iban` FOREIGN KEY (`receiver_iban`) REFERENCES `bankaccounts` (`iban`),
    CONSTRAINT `fk_sender_iban` FOREIGN KEY (`sender_iban`) REFERENCES `bankaccounts` (`iban`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

INSERT INTO `transactions` (`id`, `amount`, `purpose`, `transaction_time`, `sender_iban`, `receiver_iban`,
                            `receiver_bic`)
VALUES (1, 10, 'Iboprofaxe', '2022-01-27 16:47:25', 'AT25205020008921354067', 'AT25205020003621790458', 'SPIMAT25XXX'),
       (3, 20, 'Testetest', '2022-01-27 18:11:51', 'AT25205020008921354067', 'AT25205020003621790458', 'SPIMAT25XXX'),
       (4, 20, 'Sachen Machen', '2022-01-27 18:13:00', 'AT25205020008921354067', 'AT25205020003621790458',
        'SPIMAT25XXX'),
       (31, 120, 'testetest', '2022-02-03 21:10:36', 'AT25205020008921354067', 'AT25205020002541768309', 'SPIMAT25XXX'),
       (32, 1000, 'test2', '2022-02-03 21:11:05', 'AT25205020008921354067', 'AT25205020002541768309', 'SPIMAT25XXX'),
       (41, 350, 'Einzahlung', '2022-02-03 22:18:05', NULL, 'AT25205020001234567890', 'SPIMAT25XXX'),
       (42, 350, 'Einzahlung', '2022-02-03 22:18:05', 'AT25205020001234567890', 'AT25205020008921354067',
        'SPIMAT25XXX'),
       (46, 350, 'Auszahlung', '2022-02-03 22:23:21', 'AT25205020008921354067', 'AT25205020001234567890',
        'SPIMAT25XXX'),
       (47, 350, 'Auszahlung', '2022-02-03 22:23:21', 'AT25205020001234567890', NULL, NULL),
       (48, 10, 'Einzahlung', '2022-02-04 08:13:28', NULL, 'AT25205020001234567890', 'SPIMAT25XXX'),
       (49, 10, 'Einzahlung', '2022-02-04 08:13:28', 'AT25205020001234567890', 'AT25205020002541768309', 'SPIMAT25XXX'),
       (51, 10000, 'Einzahlung', '2022-02-04 08:13:28', NULL, 'AT25205020001234567890', 'SPIMAT25XXX'),
       (52, 5, '123', '2022-02-04 13:26:16', 'AT25205020008921354067', 'AT25205020002541768309', 'SPIMAT25XXX'),
       (53, 100, 'Einzahlung', '2022-02-04 13:34:26', NULL, 'AT25205020001234567890', 'SPIMAT25XXX'),
       (54, 100, 'Einzahlung', '2022-02-04 13:34:26', 'AT25205020001234567890', 'AT25205020008921354067',
        'SPIMAT25XXX'),
       (55, 350, 'Paypal', '2022-02-03 22:23:21', 'AT25205020001234567890', 'AT25205020008921354067', 'SPIMAT25XXX');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`
(
    `id`         int(11)      NOT NULL AUTO_INCREMENT,
    `first_name` varchar(255) NOT NULL,
    `last_name`  varchar(255) NOT NULL,
    `email`      varchar(255) NOT NULL,
    `birthdate`  datetime     NOT NULL,
    `username`   varchar(255) NOT NULL,
    `password`   varchar(255) NOT NULL,
    `role`       varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `u_email` (`email`),
    UNIQUE KEY `u_username` (`username`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `username`, `password`, `role`)
VALUES (1, 'Mathias', 'Rudig', 'mathias.rudig@icloud.com', '2022-01-03 00:00:00', 'mathiasrudig', 'mathiasrudig',
        'Kunde'),
       (2, 'Tobias', 'Tilg', 'mathias.rudig@siemens.com', '2022-01-03 00:00:00', 'tobiastilg', 'tobiastilg', 'Kunde'),
       (3, 'Clemens', 'Kerber', 'clemens.kerber@vuschnann.at', '2022-01-03 00:00:00', 'clemenskerber', 'clemenskerber',
        'Angestellter'),
       (4, 'Dario', 'Skocibusic', 'dario@ichVerwendeVbox.com', '2022-01-17 00:00:00', 'darioskocibusic',
        'darioskocibusic', 'Kunde'),
       (6, 'Bank', 'Admin', 'admin@ichVerwendeVbox.com', '2022-01-17 00:00:00', 'administrator', 'administrator',
        'Admin'),
       (8, 'Philipp', 'Maar', 'maar@icloud.com', '2022-02-01 00:00:00', 'maarphilipp', 'maarphilipp', 'Kunde');

-- 2022-02-04 14:16:26