<?php

require_once "DatabaseObject.php";

class Transaction implements DatabaseObject
{
    private $id;
    private $amount = '';
    private $purpose = '';
    private $transaction_time = '';
    private $sender_iban = '';
    private $receiver_iban = '';
    private $receiver_bic = '';

    private $errors = [];

    public function create()
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "INSERT INTO transactions (amount, purpose, transaction_time, sender_iban, receiver_iban, receiver_bic)
                            values (:amount, :purpose, :transaction_time, :sender_iban, :receiver_iban, :receiver_bic)";
        $stmt = $db->prepare($sql);
        //Schutz vor Dependency Injection
        $stmt->bindParam(':amount', $this->amount);
        $stmt->bindParam(':purpose', $this->purpose);
        $stmt->bindParam(':transaction_time', $this->transaction_time);
        $stmt->bindParam(':sender_iban', $this->sender_iban);
        $stmt->bindParam(':receiver_iban', $this->receiver_iban);
        $stmt->bindParam(':receiver_bic', $this->receiver_bic);
        $stmt->execute(); // In Datenbank schreiben
        $lastId = $db->lastInsertId();  // ID vom zuletz gespeicherten Datensatz
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        return $lastId;
    }

    public function update()
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "UPDATE transactions SET amount = :amount, purpose = :purpose, transaction_time = :transaction_time, 
                       sender_iban = :sender_iban, receiver_iban = :receiver_iban, receiver_bic = :receiver_bic WHERE id = :id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':amount', $this->amount);
        $stmt->bindParam(':purpose', $this->purpose);
        $stmt->bindParam(':transaction_time', $this->transaction_time);
        $stmt->bindParam(':sender_iban', $this->sender_iban);
        $stmt->bindParam(':receiver_iban', $this->receiver_iban);
        $stmt->bindParam(':receiver_bic', $this->receiver_bic);
        $stmt->execute(); // in Datenbank schreiben
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
    }

    public static function get($id)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM transactions WHERE  id = :id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':id', $id);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Transaction'); // Klasse für ORM definieren
        $stmt->execute(); // in Datenbank schreiben
        $transaction = $stmt->fetch(PDO::FETCH_CLASS); // ORM - erzeugen eines Users
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        // Wenn kein User gefunden wurde, wird NULL zurückgegeben
        return $transaction !== false ? $transaction : null;
    }

    public static function getAll()
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM transactions ORDER BY transaction_time"; // SenderIban prüfen !!!
        $stmt = $db->prepare($sql);
        $stmt->execute(); // in Datenbank schreiben
        // ORM - erzeugen eines Users
        $transactions = $stmt->fetchAll(PDO::FETCH_CLASS, 'Transaction'); // ORM
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        return $transactions;
    }

    public static function getAllBySenderOrReceiverIban($sender_iban)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM transactions WHERE sender_iban = :sender_iban OR receiver_iban = :receiver_iban ORDER BY transaction_time"; // SenderIban prüfen !!!
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':sender_iban', $sender_iban);
        $stmt->bindParam(':receiver_iban', $sender_iban);
        $stmt->execute(); // in Datenbank schreiben
        // ORM - erzeugen eines Users
        $transactions = $stmt->fetchAll(PDO::FETCH_CLASS, 'Transaction'); // ORM
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        return $transactions;
    }

    public static function delete($id)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "DELETE FROM transactions WHERE id = :id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':id', $id);
        $stmt->execute(); // in Datenbank schreiben
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
    }

    public static function getByDates($dateForm, $dateTo, $sender_iban)
    {
        $db = DatabaseAccess::connect(); //Verbindung aufbauen
        $sql = "SELECT * FROM transactions WHERE (sender_iban = :sender_iban OR receiver_iban = :receiver_iban) AND 
        transaction_time BETWEEN :dateForm AND DATE_ADD(:dateTo, INTERVAL 1 DAY) ORDER BY transaction_time DESC";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':dateForm', $dateForm);
        $stmt->bindParam(':dateTo', $dateTo);
        $stmt->bindParam(':sender_iban', $sender_iban);
        $stmt->bindParam(':receiver_iban', $sender_iban);
        $stmt->execute(); //in Datenbank schreiben
        //ORM - erzeugen eines Users
        $transactions = $stmt->fetchAll(PDO::FETCH_CLASS, 'Transaction'); //ORM
        DatabaseAccess::disconnect(); //schließen der Verbindung, da nur eine existieren darf
        return $transactions;
    }

    public static function getByAmounts($amountFrom, $amountTo, $sender_iban)
    {
        $db = DatabaseAccess::connect(); //Verbindung aufbauen
        $sql = "SELECT * FROM transactions WHERE (sender_iban = :sender_iban OR receiver_iban = :receiver_iban) AND
        amount BETWEEN :amountFrom AND :amountTo ORDER BY transaction_time DESC";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':amountFrom', $amountFrom);
        $stmt->bindParam(':amountTo', $amountTo);
        $stmt->bindParam(':sender_iban', $sender_iban);
        $stmt->bindParam(':receiver_iban', $sender_iban);
        $stmt->execute(); //in Datenbank schreiben
        //ORM - erzeugen eines Users
        $transactions = $stmt->fetchAll(PDO::FETCH_CLASS, 'Transaction'); //ORM
        DatabaseAccess::disconnect(); //schließen der Verbindung, da nur eine existieren darf
        return $transactions;
    }

    public static function getByText($text, $sender_iban)
    {
        $db = DatabaseAccess::connect(); //Verbindung aufbauen
        $sql = "SELECT * FROM transactions WHERE (sender_iban = :sender_iban OR receiver_iban = :receiver_iban) AND 
        purpose LIKE CONCAT('%', :text ,'%') ORDER BY transaction_time DESC";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':text', $text);
        $stmt->bindParam(':sender_iban', $sender_iban);
        $stmt->bindParam(':receiver_iban', $sender_iban);
        $stmt->execute(); //in Datenbank schreiben
        //ORM - erzeugen eines Users
        $transactions = $stmt->fetchAll(PDO::FETCH_CLASS, 'Transaction'); //ORM
        DatabaseAccess::disconnect(); //schließen der Verbindung, da nur eine existieren darf
        return $transactions;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function getPurpose()
    {
        return $this->purpose;
    }

    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;
    }

    public function getTransactionTime()
    {
        return $this->transaction_time;
    }

    public function setTransactionTime($transaction_time)
    {
        $this->transaction_time = $transaction_time;
    }

    public function getSenderIban()
    {
        return $this->sender_iban;
    }


    public function setSenderIban($sender_iban)
    {
        $this->sender_iban = $sender_iban;
    }

    public function getReceiverIban()
    {
        return $this->receiver_iban;
    }

    public function setReceiverIban($receiver_iban)
    {
        $this->receiver_iban = $receiver_iban;
    }

    public function getReceiverBic()
    {
        return $this->receiver_bic;
    }

    public function setReceiverBic($receiver_bic)
    {
        $this->receiver_bic = $receiver_bic;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function validateTransactiondata()
    {
        return $this->validateReceiverIban($this->receiver_iban) & $this->validateSenderIban($this->sender_iban) & $this->validateBic($this->receiver_bic) &
            $this->validatePurpose($this->purpose) & $this->validateAmount($this->amount);
    }

    private function validateReceiverIban($iban)
    {
        if ($iban == $this->sender_iban) {
            $this->errors['iban'] = "Sie können sich nicht selbst etwas überweisen.";
            return false;
        } elseif (strlen($iban) < 22 | strlen($iban) > 30) {
            $this->errors['iban'] = "IBAN fehlerhaft.";
            return false;
        } elseif (!Bankaccount::ifIbanExists($iban)) {
            $this->errors['iban'] = "Es wurde kein Konto mit diesem IBAN gefunden.";
            return false;
        } else {
            return true;
        }
    }

    private function validateSenderIban($iban)
    {
        if (strlen($iban) < 22 | strlen($iban) > 30) {
            $this->errors['iban'] = "IBAN fehlerhaft.";
            return false;
        } elseif (!Bankaccount::ifIbanExists($iban)) {
            $this->errors['iban'] = "Es wurde kein Konto mit diesem IBAN gefunden.";
            return false;
        } else {
            return true;
        }
    }

    private function validateBic($bic)
    {
        if (strlen($bic) !== 11 & strlen($bic) !== 8) {
            $this->errors['bic'] = "BIC fehlerhaft.";
            return false;
        } elseif (!Bankaccount::ifBicExists($bic)) {
            $this->errors['bic'] = "Es wurde kein Konto mit diesem BIC gefunden.";
            return false;
        } else {
            return true;
        }
    }

    private function validatePurpose($purpose)
    {
        if (strlen($purpose) == 0) {
            $this->errors['purpose'] = "Verwendungszweck darf nicht leer sein.";
            return false;
        } elseif (strlen($purpose) > 255) {
            $this->errors['purpose'] = "Verwendungszweck darf nicht mehr als 255 Zeichen lang sein.";
            return false;
        } else {
            return true;
        }
    }

    private function validateAmount($amount)
    {
        //falls Konto zB nicht ins Minus darf, hier überprüfen
        if ($amount <= 0) {
            $this->errors['amount'] = "Die zu überweisende Summe muss größer als 0 sein.";
            return false;
        } else {
            return true;
        }
    }


}