<?php
require_once "DatabaseAccess.php";
/**
 * Das Interface gibt uns die Standard-Funktionen für CRUD-Operationen vor.
 */
interface DatabaseObject
{

    public function create();

    public function update();

    public static function get($id);

    public static function getAll();

    public static function delete($id);

}