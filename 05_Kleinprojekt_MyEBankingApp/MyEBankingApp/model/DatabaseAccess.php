<?php

class DatabaseAccess
{

    private static $dbName = 'ebankingDB';
    private static $dbHost = '10.211.55.4';
    private static $dbUsername = 'root';
    private static $dbUserPassword = '123';

    private static $conn = null;

    private function __construct()
    {
        exit('Init function is not allowed');
    }

    public static function connect()
    {
        //Singelton Pattern (nur eine Instanz im gesamten Projekt)
        if (null == self::$conn) {
            try {
                self::$conn = new PDO("mysql:host=" . self::$dbHost . ";" . "dbname=" . self::$dbName . ';charset=utf8mb4', self::$dbUsername, self::$dbUserPassword);
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
        self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$conn;
    }

    public static function disconnect()
    {
        self::$conn = null;
    }

}