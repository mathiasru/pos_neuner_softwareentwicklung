<?php

require_once "DatabaseObject.php";

class User implements DatabaseObject
{
    private $id = 0;
    private $first_name = '';
    private $last_name = '';
    private $email = '';
    private $birthdate = '';
    private $username = '';
    private $password = '';
    private $role = 'Kunde';

    private $errors = [];

    // Crud Methoden
    public function create()
    {
        $db = DatabaseAccess::connect(); //Verbindung aufbauen
        $sql = "INSERT INTO users (first_name, last_name, email, birthdate, username, password, role)
                values (:first_name, :last_name, :email, :birthdate, :username, :password, :role)";
        $stmt = $db->prepare($sql);
        //Schutz vor Dependency Injection
        $stmt->bindParam(':first_name', $this->first_name);
        $stmt->bindParam(':last_name', $this->last_name);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':birthdate', $this->birthdate);
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':role', $this->role);
        $stmt->execute(); //in Datenbank schreiben
        $lastId = $db->lastInsertId();  //ID vom zuletzt gespeicherten Datensatz
        DatabaseAccess::disconnect(); //Schließen der Verbindung, da nur eine existieren darf
        return $lastId;
    }

    public function update()
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "UPDATE users SET first_name = :first_name, last_name = :last_name, email = :email,
                birthdate = :birthdate, username = :username, password = :password, role = :role WHERE id = :id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':id', $this->id);
        $stmt->bindParam(':first_name', $this->first_name);
        $stmt->bindParam(':last_name', $this->last_name);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':birthdate', $this->birthdate);
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':role', $this->role);
        $stmt->execute(); // in Datenbank schreiben
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
    }

    public static function get($id)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM users WHERE id = :id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':id', $id);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User'); // Klasse für ORM definieren
        $stmt->execute(); // in Datenbank schreiben
        $user = $stmt->fetch(PDO::FETCH_CLASS); // ORM - erzeugen eines Users
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        // Wenn kein User gefunden wurde, wird NULL zurückgegeben
        return $user !== false ? $user : null;
    }


    public static function getUserByUsernameAndPassword($username, $password)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM users WHERE username = :username AND password = :password";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':password', $password);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User'); // Klasse für ORM definieren
        $stmt->execute(); // in Datenbank schreiben
        $user = $stmt->fetch(PDO::FETCH_CLASS); // ORM - erzeugen eines Users
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        // Wenn kein User gefunden wurde, wird NULL zurückgegeben
        return $user !== false ? $user : null;
    }

    public static function getAll()
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM users ORDER BY first_name";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->execute(); // in Datenbank schreiben
        // ORM - erzeugen eines Users
        $users = $stmt->fetchAll(PDO::FETCH_CLASS, 'User'); // ORM - erzeugen eines Users
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        return $users;
    }

    public static function delete($id)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "DELETE FROM users WHERE id = :id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':id', $id);
        $stmt->execute(); // in Datenbank schreiben
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
    }

    private function ifUsernameExists($username)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM users WHERE username = :username";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':username', $username);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User'); // Klasse für ORM definieren
        $stmt->execute(); // in Datenbank schreiben
        $user = $stmt->fetch(PDO::FETCH_CLASS); // ORM - erzeugen eines Users
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        // Wenn kein User gefunden wurde, wird NULL zurückgegeben
        return $user !== false ? true : false;
    }

    private function ifEmailExists($email)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM users WHERE email = :email";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':email', $email);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User'); // Klasse für ORM definieren
        $stmt->execute(); // in Datenbank schreiben
        $user = $stmt->fetch(PDO::FETCH_CLASS); // ORM - erzeugen eines Users
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        // Wenn kein User gefunden wurde, wird NULL zurückgegeben
        return $user !== false ? true : false;
    }

    public function accessControl(){
        if ($this->getRole() == 'Kunde'){
            $_SESSION['userid'] = $this->getId();
            header("Location: customerView.php");
            exit();
        } else if ($this->getRole() == 'Angestellter') {
            $_SESSION['userid'] = $this->getId();
            header("Location: employeeView.php");
            exit();
        }
    }

    public static function isLoggedIn()
    {
        if (isset($_SESSION['userid'])) {
            return true;
        } else {
            return false;
        }
    }

    public function getId()
    {
        return $this->id;
    }


    public function setFirstname($first_name)
    {
        $this->first_name = $first_name;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function setLastname($last_name)
    {
        $this->last_name = $last_name;

    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    public function getBirthdate()
    {
        $sqldate = date_create($this->birthdate);
        $date = date_format($sqldate, "d.m.Y");
        return $date;
    }

    public function setUsername($username)
    {
            $this->username = $username;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setPassword($password)
    {
            $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRole()
    {
        return $this->role;
    }


    public function getErrors()
    {
        return $this->errors;
    }

    // Validierungsmethoden
    public function validateUserdata()
    {
        return $this->validateName($this->first_name) & $this->validateName($this->last_name) & $this->validateEmail($this->email) &
            $this->validateBirthdate($this->birthdate) & $this->validateUsername($this->username) & $this->validatePassword($this->password);
    }

    public function validateName($name)
    {
        if (strlen($name) == 0) { //strlen prüft die Länge eines Strings
            $this->errors['name'] = "Name darf nicht leer sein"; //schreiben in das globalle Array
            return false;
        } elseif (strlen($name) > 20) {
            $this->errors['name'] = "Name darf nicht mehr als 20 Zeichen lang sein";
            return false;
        } else {
            return true;
        }
    }

    public function validateEmail($email)
    {
        if ($email != "" && !filter_var($email, FILTER_VALIDATE_EMAIL)) { //prüfen auf leer und gültig
            $this->errors['email'] = "E-Mail ungültig";
            return false;
        } else if ($this->ifEmailExists($email)) {
            $this->errors['email'] = "Es existiert bereits ein Account mit dieser Email";
            return false;
        } else {
            return true;
        }
    }

    public function validateBirthdate($birthdate)
    {
        try {
            if ($birthdate == "") {
                $this->errors['birthdate'] = "Geburtsdatum darf nicht leer sein";
                return false;
            } else if (new DateTime($birthdate) > new DateTime()) {
                $this->errors['birthdate'] = "Geburtsdatum darf nicht in der Zukunft liegen";
                return false;
            } else {
                return true;
            }
        } catch (Exception $exception) {
            $this->errors['birthdate'] = "Geburtsdatum ungültig";
            return false;
        }
    }

    public function validateUsername($username)
    {
        if (strlen($username) < 5) {
            $this->errors['username'] = "Der Benutzername muss mindestens 5 Zeichen lang sein";
            return false;
        } else if (strlen($username) > 20) {
            $this->errors['username'] = "Der Benutzername darf nicht mehr als 20 lang sein";
            return false;
        } else if ($this->ifUsernameExists($username)) {
            $this->errors['username'] = "Dieser Username ist bereits vergeben";
            return false;
        } else {
            return true;
        }
    }

    public function validatePassword($password)
    {
        if (strlen($password) < 8) {
            $this->errors['password'] = "Passwortlänge beachten (min. 8 Zeichen)";
            return false;
        } else {
            return true;
        }
    }
}