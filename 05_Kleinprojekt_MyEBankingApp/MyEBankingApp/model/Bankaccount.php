<?php

require_once "DatabaseObject.php";

class Bankaccount implements DatabaseObject
{
    const COUNTRY_CODE = 'AT';
    const CHECKSUM = '25';
    const BANKNUMBER = '20502000';
    const IBAN_BANK = 'AT25205020001234567890';
    const BIC_BANK = 'SPIMAT25XXX';

    private $id = '';
    private $iban = '';
    private $bic = 'SPIMAT25XXX'; // Ist pro Bankfiliale unique
    private $balance = 0;
    private $user_id = 0;

    private $errors = [];

    // Crud Methoden
    public function create()
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "INSERT INTO bankaccounts (iban, bic, balance, user_id) values (:iban, :bic, :balance, :user_id)";
        $stmt = $db->prepare($sql);
        //Schutz vor Dependency Injection
        $stmt->bindParam(':iban', $this->iban);
        $stmt->bindParam(':bic', $this->bic);
        $stmt->bindParam(':balance', $this->balance);
        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->execute(); // In Datenbank schreiben
        $lastId = $db->lastInsertId();  // ID vom zuletz gespeicherten Datensatz
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        return $lastId;
    }

    public function update()
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "UPDATE bankaccounts SET iban = :iban, bic = :bic, balance = :balance, user_id = :user_id WHERE iban = :iban";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':id', $this->id);
        $stmt->bindParam(':iban', $this->iban);
        $stmt->bindParam(':bic', $this->bic);
        $stmt->bindParam(':balance', $this->balance);
        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->execute(); // in Datenbank schreiben
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
    }

    public static function get($user_id)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM bankaccounts WHERE  user_id = :user_id";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':user_id', $user_id);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Bankaccount'); // Klasse für ORM definieren
        $stmt->execute(); // in Datenbank schreiben
        $bankaccount = $stmt->fetch(PDO::FETCH_CLASS); // ORM - erzeugen eines Users
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        // Wenn kein User gefunden wurde, wird NULL zurückgegeben
        return $bankaccount !== false ? $bankaccount : null;
    }

    public static function getByIban($iban)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM bankaccounts WHERE  iban = :iban";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':iban', $iban);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Bankaccount'); // Klasse für ORM definieren
        $stmt->execute(); // in Datenbank schreiben
        $bankaccount = $stmt->fetch(PDO::FETCH_CLASS); // ORM - erzeugen eines Users
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        // Wenn kein User gefunden wurde, wird NULL zurückgegeben
        return $bankaccount !== false ? $bankaccount : null;
    }

    public static function getAll()
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM bankaccounts ORDER BY iban";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->execute(); // in Datenbank schreiben
        // ORM - erzeugen eines Users
        $bankaccounts = $stmt->fetchAll(PDO::FETCH_CLASS, 'Bankaccount'); // ORM
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        return $bankaccounts;
    }

    public static function delete($iban)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "DELETE FROM bankaccounts WHERE iban = :iban";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':iban', $iban);
        $stmt->execute(); // in Datenbank schreiben
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
    }

    private function generateIban()
    {
        //überprüfen ob IBAN schon existiert
        $iban = self::COUNTRY_CODE . self::CHECKSUM . self::BANKNUMBER;
        $data = '1234567890';
        $iban .= substr(str_shuffle($data), 0, 10);
        return $iban;
    }

    public function setIban()
    {
        if (strlen($this->iban) == 0) {
            $generatedIban = '';
            do {
                $generatedIban = self::generateIban();
            } while (Bankaccount::ifIbanExists($generatedIban));
            $this->iban = $this->generateIban();
        }
    }

    //Liefert true zurück wenn er existiert
    public static function ifIbanExists($iban)
    {
        $db = DatabaseAccess::connect(); // Verbindung aufbauen
        $sql = "SELECT * FROM bankaccounts WHERE iban = :iban";
        $stmt = $db->prepare($sql);
        // Schutz vor Dependency Injection
        $stmt->bindParam(':iban', $iban);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User'); // Klasse für ORM definieren
        $stmt->execute(); // in Datenbank schreiben
        $user = $stmt->fetch(PDO::FETCH_CLASS); // ORM - erzeugen eines Users
        DatabaseAccess::disconnect(); // Schließen der Verbindung, da nur eine Existieren darf
        // Wenn kein User gefunden wurde, wird NULL zurückgegeben
        return $user !== false ? true : false;
    }

    public static function ifBicExists($bic)
    {
        $db = DatabaseAccess::connect(); //Verbindung aufbauen
        $sql = "SELECT * FROM bankaccounts WHERE bic = :bic";
        $stmt = $db->prepare($sql);
        //Schutz vor Dependency Injection
        $stmt->bindParam(':bic', $bic);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Bankaccount'); //Klasse für ORM definieren
        $stmt->execute(); //SQL Befehl ausführen
        $bankaccount = $stmt->fetch(PDO::FETCH_CLASS); //ORM
        DatabaseAccess::disconnect(); //schließen der Verbindung
        return $bankaccount !== false ? true : false;
    }

    public function calculateBalance($iban)
    {
        $balance = 0;
        $transactions = Transaction::getAllBySenderOrReceiverIban($iban);

        foreach ($transactions as $transaction) {
            $this->getIban() == $transaction->getReceiverIban() ? ($balance += $transaction->getAmount()) : $transaction->getReceiverIban();
            $this->getIban() == $transaction->getSenderIban() ? ($balance -= $transaction->getAmount()) : $transaction->getSenderIban();
        }

        if ($balance == $this->balance){
            return $this->balance;
        } else {
            $this->setBalance($balance);
            $this->update();
            return $balance; //muss hier vom Objekt sein??
        }

    }

    public function getId()
    {
        return $this->id;
    }

    public function getIban()
    {
        return $this->iban;
    }

    public function getBic()
    {
        return $this->bic;
    }

    public function setBalance($balance)
    {
        $this->balance = $balance;
    }

    public function getBalance()
    {
        return $this->balance;
    }


    public function setUserId($user_id)
    {
        if ($this->user_id == 0) {
            $this->user_id = $user_id;
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }
}