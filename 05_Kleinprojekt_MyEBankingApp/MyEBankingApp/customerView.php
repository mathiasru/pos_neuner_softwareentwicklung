<?php
ob_start();
session_start();
require_once "model/User.php";
require_once "model/Bankaccount.php";
require_once "model/Transaction.php";
require "lib/filter.func.inc.php";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
    <title>Kontoübersicht</title>
</head>
<body class="bg-light">
<?php

if (!User::isLoggedIn()) {
    echo "<div class='mt-5 h1 text-center'>401: Unautorisierter Zugang";
    exit();
}

if (User::isLoggedIn()) {
    $user = User::get($_SESSION['userid']);
    if ($user->getRole() != 'Kunde') {
        $user->accessControl();
    }
}

if (isset($_POST['logout'])) { //Formularverarbeitung Logout
    session_destroy();
    header("Location: index.php");
    exit();
}

$gesuchtDatum = false;
$gesuchtBetraege = false;
$gesuchtText = false;

$dateFrom = '';
$dateTo = '';
$amountFrom = '';
$amountTo = '';
$text = '';

if (isset($_POST['searchDate'])) { //Formularverarbeitung Datumssuche
    $dateFrom = isset($_POST['dateFrom']) ? $_POST['dateFrom'] : '';
    $dateTo = isset($_POST['dateTo']) ? $_POST['dateTo'] : '';

    if (validateDate($dateFrom, $dateTo)) { //Validierung
        $gesuchtDatum = true;
    }
}

if (isset($_POST['searchAmount'])) { //Formularverarbeitung Betragssuche
    $amountFrom = isset($_POST['amountFrom']) ? $_POST['amountFrom'] : '';
    $amountTo = isset($_POST['amountTo']) ? $_POST['amountTo'] : '';

    if (validateAmount($amountFrom, $amountTo)) {
        $gesuchtBetraege = true;
    }
}

if (isset($_POST['searchText'])) { //Formularverarbeitung Textsuche
    $text = isset($_POST['text']) ? $_POST['text'] : '';

    if (validateText($text)) {
        $gesuchtText = true;
    }
}

$userid = $_SESSION['userid'];
$user = new User();
$user = $user->get($userid);

$bankaccount = new Bankaccount();
$bankaccount = $bankaccount->get($userid);

?>
<div class="mt-5 container "> <!--border border-secondary rounded-->
    <div class="row rounded-3">
        <h1 class="m-2 mb-3 mt-3 text-center">Kontoübersicht</h1>
    </div>
    <div class="row mt-4">
        <div class="col-sm-4 mb-5 mt-5">
            <div class="mt-5 mx-4">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item rounded-3">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                             class="bi bi-person" viewBox="0 0 16 16">
                            <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                        </svg>
                        <?= $user->getFirstName() . " " . $user->getLastName() ?>
                    </li>
                    <li class="list-group-item rounded-3">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                             class="bi bi-calendar" viewBox="0 0 16 16">
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                        </svg>
                        <?= $user->getBirthdate() ?>
                    </li>
                    <li class="list-group-item rounded-3">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                             class="bi bi-envelope" viewBox="0 0 16 16">
                            <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4Zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2Zm13 2.383-4.708 2.825L15 11.105V5.383Zm-.034 6.876-5.64-3.471L8 9.583l-1.326-.795-5.64 3.47A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.741ZM1 11.105l4.708-2.897L1 5.383v5.722Z"/>
                        </svg>
                        <?= $user->getEmail() ?>
                    </li>
                    <li class="list-group-item rounded-3">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                             class="bi bi-bank m" viewBox="0 0 16 16">
                            <path d="M8 .95 14.61 4h.89a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5H15v7a.5.5 0 0 1 .485.379l.5 2A.5.5 0 0 1 15.5 17H.5a.5.5 0 0 1-.485-.621l.5-2A.5.5 0 0 1 1 14V7H.5a.5.5 0 0 1-.5-.5v-2A.5.5 0 0 1 .5 4h.89L8 .95zM3.776 4h8.447L8 2.05 3.776 4zM2 7v7h1V7H2zm2 0v7h2.5V7H4zm3.5 0v7h1V7h-1zm2 0v7H12V7H9.5zM13 7v7h1V7h-1zm2-1V5H1v1h14zm-.39 9H1.39l-.25 1h13.72l-.25-1z"/>
                        </svg>
                        <?= $bankaccount->getIban() ?>
                    </li>

                    <li class="list-group-item rounded-3">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                             class="bi bi-piggy-bank" viewBox="0 0 16 16">
                            <path d="M5 6.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0zm1.138-1.496A6.613 6.613 0 0 1 7.964 4.5c.666 0 1.303.097 1.893.273a.5.5 0 0 0 .286-.958A7.602 7.602 0 0 0 7.964 3.5c-.734 0-1.441.103-2.102.292a.5.5 0 1 0 .276.962z"/>
                            <path fill-rule="evenodd"
                                  d="M7.964 1.527c-2.977 0-5.571 1.704-6.32 4.125h-.55A1 1 0 0 0 .11 6.824l.254 1.46a1.5 1.5 0 0 0 1.478 1.243h.263c.3.513.688.978 1.145 1.382l-.729 2.477a.5.5 0 0 0 .48.641h2a.5.5 0 0 0 .471-.332l.482-1.351c.635.173 1.31.267 2.011.267.707 0 1.388-.095 2.028-.272l.543 1.372a.5.5 0 0 0 .465.316h2a.5.5 0 0 0 .478-.645l-.761-2.506C13.81 9.895 14.5 8.559 14.5 7.069c0-.145-.007-.29-.02-.431.261-.11.508-.266.705-.444.315.306.815.306.815-.417 0 .223-.5.223-.461-.026a.95.95 0 0 0 .09-.255.7.7 0 0 0-.202-.645.58.58 0 0 0-.707-.098.735.735 0 0 0-.375.562c-.024.243.082.48.32.654a2.112 2.112 0 0 1-.259.153c-.534-2.664-3.284-4.595-6.442-4.595zM2.516 6.26c.455-2.066 2.667-3.733 5.448-3.733 3.146 0 5.536 2.114 5.536 4.542 0 1.254-.624 2.41-1.67 3.248a.5.5 0 0 0-.165.535l.66 2.175h-.985l-.59-1.487a.5.5 0 0 0-.629-.288c-.661.23-1.39.359-2.157.359a6.558 6.558 0 0 1-2.157-.359.5.5 0 0 0-.635.304l-.525 1.471h-.979l.633-2.15a.5.5 0 0 0-.17-.534 4.649 4.649 0 0 1-1.284-1.541.5.5 0 0 0-.446-.275h-.56a.5.5 0 0 1-.492-.414l-.254-1.46h.933a.5.5 0 0 0 .488-.393zm12.621-.857a.565.565 0 0 1-.098.21.704.704 0 0 1-.044-.025c-.146-.09-.157-.175-.152-.223a.236.236 0 0 1 .117-.173c.049-.027.08-.021.113.012a.202.202 0 0 1 .064.199z"/>
                        </svg>
                        <?= $bankaccount->calculateBalance($bankaccount->getIban()) . " €" ?>
                    </li>
                </ul>


                <div class="mt-3 row">
                    <form id="form_logout" action="customerView.php" method="post">
                        <input type="submit" name="logout" class="btn btn-outline-secondary col-6"
                               value="Logout"/>
                    </form>
                </div>


            </div>
        </div>
        <div class="col-sm-8 mb-5">
            <h4 class="text-center">Transaktionen</h4>

            <div class="mt-4 form-group">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th>Summe</th>
                        <th>Verwendungszweck</th>
                        <th>Transaktionszeitpunkt</th>
                        <th>Empfänger</th>
                        <th>Absender</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sender_iban = $bankaccount->getIban();
                    $transactions = Transaction::getAllBySenderOrReceiverIban($sender_iban);

                    if ($gesuchtDatum) {
                        $transactions = Transaction::getByDates($dateFrom, $dateTo, $sender_iban);
                    }

                    if ($gesuchtBetraege) {
                        $transactions = Transaction::getByAmounts($amountFrom, $amountTo, $sender_iban);
                    }

                    if ($gesuchtText) {
                        $transactions = Transaction::getByText($text, $sender_iban);
                    }
                    foreach ($transactions as $transaction) {
                        $sqldate = date_create($transaction->getTransactionTime());
                        $date = date_format($sqldate, "H:i:s - d.m.Y");
                        echo "<tr>";
                        echo "<td>" . $transaction->getAmount() . "€</td>";
                        echo "<td>" . $transaction->getPurpose() . "</td>";
                        echo "<td>" . $date . "</td>";

                        if ($transaction->getReceiverIban() == Bankaccount::IBAN_BANK) {
                            echo "<td>Bar</td>";
                        } else if ($bankaccount->getIban() == $transaction->getReceiverIban()) {
                            echo "<td>Kontoeingang</td>";
                        } else {
                            echo "<td>" . $transaction->getReceiverIban() . "</td>";
                        }
                        if ($transaction->getSenderIban() == Bankaccount::IBAN_BANK) {
                            echo "<td>Bar</td>";
                        } else if ($bankaccount->getIban() == $transaction->getSenderIban()) {
                            echo "<td>Kontoausgang</td>";
                        } else {
                            echo "<td>" . $transaction->getSenderIban() . "</td>";
                        }
                        //Alte Version nicht mehr verwenden
                        // echo "<td>" . ($bankaccount->getIban() == $transaction->getReceiverIban() ? 'Kontoeingang' : $transaction->getReceiverIban()) . "</td>";
                        // echo "<td>" . ($bankaccount->getIban() == $transaction->getSenderIban() ? 'Kontoausgang' : $transaction->getSenderIban()) . "</td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>

            <div align="right">
                <a href="transactionView.php" class="col-1 btn btn-outline-primary">Neu</a>
            </div>

            <div class="form-group mt-4">
                <h5 class="text-center">Transaktionen durchsuchen</h5>
            </div>

            <?php
            if (!empty($filtererrors)) {
                echo "<div class='alert-danger form-control'>Die eingegeben Daten sind fehlerhaft!<ul>";
                foreach ($filtererrors as $key => $value) {
                    echo "<li>" . $value . "</li>";
                }
                echo "</ul></div>";
            }
            ?>

            <!-- Suche nach Datum -->
            <form action="customerView.php" method="post" class="mt-4">
                <div class="row">
                    <div class="col-sm-10 form-group">
                        <div class="input-group">
                            <span class="input-group-text">Von</span>
                            <input type="date"
                                   id="dateFrom"
                                   name="dateFrom"
                                   class="form-control"
                                   value="<?= htmlspecialchars($dateFrom) ?>"
                                   required
                            />
                            <span class="input-group-text">Bis</span>
                            <input type="date"
                                   id="dateTo"
                                   name="dateTo"
                                   class="form-control"
                                   value="<?= htmlspecialchars($dateTo) ?>"
                                   required
                            />
                        </div>
                    </div>
                    <div class="col-sm-2 form-group">
                        <input type="submit"
                               name="searchDate"
                               class="btn btn-outline-primary w-100"
                               value="Suchen"/>
                    </div>
                </div>
            </form>

            <!-- Suche nach Beträgen -->
            <form action="customerView.php" method="post" class="mt-3">
                <div class="row">
                    <div class="col-sm-10 form-group">
                        <div class="input-group">
                            <span class="input-group-text">Von</span>
                            <input type="number"
                                   id="amountFrom"
                                   name="amountFrom"
                                   class="form-control"
                                   min="0"
                                   placeholder="Betrag"
                                   value="<?= htmlspecialchars($amountFrom) ?>"
                                   required
                            />
                            <span class="input-group-text">Bis</span>
                            <input type="number"
                                   id="amountTo"
                                   name="amountTo"
                                   class="form-control"
                                   min="0"
                                   placeholder="Betrag"
                                   value="<?= htmlspecialchars($amountTo) ?>"
                                   required
                            />
                        </div>
                    </div>
                    <div class="col-sm-2 form-group">
                        <input type="submit"
                               name="searchAmount"
                               class="btn btn-outline-primary w-100"
                               value="Suchen"/>
                    </div>
                </div>
            </form>

            <!-- Suche nach Text-->
            <form action="customerView.php" method="post" class="mt-3">
                <div class="row">

                    <div class="col-sm-10 form-group">
                        <div class="input-group">
                            <span class="input-group-text">Verwendungszweck</span>
                            <input type="text"
                                   name="text"
                                   class="form-control"
                                   required
                                   placeholder="hier eingeben"
                                   value="<?= htmlspecialchars($text) ?>"
                            />
                        </div>
                    </div>

                    <div class="col-sm-2 form-group">
                        <input type="submit"
                               name="searchText"
                               class="btn btn-outline-primary w-100"
                               value="Suchen"/>
                    </div>

                </div>
            </form>

            <div class="row mt-3">
                <div class="col-sm-2 form-group">
                    <a href="customerView.php" class="btn btn-outline-secondary w-100">Leeren</a>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>