
# Notenerfassung 2.0

- Temporäres Speichern von Daten mittels Cookies und Sessions
- Löschen der Temporären Daten
- Trennung von Darstellung und der Businesslogik (Objektorientierte Programmierung)
    - Model-Klasse in eigenem Ordner (models), Datenfelder und Methoden
    - Formularvalidierung
<br>
<br>

# Inhaltsverzeichnis
- [Notenerfassung 2.0](#notenerfassung-20)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Cookie](#cookie)
- [Session](#session)
- [Opjektorientierung](#opjektorientierung)
- [JSON](#json)



# Cookie

Ein Cookie wird häufig verwendet, um einen Benutzer zu identifizieren. Ein Cookie ist eine kleine Datei, die der Server auf dem Computer des Benutzers einbettet. Jedes Mal, wenn derselbe Computer eine Seite mit einem Browser anfordert, sendet er auch das Cookie. Mit PHP können Sie Cookie-Werte sowohl erstellen als auch abrufen.

→ Assoziatives Array

- werden am Client abgespeichert
- Cookie Id + Session Id (Information die im Browser gespeichert wird)
- haben Ablaufzeit, kann gesetzt werden (default Sitzungsende ca. 30min)
- pro Seite/Domain gültig
- wird vom Server gesetzt
- Kann vom Server validiert werden

```php
setcookie('name', 'value'); //erneuert Cookie und schickt dieses zurück
print_r($_COOKIE); //gibt übermitteltes Cookie aus (muss vom Client übermittelt werden)
setcookie("user", "", time() - 3600); // set the expiration date to one hour ago
if(count($_COOKIE) > 0) //prüfen ob Cookies vorhanden sind
```

# Session

Eine Session ist eine Möglichkeit, Informationen (in Variablen) zu speichern, die auf mehreren Seiten verwendet werden sollen. Im Gegensatz zu einem Cookie werden die Informationen nicht auf dem Computer des Benutzers gespeichert.

→ Assoziatives Array

- werden am Server gespeichert
- Session ID wird lokal in einem Cookie gespeichert
- User Authentifizierung
- Standardmäßig bleiben Sitzungsvariablen bestehen, bis der Benutzer den Browser schließt.

```php
session_start(); //startet die Session, muss unbedingt am Anfang der Seite stehen (keine Ausgabe vorher)
$_SESSION['name'] = 'mathias'; //setzen einer Session
print_r($_SESSION);
session_destroy; //löscht die Session Id
```

# Opjektorientierung

- Klasse mit Konstruktor

```php
<?php
class Fruit {
  // Properties
  public $name;
  public $color;

	function __construct($name) {
    $this->name = $name; 
  }

  // Methods
  function set_name($name) {
    $this->name = $name;
  }
  function get_name() {
    return $this->name;
  }
}
?>
```

- Ein Destruktor wird aufgerufen, wenn das Objekt zerstört oder das Skript gestoppt oder beendet wird.

```php
function __destruct() {
    echo "The fruit is {$this->name}."; 
  }
```

- Vererbung, Abstrakte Klassen und Interfaces sind wie in Java zu behandeln
- Zugriffsmodifikatoren wie in Java (public, private, ...)
- Konstanten wie in Java Final Variablen

```php
<?php
class Konstante {
  const test = "Test";
}

echo Konstante::test; //Zugriff auf Konstante
?>
```

- Serialisieren von Objekten mittels serialize() / unserialize();
- Statische Variablen und Methoden

```php
<?php
class Test {
  public static function test() {
    echo "Hello World!";
  }
}

Test::test(); //Aufruf statische Methode
?>
```

# JSON

Das JSON-Format (JavaScript Object Notation) ähnelt syntaktisch dem Code zum Erstellen von JavaScript-Objekten. Aus diesem Grund kann ein JavaScript-Programm JSON-Daten problemlos in JavaScript-Objekte umwandeln.

```json
{
"name":"Mathias",
"age":30
}
```

JavaScript hat eine eingebaute Funktion zum Konvertieren von JSON-Strings in JavaScript-Objekte:

`JSON.parse()`

JavaScript hat auch eine eingebaute Funktion zum Konvertieren eines Objekts in einen JSON-String:

`JSON.stringify()`
