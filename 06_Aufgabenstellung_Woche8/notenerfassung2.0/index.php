<?php
//davor keine Aushgabe, sonst funktioniert die Session nicht
session_start();
require_once "models/GradeEntry.php"; //PHP prüft hier, ob die gewünschte Datei bereits eingebunden wurde
// um sie nicht ein weiteres Mal einzubinden

$gradeEntry = new GradeEntry();
$message = '';
if (isset($_POST['submit'])) { //name des submit buttons
    //setzten der Datenfelder
    $gradeEntry->setName(isset($_POST['name']) ? $_POST['name'] : ''); //ternary-operator
    $gradeEntry->setEmail(isset($_POST['email']) ? $_POST['email'] : '');
    $gradeEntry->setSubject(isset($_POST['subject']) ? $_POST['subject'] : '');
    $gradeEntry->setGrade(isset($_POST['grade']) ? $_POST['grade'] : '');
    $gradeEntry->setExamDate(isset($_POST['examDate']) ? $_POST['examDate'] : '');

    if ($gradeEntry->validate()) {
        $gradeEntry->save();
        $message = "<p class='alert alert-success'>Die eingegebenen Daten sind in Ordnung!</p>";
    } else {
        $message = "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
        foreach ($gradeEntry->getErrors() as $key => $value) {
            $message .= "<li>" . $value . "</li>";
        }
        $message .= "</ul></div>";
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Required meta tags -->
    <link href="css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <!-- einbinden der Bootstrap CSS Datei-->
    <title>Notenerfassung 2.0</title>
    <script type="application/javascript" src="js/index.js"></script>
    <!-- Einbinden der JavaScript-Datei -->
</head>
<body>
<div class="container">
    <h1 class="mt-5 mb-5">Notenerfassung 2.0</h1>
    <!-- mt-5 = margin top -->
    <!-- mb-5 = margin bottom -->
    <!-- Hier startet das Php-Script zur Serverseitigen Validierung -->
    <?php echo $message ?>
    <form id="form_grade" action="index.php" method="post">
        <!-- action definiert wohin das Formular per method(POST) geschickt wird -->
        <div class="row">
            <!-- row = neue Reihe innerhalb des containers -->
            <div class="col-sm-6 form-group">
                <!-- col-sm-6 = Größe für responsive Design -->
                <label for="name">Name*</label>
                <input
                        type="text"
                        name="name"
                        class="form-control <?= $gradeEntry->hasErrors('name') ? 'is-invalid' : '' ?>"
                        maxlength="20"
                        required
                        value="<?= htmlspecialchars($gradeEntry->getName()) ?>"
                />
                <!-- type legt den Typ der Eingabe fest für die Clientseitige Validierung -->
                <!-- maxlength prüft die Länge der Eingabe für die Clientseitige Validierung -->
                <!-- required beschreibt ein verpflichtendes Feld (bei nicht eingabe kann das Formular nicht abgeschickt werden) -->
            </div>
            <div class="col-sm-6 form-group">
                <label for="email">E-Mail</label>
                <input
                        type="email"
                        name="email"
                        class="form-control <?= $gradeEntry->hasErrors('email') ? 'is-invalid' : '' ?>"
                        value="<?= htmlspecialchars($gradeEntry->getEmail()) ?>"
                />
                <!-- mit value werden vorher eingetragene Werte wieder eingesetzt werden -->
                <!-- htmlspecialchars wird zum Schutz vor Code Einschleusungen verwendet -->
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 form-group">
                <label for="subject">Fach*</label>
                <select
                        name="subject"
                        class="custom-select <?= $gradeEntry->hasErrors('subject') ? 'is-invalid' : '' ?>"
                        required
                >
                    <!-- custom-select = Auswahl -->
                    <option value="" hidden>Fach auswählen</option>
                    <!-- hidden wird nur angezeigt, kann aber nicht selektiert werden -->
                    <option value="m" <?php if ($gradeEntry->getSubject() == 'm') echo "selected = 'selected'"; ?>>
                        Mathematik
                    </option>
                    <option value="d" <?php if ($gradeEntry->getSubject() == 'd') echo "selected = 'selected'"; ?>>
                        Deutsch
                    </option>
                    <option value="e" <?php if ($gradeEntry->getSubject() == 'e') echo "selected = 'selected'"; ?>>
                        Englisch
                    </option>
                </select>
            </div>
            <div class="col-sm-4 form-group">
                <label for="grade">Note*</label>
                <input type="number"
                       name="grade"
                       class="form-control <?= $gradeEntry->hasErrors('grade') ? 'is-invalid' : '' ?>"
                       min="1"
                       max="5"
                       required
                       value="<?= htmlspecialchars($gradeEntry->getGrade()) ?>"
                />
                <!-- min und max grenzen den Wertebereich für die Clientseitige Validierung ein -->
            </div>
            <div class="col-sm-4 form-group">
                <label for="examDate">Prüfungsdatum*</label>
                <input
                        type="date"
                        name="examDate"
                        class="form-control <?= $gradeEntry->hasErrors('examDate') ? 'is-invalid' : '' ?>"
                        required
                        onchange="validateExamDate(this)"
                        value="<?= htmlspecialchars($gradeEntry->getExamDate()) ?>"
                />
                <!-- onchange löst eine funktion aus und mit this wird das eingetragene Datum übergeben -->
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3 mb-3">
                <input
                        type="submit"
                        name="submit"
                        class="btn btn-primary btn-block"
                        value="Speichern"
                />
                <!-- btn-primary = roter Button -->
                <!-- btn-block = Format des Buttons -->
                <!-- value = Dargestellter Werteinhalt des Buttons -->
            </div>
            <div class="col-sm-3 mb-3">
                <a href="index.php" class="btn btn-secondary btn-block">Löschen</a>
                <!-- Link als Button zur index.php -->
                <!-- btn-secondary = blauer Button -->
            </div>
        </div>
    </form>
    <h2 class="mt-3">Noten</h2>
    <div id="grades">
        <!-- für CSS oder JavaScript Funktionen -->
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>E-Mail</th>
                <th>Prüfungsdatum</th>
                <th>Fach</th>
                <th>Note</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $grades = GradeEntry::getAll();
            foreach ($grades as $grade) {
                echo "<tr>";
                echo "<td>" . $grade->getName() . "</td>";
                echo "<td>" . $grade->getEmail() . "</td>";
                echo "<td>" . $grade->getExamDateFormatted() . "</td>";
                echo "<td>" . $grade->getSubjectFormatted() . "</td>";
                echo "<td>" . $grade->getGrade() . "</td>";
                echo "</tr>";
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php
    if (count($grades) > 0){ //nur anzeigen, wenn ein Noteneintrag vorhanden
    ?>
    <form action="clear.php" method="post">
        <input type="submit" name="clear" class="btn btn-danger" value="Alle Note löschen"/>
    </form>
    <?php
    }
    ?>
</div>
</body>
</html>