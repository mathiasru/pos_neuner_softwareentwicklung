<?php

class GradeEntry
{
    //Datenfelder
    private $name = '';
    private $email = '';
    private $examDate = '';
    private $subject = '';
    private $grade = '';
    private $errors = []; //Array für die Fehlermeldungen

    //Konstruktor
    public function __constructor()
    {

    }

    /**
     * Gibt alle unserialisierten Objekte aus dem Session Array in form eines Arrays zurück.
     * @return array
     */
    public static function getAll() //statische Funktion, da wir für den Aufruf kein Objekt erzeugen wollen
    {
        $grades = []; //Array erstellen
        if (isset($_SESSION['grades'])) { //prüfen ob Daten im Session Array vorhanden sind
            foreach ($_SESSION['grades'] as $grade) { //Session Array durchitterieren
                $grades[] = unserialize($grade); //Textdaten wieder in Objekte konvertieren und in ein Array speichern
            }
        }
        return $grades;
    }

    /**
     * Löscht alle Grades Einträge aus dem Session Array
     */
    public static function deleteAll() //statische Funktion, da wir für den Aufruf kein Objekt erzeugen wollen
    {
        if (isset($_SESSION['grades'])) { //prüfen ob Daten im Session Array vorhanden sind
            unset($_SESSION['grades']); //hier könnte man die Session auch mittels session_destroy; zerstören
        }
    }

    /**
     * Speichert ein serialisiertes Objekt in das Session Array und gibt einen Boolean zurück.
     * @return bool
     */
    public function save()
    {
        if ($this->validate()) {
            $grade = serialize($this); //serialisieren eines Objektes, um es als Text zu speichern
            $_SESSION['grades'][] = $grade; //anhängen von Daten mittels Array-Push
            return true;
        }
        return false;
    }

    /**
     * Gibt ein Formatiertes Datum zurück.
     * @return false|string
     */
    public function getExamDateFormatted()
    {
        return date_format(date_create($this->examDate), "d.m.Y");
    }

    /** Gibt ein Formatiertes Fach zurück */
    public function getSubjectFormatted()
    {
        switch ($this->subject) {
            case 'm':
                return 'Mathematik';
            case 'd':
                return 'Deutsch';
            case 'e':
                return 'Emglisch';
            default:
                return null;
        }
    }

    /**
     * Prüft ob ein bestimmter Fehler vorhanden ist.
     * @param $field übergebener Fehlername.
     * @return bool
     */
    public function hasErrors($field)
    {
        if (isset($this->errors[$field])) {
            return true;
        }
        return false;
    }

    /**
     * Validieren aller Datenfelder mittels Methoden.
     */
    public function validate()
    {
        //Verwendung einfacher & damit alle Funktionen für die Ausgabe der Fehlermeldungen ausgeführt werden
        return $this->validateName() & $this->validateEmail() & $this->validateSubject() & $this->validateGrade() & $this->validateExamDate();
    }

    /**
     * Die Funktion überprüft den übergebenen String auf Gültigkeit.
     * @return bool
     */
    private function validateName()
    {
        if (strlen($this->name) == 0) { //strlen prüft die Länge eines Strings
            $this->errors['name'] = "Name darf nicht leer sein"; //schreiben in das globalle Array
            return false;
        } elseif (strlen($this->name) > 20) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Die Funktion prüft auf ein gültiges Datum, welches nicht leer oder in der Zukunft liegen darf.
     * @return bool
     */
    private function validateExamDate()
    {
        try {
            if ($this->examDate == "") {
                $this->errors['examDate'] = "Prüfungsdatum darf nicht leer sein";
                return false;
            } else if (new DateTime($this->examDate) > new DateTime()) {
                $this->errors['examDate'] = "Prüfungsdatum darf nicht in der Zukunft liegen";
                return false;
            } else {
                return true;
            }
        } catch (Exception $exception) {
            $this->errors['examDate'] = "Prüfungsdatum ungültig";
            return false;
        }
    }

    /**
     * Die Funktion überprüft auf ein gültiges Fach (m,d,e).
     * @return bool
     */
    private function validateSubject()
    {
        if ($this->subject != 'm' && $this->subject != 'd' && $this->subject != 'e') {
            $this->errors['subject'] = "Fach ungültig";
            return false;
        } else {
            return true;
        }
    }

    /**
     * Die Funktion überprüft auf eine gültige Zahl zwischen 1-5 und prüft ob es überhaupt eine Zahl ist.
     * @return bool
     */
    private function validateGrade()
    {
        if (!is_numeric($this->grade) || $this->grade < 1 || $this->grade > 5) { //is_numeric prüft auf eine Zahl
            $this->errors['grade'] = "Note ungültig";
            return false;
        } else {
            return true;
        }
    }

    /**
     * Die Funktion überprüft auf eine gültige E-Mail Adresse.
     * @return bool
     */
    private function validateEmail()
    {
        if ($this->email != "" && !filter_var($this->email, FILTER_VALIDATE_EMAIL)) { //prüfen auf leer und gültig
            $this->errors['email'] = "E-Mail ungültig";
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getExamDate()
    {
        return $this->examDate;
    }

    /**
     * @param string $examDate
     */
    public function setExamDate($examDate)
    {
        $this->examDate = $examDate;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @param string $grade
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}