# POS_Neuner_Softwareentwicklung
Hier werden wöchentlich die Aufgaben zum Unterrichtsgegenstand POS-Softwareentwicklung hochgeladen.

- [x] Formulare (Woche1)
- [x] Formulare (Woche2)
- [x] BMI-Rechner (Woche3)
- [x] Sprint 1 E-Banking (Woche6+7)
- [x] Notenerfassung 2.0 (Cookies, Sessions, OOP) (Woche8)
- [x] Wochenkarte (Woche9)
- [x] Warenkorb (Woche10+11)
- [x] Sprint 2 E-Banking (Woche12+13)
