<?php
require "userdata.php"; //Benutzerdaten

/**
 * Gibt alle Daten in Form eines mehrdimmensionalen Arras zurück.
 * @return array[] gibt alle Daten in Form eines mehrdimmensionalen Arras zurück.
 */
function getAllData()
{
    global $data;
    return $data;
    // hier könnte ggf. gefrüft werden, ob vorhanden -> null
    // müsste beim Funktionsaufruf berücksichtigt werden.
}

/**
 * Gibt ein bestimmtes Array (Benutzerobjekt), welches anhand der ID gefiltert wird zurück.
 * @param $id übergebene ID, nach der gesucht wird.
 * @return array|null gibt ein Array (Benutzerobjekt) zurück.
 */
function getDataPerId($id)
{
    global $data;
    foreach ($data as $users) {
        if ($users['id'] == $id) {
            return $users;
        }
    }
    return null;
}

/**
 * Gibt anhand des übergebenen Filters ein mehrdimmensionales Array zurück.
 * @param $filter übergebener Filter, nachdem die User gefiltert werden.
 * @return array gibt das gefilterte mehrdimmensionale Array zurück.
 */
function getFilteredData($filter)
{
    global $data;
    $filteredData = [];
    foreach ($data as $users => $inhalt) {
        $gefundenFirstname = stripos($inhalt['firstname'], $filter);
        $gefundenLastname = stripos($inhalt['lastname'], $filter);
        $gefundenEmail = stripos($inhalt['email'], $filter);
        $gefundenBirthdate = stripos($inhalt['birthdate'], $filter);
        if ($gefundenFirstname !== false | $gefundenLastname !== false | $gefundenEmail !== false | $gefundenBirthdate !== false) {
            $filteredData[] = $inhalt;
        }
    }
    return $filteredData;
}

/**
 * Prüft ob eine ID vorhanden ist und gibt einen boolschen Wert zurück.
 * @param $id übergebene ID, nach der gesucht wird.
 * @return bool bei Übereinstimmung wird ein true zurückgegeben.
 */
function existingId($id)
{
    global $data;
    foreach ($data as $users) {
        if ($users['id'] == $id) {
            return true;
        }
    }
    return false;
}

/**
 * Gibt Teilnhalte eines Mehrdimensionalen Arrays in Form einer formatierten Tabelle aus.
 * @param $userData mehrdimensionales Array, welches in Form einer Tabelle ausgegeben wird.
 */
function printData($userData)
{
    foreach ($userData as $user => $inhalt) {
        echo "<tr>";
        echo "<td><a href='details.php?id=" . $inhalt['id'] . "'>" . $inhalt['firstname'] . " " . $inhalt['lastname'] . "</a></td>";
        echo "<td>" . $inhalt['email'] . "</td>";
        echo "<td>" . formatDate($inhalt['birthdate']) . "</td>";
        echo "</tr>";
    }
}

/**
 * Gibt Inhalte eines Mehrdimensionalen Arrays in Form einer formatierten Tabelle aus.
 * @param $detailsUser mehrdimensionales Array, welches in Form einer Tabelle ausgegeben wird.
 */
function printDetails($detailsUser)
{
    echo "<div class='mt-3'>";
    echo "<table class='table'>";
    echo "<tbody>";
    echo "<tr><td class='col-sm-3'>Firstname</td>";
    echo "<td>" . $detailsUser['firstname'] . "</td></tr>";
    echo "<tr><td class='col-sm-3'>Lastanme</td>";
    echo "<td>" . $detailsUser['lastname'] . "</td>";
    echo "<tr><td class='col-sm-3'>E-Mail</td>";
    echo "<td>" . $detailsUser['email'] . "</td></tr>";
    echo "<tr><td class='col-sm-3'>Birthdate</td>";
    echo "<td>" . formatDate($detailsUser['birthdate']) . "</td></tr>";
    echo "<tr><td class='col-sm-3'>Phone</td>";
    echo "<td>" . $detailsUser['phone'] . "</td></tr>";
    echo "<tr><td class='col-sm-3'>Street</td>";
    echo "<td>" . $detailsUser['street'] . "</td></tr>";
    echo "</tbody></table></div>";
}

/**
 * Gibt die übergebene Message in Form eines Allerts aus.
 * @param $errorMessage übergebene Error Message.
 */
function printAlert($errorMessage)
{
    echo "<div class='alert alert-danger mt-2'>" . $errorMessage . "</div>";
}

/**
 * Formatiert ein übergebenes Datum und gibt dieses zurück.
 * @param $birthdate übergebenes Geburtsdatum.
 * @return string formatiertes Datum.
 */
function formatDate($birthdate)
{
    try {
        $date = new DateTime($birthdate);
    } catch (Exception $exception) {
    }
    return $date->format('d.m.Y');
}