<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
    <title>Benutzerdaten</title>
</head>
<body>
<?php
require "./lib/func.inc.php";
$suche = '';
?>
<div class="container">
    <h1 class="mt-5 mb-3">Benutzerdaten anzeigen</h1>
    <form action="index.php" method="post" id="form-grade">
        <div class="row">
            <div class="col-sm-1 form-group">
                <label for="name">Suche:</label>
            </div>
            <div class="col-sm-2 form-group">
                <input type="text"
                       name="suche"
                       class="form-control"
                       value="<?php htmlspecialchars($suche) ?>"
                       required
                />
            </div>
            <div class="col-sm-1 form-group">
                <input type="submit"
                       name="submit"
                       class="btn btn-primary w-100"
                       value="Suchen"/>
            </div>
            <div class="col-sm-1 form-group">
                <a href="index.php" class="btn btn-secondary w-100">Leeren</a>
            </div>
        </div>
    </form>
    <div class="mt-3">
        <table class="table table-bordered table-striped">
            <thead class="thead-dark">
            <tr>
                <th>Name</th>
                <th>E-mail</th>
                <th>Birthdate</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (isset($_POST['suche']) && $_POST['suche'] != "") {
                $filteredData = getFilteredData($_POST['suche']);
                if (empty($filteredData)){
                    printAlert("Keine Einträge gefunden");
                }else{
                    printData($filteredData);
                }
            } else {
                $userData = getAllData();
                printData($userData);
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>