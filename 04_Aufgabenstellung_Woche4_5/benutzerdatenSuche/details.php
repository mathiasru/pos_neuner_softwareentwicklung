<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
    <title>Benutzerdetails</title>
</head>
<body>
<?php
require "./lib/func.inc.php";
$id = ($_GET);
?>
<div class="container">
    <h1 class="mt-5 mb-3">Benutzerdetails</h1>
    <div>
        <a href="index.php" class="btn btn-primary">Zurück</a>
    </div>
    <?php
    if (!is_numeric($id['id']) | !existingId($id['id'])) {
        printAlert("Fehlerhafte ID");
    } else {
        $detailsUser = getDataPerId($id['id']);
        if ($detailsUser != null){
            printDetails($detailsUser);
        } else {
            printAlert("Keine Daten gefunden");
        }
    }
    ?>
</div>
</body>
</html>