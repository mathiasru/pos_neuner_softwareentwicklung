# Benutzerdaten Bericht und Testprotokoll

- [Benutzerdaten Bericht und Testprotokoll](#benutzerdaten-bericht-und-testprotokoll)
- [Konfiguration Webserver](#konfiguration-webserver)
- [Implementieren der Benutzerdaten-Suche](#implementieren-der-benutzerdaten-suche)
  - [Einstellungen PhpStorm](#einstellungen-phpstorm)
  - [Formular erstellen und clientseitige Validierung](#formular-erstellen-und-clientseitige-validierung)
  - [Serverseitige Validierung](#serverseitige-validierung)
  - [Funktion](#funktion)
    - [Benutzerausgabe innerhalb der index.php](#benutzerausgabe-innerhalb-der-indexphp)
    - [Detailausgabe innerhalb der details.php](#detailausgabe-innerhalb-der-detailsphp)
- [Testprotokoll](#testprotokoll)
# Konfiguration Webserver

- Virtuelle Maschine mit Linux Ubuntu Server
- Docker Container lt. docker-compose.yml
```yml
  version: '2'
  services:
  apache:
    image: thecodingmachine/php:7.2-v2-apache-node10
    container_name: apache
    restart: unless-stopped
    ports:
      - 80:80
    environment:
            PHP_EXTENSION_PGSQL: 1
            PHP_EXTENSION_XDEBUG: 1
            PHP_EXTENSION_MONGODB: 1
            PHP_EXTENSION_MONGO: 1
            PHP_INI_ERROR_REPORTING: E_ALL
            APACHE_EXTENSION_AUTOINDEX: 1

    volumes:
      - ./www:/var/www/html
```

- sftp installieren
    - sudo apt-get install vsftpd
- Verbindung von PHPStorm zu VM mittels sftp
    - unter Tools Deployment anlegen
- Verbindung zum Host Zugangsdaten
    - Benutzer: mathiasrudig
    - Passwort: Standard ohne !

# Implementieren der Benutzerdaten-Suche
Das gesamte Programm wurde im Zuge  Partnerarbeit durchgeführt (Rudig, Tilg), teilweise mit unterschiedlichen Ansätzen aber selbem Ergebnis.

## Einstellungen PhpStorm

- SSH-Verbindung -> Terminal (Tools - Start SSH Session)
- FTP-Verbindung -> Remote Host (Tools - Deployment - Browse Remote Host)
- SSH-Verbindung eintragen (Tools - Deployment - Configuration - Connection)
- Pfad mappen (Tools - Deployment - Configuration - Mappings)
- Upload bei Safe (Settings - Deployment - Options - Always)


## Formular erstellen und clientseitige Validierung

- Aufbau und Ordnerstruktur des Projektes einrichten (css, lib).
- Formular definieren `action="index.php" method="post"`
- input Feld "suche" (input-type text)
- `required` für clientseitige Validierung

## Serverseitige Validierung

- value des Inputs mit `<?php htmlspecialchars($suche) ?>` schützen
- Überprüfung nach dem Senden der Daten (submit) ob etwas gesucht (ob die Variable gesetzt) wurde
- Alert Ausgabe, falls nichts eingetragen wurde

## Funktion

Das Programm hat drei Hauptaufgaben:

- das Anzeigen aller Benutzerdaten
- das Anzeigen der Benutzerdaten nachdem gefiltert wurde
- das Anzeigen einer Detailseite, die alle Daten eines bestimmten Benutzers enthält

Diese Aufgaben sind jeweils in eigene Funktionen innnerhalb der `func.inc.php` ausgelagert. Zusätzlich muss beim Datum darauf geachtet werden, dass es im richtigen Format (dd.mm.yyyy) ausgegeben wird. Das Formular inklusive Validierung ist hier also nur eine Teilaufgabe, um die Filterfunktion bereitzustellen.

### Benutzerausgabe innerhalb der index.php
Innerhalb der `index.php` sollen die Benutzerdaten im Gesamten oder je nach Suche eine gefilterte Auswahl mittels Tabelle ausgegeben werden.
Dies wurde über eine einfache If-Else-Verzweigung gelöst, welche ausgelagerte Funktionen aufruft und in einer Tabelle mittels echo ausgibt.
Zusätzlich wird im Fehlerfall ein Alert ausgegeben.

```php
<?php
  if (isset($_POST['suche']) && $_POST['suche'] != "") {
    $filteredData = getFilteredData($_POST['suche']);
    if (empty($filteredData)){
       printAlert("Keine Einträge gefunden");
    }else{
       printData($filteredData);
    }
  } else {
      $userData = getAllData();
      printData($userData);
  }
?>
```

### Detailausgabe innerhalb der details.php
Der selektierte Benutzer führt über einen Link, der die ID enthält auf die `details.php`. Diese Id wird über ein GET-Array abgefangen und verwendet um die Benutzerdetails mittels ausgelagerten Funktionen darzustellen. Im Fehlerfall wird die Tabelle nicht ausgegeben, sondern nur ein Alert mit der jeweiligen Error-Message. 

```php
<?php
  if (!is_numeric($id['id']) | !existingId($id['id'])) {
    printAlert("Fehlerhafte ID");
  } else {
    $detailsUser = getDataPerId($id['id']);
    if ($detailsUser != null){
      printDetails($detailsUser);
    } else{
      printAlert("Keine Daten gefunden");
    }
  }
?>
```


# Testprotokoll

Aufrufen der index.php ohne Filterung:</br>
<img src="04_Bericht_BenutzerSuche_Bilder/Test_GetAllData.png" alt="Test_GetAllData" width=""/>

Aufrufen der index.php nach betätigen des Such-Buttons (POST-Method):</br>
<img src="04_Bericht_BenutzerSuche_Bilder/Test_GetFilteredData.png" alt="Test_GetFilteredData" width=""/>

Aufrufen der details.php über den Link mittels ID (GET-Method):</br>
<img src="04_Bericht_BenutzerSuche_Bilder/Test_GetDataPerId.png" alt="Test_GetDataPerId" width=""/>

Fehlerfall bei nicht vorhandener ID bzw. mitgabe eines Strings:</br>
<img src="04_Bericht_BenutzerSuche_Bilder/Test_Error.png" alt="Test_Error" width=""/>
